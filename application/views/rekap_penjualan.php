<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once('template/header.php') ?>
<style type="text/css" media="screen">
	.btn-info {
		background: #21b9bb linear-gradient(#21b9bb,#0fa6a8);
	}
</style>
<div class="right_col" role="main">
	<div class="row">
		<div class="x_panel">
      <div class="x_title">
        <h2>Rekap Transaksi<small></small></h2>
        <div class="clearfix"></div>
      </div>
      <section class="content">
      	<div class="row">
		    	<div class="col-lg-12">
						<div class="bs-docs-section">
					   	<div class="panel with-nav-tabs panel-default">
					      <div class="panel-heading">
		       			</div>
		       			<div class="panel-body">
		       				<div class="panel panel-default">
										<form method="POST" action="<?php echo site_url('Sales/export_penjualan') ?>" role="form" data-toggle="validator" class="form-horizontal" novalidate="true">
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-sm-3 control-label" for="nohape">Periode Awal</label>
													<div class="col-sm-9">
														<input value="" id="tanggal1" placeholder="Wajib isi (tanggal-bulan-tahun)" id="tanggalAwal" class="form-control" name="tanggalAwal" type="text">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label" for="nohape">Periode Akhir</label>
													<div class="col-sm-9">
														<input value="" id="tanggal2" placeholder="Wajib isi (tanggal-bulan-tahun)" id="tanggalAkhir" class="form-control" name="tanggalAkhir" type="text">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Stat. Penjualan</label>
													<div class="col-sm-9">
														<select class="form-control" name="status_penjualan">
															<option value="ALL">Semua Transaksi</option>
															<option value="BERHASIL">Transaksi Berhasil</option>
															<option value="BATAL">Transaksi Batal</option>
															<option value="MENUNGGU">Menunggu</option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label" for="nohape">&nbsp;</label>
													<div class="col-sm-9">
														<button id="submitRekap" type="submit" class="btn btn-info"><span class="fa fa-file-excel-o"></span> Export Excel</button>
													</div>
												</div>
											</div>
										</form>
									</div>  
		       			</div>
		       		</div>																						
		       		</div>
		       	</div>
		       </div>
		     </div>
			</section>
		</div>
	</div>
</div>
<?php require_once('template/footer.php') ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" charset="utf-8">
	$('#tanggal1').datepicker({
		autoclose: true,
    format: "yyyy-mm-dd",
    todayHighlight: true,
    orientation: "top auto",
    todayBtn: true,
    todayHighlight: true,
	});
	$('#tanggal2').datepicker({
		autoclose: true,
    format: "yyyy-mm-dd",
    todayHighlight: true,
    orientation: "top auto",
    todayBtn: true,
    todayHighlight: true,
	});
</script>