<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once('template/header.php') ?>
<style type="text/css" media="screen">
	td input[type="text"] {
    float: left;
    margin: 0 auto;
    width: 100%;
	}
	td input[type="checkbox"] {
    float: left;
    margin: 0 auto;
    width: 100%;
	}
	.panel-menu {
    padding: 10px 15px;
    background-color: #f5f5f5;
    border-radius: 10px;
	}
	.btn-primary {
		background: #25a4ed linear-gradient(#25a4ed ,#1f9ae1);
    border-color: #25a4ed;
    color: #FFFFFF;
	}
</style>
<div class="right_col" role="main">
	<div class="row">
		<div class="x_panel">
      <div class="x_title">
        <h2>PENJUALAN<small></small></h2>
        <div class="clearfix"></div>
      </div>
      <section class="content">
      	<div class="row">
		    	<div class="col-lg-12">
						<div class="bs-docs-section">
					   	<div class="panel with-nav-tabs panel-default">
					      <div class="panel-heading">
				          <ul class="nav nav-pills">
			              <li class=""><a href="<?php echo site_url('Sales') ?>">1. CATAT PENJUALAN <i class="glyphicon glyphicon-chevron-right"></i></a></li>
			              <li class="active"><a href="<?php echo site_url('Sales/konfirmasi_pengiriman') ?>">2. KONFIRMASI PENGIRIMAN <i class="glyphicon glyphicon-chevron-right"></i></a></li>
			              <li class=""><a href="<?php echo site_url('Sales/penjualan_selesai') ?>">3. PENJUALAN SELESAI <i class="glyphicon glyphicon-ok"></i></a></li>
				          </ul>
		       			</div>
		       			<div class="panel-body">
		       				<div class="tab-content">
		       					<div class="col-lg-12 panel-menu">
						    		  <a type="button" data-toggle="modal" data-target="#cetak_label"class="btn btn-default" ><i class="glyphicon glyphicon-print"></i>  Cetak Label Pengiriman</a>						    		  
						    		</div>
				            <form id="form_data" accept-charset="utf-8">
											<br><br><br><br>
											<table id="daftar_data_konfirmasi" class="table table-bordered table-striped table-hover">
												<thead>
													<tr>
														<th>#</th>
														<th>No Invoice</th>
														<th width="10%">Tgl Order</th>
														<th>Pelanggan</th>
														<th>Kota</th>
														<th>Kurir</th>
														<th width="10%">Tgl Kirim</th>
														<th width="15%">No Resi</th>
														<th>Penjualan<br>Batal</th>
													</tr>
												</thead>
												<tbody align="center">
													<?php 
														$no = 1;
														if ($data_konfirmasi->num_rows() > 0) {
															foreach ($data_konfirmasi->result() as $row) {
																?>
																<tr>
																	<td><?php echo $no ?>
																		<input type='hidden' name='no_invoice[]' value='<?php echo $row->no_invoice ?>'>
																		<input type='hidden' name='no_hp[]' value='<?php echo $row->no_hp ?>'>
																		<input type='hidden' name='kurir[]' value='<?php echo $row->kurir ?>'>
																		<input type='hidden' name='nama[]' value='<?php echo $row->nama ?>'>
																	</td>
																	<td><?php echo $row->tanggal ?></td>
																	<td><?php echo $row->no_invoice?></td>
																	<td><?php echo $row->nama ?></td>
																	<td><?php echo $row->kota ?></td>
																	<td><?php echo $row->kurir ?></td>
																	<td><input type='text' name='tgl_kirim[]' id='tgl_kirim' class='tgl_kirim' value='<?php echo $row->tgl_kirim ?>' readonly ></td>
																	<td><input type='text' name='no_resi[]' value='<?php echo $row->no_resi ?>' ></td>
																	<td><input type='checkbox' name='batal_check[]' value='<?php echo $row->no_invoice ?>' id='batal_check'>
																	</td>
																</tr>
																<?php	
															}
														}
													 ?>
												</tbody>
											</table>		
											<!-- Modal -->
											<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											  <div class="modal-dialog">
											    <div class="modal-content">
											      <div class="modal-header">
											        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											        <h4 class="modal-title" id="myModalLabel">Pembatalan Pesanan / Pengiriman</h4>
											      </div>
											      <div class="modal-body">
											         <div class="form-group">
															  <label for="comment">Keterangan :</label>
															  <textarea class="form-control" rows="5" name="keterangan" id="keterangan" required="">Stok Habis</textarea>
															</div> 
											      </div>
											      <div class="modal-footer">
											        <button type="button" class="btn btn-default" onclick="uncheck()" data-dismiss="modal">Close</button>
											        <a class="btn btn-danger"  id="submitBatal">Simpan</a>
											      </div>
											    </div>
											  </div>
											</div>
											<button type="submit" id="submitSimpan" name="submitSimpan" class="btn btn-primary">Simpan</button>		
											<input name="kirimsms" id="kirimsms" value="Y" checked="" type="checkbox">  Centang untuk sekaligus mengirim SMS-RESI
										</form>	
		       				</div>
		       			</div>																						
		       		</div>
		       	</div>
		       </div>
		     </div>
			</section>
		</div>
	</div>
</div>

<!-- cetak label -->
<div class="modal fade" id="cetak_label" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 900px" role="document">
    <div class="modal-content">
    	<div class="modal-header">
    		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    		<h4 class="modal-title">LABEL PENGIRIMAN YANG AKAN DICETAK</h4>
    	</div>
    	<div class="modal-body">
    		<form method="post" id="form_label" accept-charset="utf-8">
	    		<div class="row">
	        	<div class="col-lg-12">
	        		<input type="checkbox" id="show_barang" name="show_barang" value="Y"> <i>Tampilkan Daftar Produk</i><br>
	        		<input type="checkbox" id="show_logo" name="show_logo" value="Y" > <i>Tampilkan Logo</i>
	        	</div>
						<div class="col-lg-12">
							<br>
		        	<small>- Baris bercetak tebal sudah pernah dicetak </small>
		        </div>
	        	<div class="col-lg-12">
	        		&nbsp;
	        	</div>
	        </div>
	    		<div class="row">
	    			<div class="col-lg-12">
	    				<table id="daftar_data_konfirmasi" class="table table-bordered table-hover table-striped">
	    				<thead>
	    					<tr>
	    						<th>No</th>
							 		<th>Cetak All <input type="checkbox" id="checkedAll" checked onclick="showall()"></th>
							 		<th>No Invoice</th>
							 		<th>Tanggal Order</th>
							 		<th>Nama Penerima</th>
							 		<th>Kota</th>
							 		<th>No HP</th>
	    					</tr>
	    				</thead>
	    				<tbody align="center" style="font-weight:bold">
	    					<?php 
	    						if ($data_konfirmasi->num_rows() > 0) {
	    							$no = 1;
	    							?>
	    							<?php foreach ($data_konfirmasi->result() as $row): ?>
	    											<tr>
	    												<td width="3%" ><?php echo $no++ ?></td>
	    												<td><input type="checkbox" class="checkSingle" name="cetak_label_[]" checked></td>
	    												<td><?php echo $row->no_invoice ?></td>
	    												<td><?php echo $row->tanggal ?></td>
	    												<td><?php echo $row->nama ?></td>
	    												<td><?php echo $row->kota ?></td>
	    												<td><?php echo $row->no_hp ?></td>
	    											</tr>
	    							<?php endforeach ?>
	    						
	    							<?php
	    						}
	    					 ?>
	    				</tbody>
	    			</table>
	    			</div>
	    		</div>
	    	</div>	
    		</form>
		  <div class="modal-footer">
		  	<div>
  			<button type="button" onclick="print_now()" class="btn btn-primary">Cetak Label</button> 
  		</div><br>
  		<p><i>(Pastikan browser Anda mengijinkan Pop Up)</i></p>
		  </div>
		</div><!-- /.modal-content -->
  </div>
 </div>
<?php require_once('template/footer.php') ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#daftar_data_konfirmasi').DataTable({
			"processing": true, 
			"order": [], 
			"pageLength": 5,
			"bLengthChange": false,
			"bInfo": false,
			"bDestroy": true,
			"language": {
	      "emptyTable": "Data Daftar Pengiriman Belum Ada!"
	    },
	    "dom": '<"search"f><"top"l>rt<"bottom"ip><"clear">'
		}); 

		$('#form_data').on('submit', function(e) {
			e.preventDefault();
	  	
			swal({
			  title: "Warning!",
			  text: "Apakah label pengirim sudah dicetak ?",
			  icon: "warning",
			  buttons: ["Batal", "Simpan"],
			})
			.then((willDelete) => {
			  if (willDelete) {
			  	$.ajax({
		        type: 'post',
		        url: '<?php echo site_url('Sales/simpan_konfirmasi_pengiriman') ?>',
		        data: $('#form_data').serialize(),
		        success: function () {
		      		if ($('#kirimsms').is(':checked')) {
		      			swal("Data Berhasil Disimpan Dan SMS-RESI sudah disampaikan!", {
						      icon: "success",
						    });
						    
		      		} else {
		      			swal("Data Berhasil Disimpan!", {
						      icon: "success",
						    });
						    
		      		}
		      		location.reload();
		        }

		      });
			  } else {
			    swal("Gagal Disimpan!");
			  }
			});

		});

		$('#daftar_data_konfirmasi tbody').on('focus',".tgl_kirim", function(){
			$(this).datepicker({
				autoclose: true,
	      format: "yyyy-mm-dd",
	      todayHighlight: true,
	      orientation: "top auto",
	      todayBtn: true,
	      todayHighlight: true,
			});
		});

		$('#daftar_data_konfirmasi tbody').on('change',"#batal_check", function(){
			if ($(this).is(':checked')) {
				$('#myModal').modal();
			}	
		});

		$('#submitBatal').click(function() {
			var no_invoice = $('#batal_check').val();
			var ket = $('#keterangan').val();
			$.post('<?php echo site_url('Sales/simpan_konfirmasi_pengiriman/') ?>', {'no_invoice_pembatalan': no_invoice,'keterangan': ket }, function(data){
				$('#myModal').modal('hide');
				$('#daftar_data_konfirmasi').DataTable({
					"processing": true, 
					"order": [], 
					"pageLength": 5,
					"bLengthChange": false,
					"bInfo": false,
					"bDestroy": true,
					"language": {
			      "emptyTable": "Data Daftar Pengiriman Belum Ada!"
			    },
		      "ajax": {
		        "url": "<?php  echo site_url('Sales/daftar_konfirmasi_pengiriman') ?>",
		        "type": "GET"
		      }
				}); 
			});
  	});
	
	});	

	function uncheck() {
		$("#batal_check").prop("checked", false);
	}
	function print_now(){
		$.ajax({
    	type: 'post',
    	url : '<?php echo site_url('Sales/print_post') ?>',
    	data : $('#form_label').serializeArray(),
    	success: function(msg){
				window.open(msg);
    	}	
    })
	}
	
	function showall(){
  	if($("#checkedAll").is(':checked')){
	      $(".checkSingle").each(function(){
	        $(this).prop('checked',true);
	      })              
	    }else{
	      $(".checkSingle").each(function(){
	        $(this).prop('checked',false);
	      })              
	    }
  }
</script>