<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once('template/header.php') ?>
<style media="screen">
  footer {
    margin-top: 0px;
  }

  .dropdown-menu {
    min-width:20px;
  }
  .btn-info {
    background: #21b9bb linear-gradient(#21b9bb,#0fa6a8);
    border-color: #23c6c8;
    color: #FFFFFF;
  }

  .hr-line-dashed {
    border-top: 1px dashed #d8d8d8;
    color: #ffffff;
    background-color: #ffffff;
    height: 1px;
    margin: 10px 0;
  }
</style>

<div class="right_col" role="main">
  <div class="row">
    <?php if ($response = $this->session->flashdata('stok_masuk')) { ?>
    <div class="row">
        <div class="col-lg-12" align="center">
        <div class="alert alert-success" role="alert">
          <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
          <span class="sr-only">success:</span>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo $response; ?>
        </div>
        </div>
      </div>
<?php } elseif ($response = $this->session->flashdata('stok_keluar')) { ?>
         <div class="row">
        <div class="col-lg-12" align="center">
        <div class="alert alert-success" role="alert">
          <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
          <span class="sr-only">success:</span>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo $response; ?>
        </div>
        </div>
      </div>
<?php } elseif ($response = $this->session->flashdata('stok_sesuai')) { ?>
      <div class="row">
        <div class="col-lg-12" align="center">
        <div class="alert alert-success" role="alert">
          <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
          <span class="sr-only">success:</span>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo $response; ?>
        </div>
        </div>
      </div>
 <?php } ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Update Stok <small></small></h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="btn-group">
            <a href="<?php echo site_url('Produk/update_stok') ?>" class="btn btn-info"><span class="fa fa-plus"></span> Tambahkan Stok</a>  
           </div>
          <div class="btn-group">
            <a href="<?php echo site_url('Produk/stok_keluar') ?>" class="btn btn-info"><span class="fa fa-minus"></span> Kurangi Stok</a>  
          </div>
          <div class="btn-group">
            <a href="<?php echo site_url('Produk/stok_sesuai') ?>" class="btn btn-info"><span class="glyphicon glyphicon-check"></span> Penyesuaian Stok</a>  
         </div>
          <hr class="hr-line-dashed">
          <div class="tab-content">
            <h2>Pengurangan Stok</h2><br>
              <form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>Produk/kurangi_stok" autocomplete="off">
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="exampleInputEmail1">No Bukti Pengurangan Stok</label>
                    <input type="text" class="form-control" readonly="" placeholder="Diisi oleh sistem" id="nobukti" name="nobukti">
                  </div>
                  <div class="form-group">
                    <label>Tanggal</label>
                    <div class="datepicker input-group date">
                        <input type="text" class="form-control" name="tanggal" readonly value="<?php echo(date("Y-m-d")); ?>">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Notes</label>
                    <textarea type="text" class="form-control" id="catatan_tambah" name="catatan" placeholder="Contoh: Belanja Dari Supplier">Stok Keluar</textarea>
                  </div>
                </div>
              <div class="col-md-9">
                <div class="form-group">
                  <label><strong>Masukkan Produk</strong></label>
                  <select id="kurang_jenis_produk" class="form-control" onchange="update_stok_jenis_masukkan('kurang')">
                      <option value="" selected="">- Pilih -</option>
                      <option value="manual">Manual</option>
                      <option value="excel">Upload Excel</option>
                  </select>
                </div>
                <div class="list-table" id="kurang_manual" style="display:none">
                  <br>
                <div class="optionBox">
                    <table id="tabelProduknya" class="table table-hover table-bordered">
                      <thead class="cf">
                        <tr>
                          <th style="width:3%" align="center">#</th>
                          <th style="width:35%">Masukkan Nama Produk / Kode Produk*</th>
                          <th style="width:15%">Stok**</th>
                          <th style="width:5%"></th>
                        </tr></thead>
                      <tbody id="tbody_stok">
                        <tr id="tr1">
                          <td align="center" data-title="#">1</td>
                          <td data-title="Produk*">
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-search"></i></span>
                              <input type="text" class="form-control" style="font-size: 12px" placeholder="Masukkan 4 karakter dalam Nama/SKU" id="cari_produk1" name="nama_produk[]">
                            </div>
                            <input type="hidden" class="form-control" id="kode_produk1" name="kode_produk[]">
                          </td>
                          <td data-title="Stok">
                            <input type="number" id="jumlah_barang1" name="jumlah_barang[]" value="1" class="jumlah form-control large" size="2" style="text-align:right" min="1">
                          </td>
                          <td align="center">
                            <a href="javascript:void(0);" onclick="T_removeElement_awal();" class="btn btn-danger" id="del_row1">
                            <i class="glyphicon glyphicon-minus"></i>
                            </a>
                          </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="form-group col-md-12"><button type="button" class="badge progress-bar-warning" onclick="addProduk();" id="add_rowProduk"><i class="glyphicon glyphicon-plus"></i> Tambah Produk Lain</button></div>  
                <br> <br>
                <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-save"></i> Simpan</button>
                 
                </div>
              </div>  
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once('template/footer.php') ?>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
      $( "#cari_produk1" ).autocomplete({
        autoFocus: true,
        source: "<?php echo site_url('Produk/get_autocomplete/?');?>",
        select: function (e, ui) {
         var produk = ui.item.value;
         var kode = produk.substr(0, 7);  
         $.ajax({
          type: "GET",
          data: "",
          url: "<?php echo site_url('Sales/get_data_produk/'); ?>"+kode,
          success: function(results) {
            var data = JSON.parse(results);
            $('#kode_produk1').val(data.kode_produk);          
          }
         });
        },
        messages: {
          noResults: '',
          results: function() {}
        }
       
      });
  });


  function update_stok_jenis_masukkan(i){
      if($('#'+ i + '_jenis_produk').val() == 'manual') {            
          $('#'+ i + '_manual').show();
          $('#'+ i + '_excel').hide();
      }else {
      
      }
  }

  $('.datepicker').datepicker({
      autoclose: true,
      format: "yyyy-mm-dd",
      todayHighlight: true,
      orientation: "top auto",
      todayBtn: true,
      todayHighlight: true,
  });

  var count_id = 1;

  function T_removeElement_awal() {
    if($('#tbody_stok').children('tr').length > 1) {
      document.getElementById('tbody_stok').removeChild(document.getElementById('tr1'));
      sumall();
    } else {
      alert("Tidak Ada Form Yang Di Hapus ");
    }
  }

  function T_removeElement(i) {
    if($('#tbody_stok').children('tr').length > 1) {
      document.getElementById('tbody_stok').removeChild(document.getElementById('tr' + i));
      count_id--;
      sumall();
    } else {
      alert("Tidak Ada Form Yang Di Hapus ");
    }
  }

function addProduk() {
    count_id++;

    var objNewDiv = document.createElement('tr');
    objNewDiv.setAttribute('id', 'tr' + count_id);
    objNewDiv.innerHTML = '<td align="center">'+count_id+'</td>';
    objNewDiv.innerHTML += '<td data-title="Produk*"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input type="text" class="form-control" style="font-size: 12px" placeholder="Masukkan 4 karakter dalam Nama/SKU" id="cari_produk'+count_id+'" name="nama_produk[]" autocomplete="off"></div><input type="hidden" class="form-control" id="kode_produk'+count_id+'" name="kode_produk[]"></td>';
    objNewDiv.innerHTML += '<td data-title="Stok"><input type="number" min="1" id="jumlah_barang'+count_id+'" name="jumlah_barang[]" value="1" class="jumlah form-control large" size="2" style="text-align:right"></td>';

    objNewDiv.innerHTML += '<td align="center"><a href="javascript:void(0);" onclick="T_removeElement('+count_id+');" class="btn btn-danger" id="del_row'+count_id+'"><i class="glyphicon glyphicon-minus"></i></a></td></tr>';
    document.getElementById('tbody_stok').appendChild(objNewDiv);

    $( "#cari_produk"+count_id ).autocomplete({
      autoFocus: true,
      source: "<?php echo site_url('Produk/get_autocomplete/?');?>",
      select: function (e, ui) {
          var produk = ui.item.value;
          var kode = produk.substr(0, 7);           
           $.ajax({
            type: "GET",
            data: "",
            url: "<?php echo site_url('Sales/get_data_produk/'); ?>"+kode,
            success: function(results) {
              var data = JSON.parse(results);
              $('#kode_produk'+count_id).val(data.kode_produk);
            }
           })
        },
      
      messages: {
        noResults: '',
        results: function() {}
      }
    });
  }
</script>