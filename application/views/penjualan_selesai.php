<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once('template/header.php') ?>
<style type="text/css" media="screen">
	#datatable .dropdown-menu {
    position: relative; float: none; width: 165px; min-width:20px;
  }
</style>
<div class="right_col" role="main">
	<div class="row">
		<div class="x_panel">
      <div class="x_title">
        <h2>PENJUALAN<small></small></h2>
        <div class="clearfix"></div>
      </div>
      <section class="content">
      	<div class="row">
		    	<div class="col-lg-12">
						<div class="bs-docs-section">
					   	<div class="panel with-nav-tabs panel-default">
					      <div class="panel-heading">
				          <ul class="nav nav-pills">
			              <li class=""><a href="<?php echo site_url('Sales') ?>">1. CATAT PENJUALAN <i class="glyphicon glyphicon-chevron-right"></i></a></li>
			              <li class=""><a href="<?php echo site_url('Sales/konfirmasi_pengiriman') ?>">2. KONFIRMASI PENGIRIMAN <i class="glyphicon glyphicon-chevron-right"></i></a></li>
			              <li class="active"><a href="<?php echo site_url('Sales/penjualan_selesai') ?>">3. PENJUALAN SELESAI <i class="glyphicon glyphicon-ok"></i></a></li>
				          </ul>
		       			</div>
		       			<div class="panel-body">
		       				<div class="tab-content">
				            <form method="post" accept-charset="utf-8">
											<table id="datatable" class="table table-bordered table-striped table-hover">
												<thead>
													<tr>
														<th>#</th>
														<th>No Invoice</th>
														<th>Tgl Order</th>
														<th>Pelanggan</th>
														<th>Kota</th>
														<th>Kurir</th>
														<th>Tgl Kirim</th>
														<th>No Resi</th>
														<!-- <th>Aksi</th> -->
													</tr>
												</thead>
												<tbody align="center">
													<?php 
														$no = 1;
														if ($data->num_rows() > 0) {
															foreach ($data->result() as $row) {
																?>
																<tr>
																	<td><?php echo $no++; ?></td>
																	<td><?php echo $row->no_invoice; ?></td>
																	<td width="10%"><?php echo $row->tanggal; ?></td>
																	<td><?php echo $row->nama; ?></td>
																	<td><?php echo $row->kota; ?></td>
																	<td><?php echo $row->kurir; ?></td>
																	<td width="10%"><?php echo $row->tgl_kirim; ?></td>
																	<td><?php echo $row->no_resi; ?></td>
																	<!-- <td>
																			<button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" id="dropdown-button" aria-expanded="false">Aksi <span class="caret"></span></button>
																			<ul class="dropdown-menu">
																				<li><a title="Invoice" data-modal-size="modal-md" class="" href="#"><i class="fa fa-ticket"></i>&nbsp&nbsp&nbsp&nbspInvoice</a></li>
																				<li><a title="Transaksi Batal" href="#" class="" data-modal-size="modal-sm"><i class="fa fa-remove"></i>&nbsp&nbsp&nbsp&nbspBatalkanTransaksi</a></li>
																				<li><a href="#" data-modal-size="modal-sm" class="load-ajax-modal"><i class="fa fa-pencil-square"></i>&nbsp&nbsp&nbsp&nbspUbah Resi</a></li>
																				<li><a data-modal-size="modal-md" class="" href="#"><i class="fa fa-mobile-phone"></i>&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSMS Resi</a></li>
																			</ul>
																	</td> -->
																</tr>
																<?php
															}
														}
													 ?>
												</tbody>
											</table>				
										</form>	
		       				</div>
		       			</div>																						
		       		</div>
		       	</div>
		       </div>
		     </div>
			</section>
		</div>
	</div>
</div>
<?php require_once('template/footer.php') ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#dat').DataTable({
			"processing": true, 
			"pageLength": 10,
			"bLengthChange": false,
			"bInfo": false,
			"bDestroy": true,

			"language": {
	      "emptyTable": "Data Belum Ada!"
	    }
            
		}); 
	});	



</script>