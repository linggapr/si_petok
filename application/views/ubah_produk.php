<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once('template/header.php') ?>
<style media="screen">
  footer {
    margin-top: 0px;
  }
  th {
    text-align: center;
  }
</style>
<div class="right_col" role="main">
  <?php
  	foreach ($produk->result() as $row) {
   ?>
   	<?php echo form_open_multipart('Produk/update');?>
	    <div class="col-md-12">
	      <div class="panel panel-default" id="info-produk" >
	        <div class="panel-body">
	        <label>Ada Varian Ukuran/Warna :</label>
	        <select class="form-control" name="varianchange" id="varian" onchange="varian_change()">
	          <?php if ($row->ukuran != NULL || $row->warna != NULL) {
	          	echo '<option value="Y">Ada</option>';
	          } else {
	          	echo '<option value="N">Tidak</option>';
	          }?>
	        </select>
	      </div>
	    </div>
	    <div class="panel panel-default" id="info-produk" style="">
	      <div class="panel-body">
	        <div class="col-sm-3">
	          <strong>Kategori Produk*</strong>
	          <select class="form-control" name="kategori" id="kategori" onchange="kategorichange()">
	            <?php
	              foreach ($kategori->result() as $ktgori) {
	                if ($row->kode_kategori == $ktgori->kode_kategori) {
					 echo '<option value="'.$ktgori->kode_kategori.'">'.strtoupper($ktgori->nama_kategori).'</option>';
	                }
	              }
	              foreach ($kategori->result() as $ktgori) {
	                if ($row->kode_kategori != $ktgori->kode_kategori) {
					 echo '<option value="'.$ktgori->kode_kategori.'">'.strtoupper($ktgori->nama_kategori).'</option>';
	                }
	              }
	             ?>

	            <option value="0">Tambah Kategori Baru</option>
	          </select>
	          <span class="help-block with-errors"></span>
	        </div>
	        <div class="col-sm-3 kategoribaru" style="display:none;">
	          <strong>Kategori Baru*</strong>
	          <input type="text" class="form-control" name="kategori_baru" id="kategori_baru" placeholder="Kategori Produk"><span class="help-block with-errors"></span>
	        </div>
	        <div class="col-sm-4">
	          <strong>Nama Produk*</strong>
	          <input type="text" class="form-control" name="nama_produk" placeholder="Nama Produk" required="" value="<?php echo $row->nama_produk ?>"><span class="help-block with-errors"></span>
	        </div>
	          <div class="col-md-12">
	          Deskripsi Produk
	            <textarea name="deskripsi" ><?php echo $row->deskripsi ?></textarea>
	          </div>
	        </div>
	    </div>

	    <div class="panel panel-default" id="info-produk" style="">
	      <span class="btn btn-primary"> K E T E R A N G A N </span>
	      <div class="panel-body">
	        <div class="col-sm-4">
	          <strong>SKU (Kode Produk)* </strong>
	           <input type="text" class="form-control" name="kode_produk" readonly value="<?php echo $row->kode_produk ?>"><span class="help-block with-errors"></span>
	        </div>
	        <?php
	        	if ($row->ukuran != NULL || $row->warna != NULL) {
	        		?>

	        <div class="col-sm-4 ukuran">
	          <strong>Ukuran</strong>
	          <input type="text" class="form-control" name="ukuran" value="<?php echo $row->ukuran ?>"> <span class="help-block with-errors"></span>
	        </div>
	        <div class="col-sm-4 warna">
	          <strong>Warna</strong>
	          <input type="text" class="form-control" name="warna" value="<?php echo $row->warna ?>"><span class="help-block with-errors"></span>
	        </div>
	        		<?php
	        	}
	         ?>
	      </div>
	      <div class="panel-body">
	        <div class="col-sm-3">
	          <strong>Gambar Produk</strong>
	          <img src="<?php echo base_url().'uploads/'.$row->gambar; ?>" height="300" width="237" alt="Gambar <?php echo $row->nama_produk ?>">
	           <input type="file" class="form-control" name="gambar"><span class="help-block with-errors">
	           <?php echo $row->gambar ?>
	           	</span>
	        </div>
	        <div class="col-sm-3">
	          <strong>Berat (Gram)*</strong>
	          <input type="number" class="form-control" name="berat" value="<?php echo $row->berat ?>"><span class="help-block with-errors"></span>
	        </div>
	        <div class="col-sm-3">
	          <strong>Jenis*</strong>
	          <select class="form-control" name="jenis">
	            <?php
	              foreach ($jenises->result() as $jenis) {
	                if ($row->jenis == $jenis->jenis) {
					 echo '<option value="'.$jenis->kode_jenis.'">'.strtoupper($jenis->jenis).'</option>';
	                }
	              }
	              foreach ($jenises->result() as $jenis) {
	                if ($row->jenis != $jenis->jenis) {
					 echo '<option value="'.$jenis->kode_jenis.'">'.strtoupper($jenis->jenis).'</option>';
	                }
	              }
	             ?>

	          </select>
	        </div>
	        <div class="col-sm-3">
	          <strong>Stok saat ini</strong>
	          <input type="number" class="form-control" name="stok" readonly value="<?php echo $row->stok ?>"><span class="help-block with-errors"></span>*Perbarui Stok di <i>Update Stok</i>
	        </div>
	      </div>
	      <span class="btn btn-primary"> H A R G A </span>
	      <div class="panel-body">
	        <div class="col-sm-3">
	          <strong>Harga Eceran*</strong>
	          <input type="number" class="form-control" name="harga" value="<?php echo $row->harga ?>"><span class="help-block with-errors"></span>
	        </div>
	        <div class="col-sm-3">
	          <strong>Diskon (%)</strong>
	          <input type="number" class="form-control" name="diskon" value="<?php echo $row->diskon ?>"><span class="help-block with-errors"></span>
	        </div>
	        <div class="col-sm-6 col-md-6">
	          <?php
	          	$harga_grosir = $grosires->result();
              print_r($row->kode_produk);
	          	if ($harga_grosir[0]->kode_produk == $row->kode_produk) {
	          		?>
	          		<span>Harga Grosir</span> <input type="checkbox" checked="">
	          		<table class="table table-bordered table-hover" id="tabel-grosir-no" >
			            <tr><th>Min Order</th><th>Harga</th></tr>
	          		<?php
	          		$i = 1;
	          		foreach ($harga_grosir as $grosir) {
	          		?>
			            <tr>
			                <input type="hidden" value="<?php echo $grosir->id ?>" name="id_grosir<?php echo $i ?>"/>
			                <td>
			                <input type="number" class="form-control" value="<?php echo $grosir->min_order ?>" name="harga_grosir_min_<?php echo $i ?>" required="" />
			                </td>
			                <td>
			                <input type="number" class="form-control" value="<?php echo $grosir->harga_grosir ?>" name="harga_grosir_<?php echo $i ?>" required=""/>
			                </td>
			            </tr>
	          		<?php
	          			$i++;
	          		}
	          		echo '</table>';
	          	} else {
	          		?>
					  <span>Harga Grosir</span> <input type="checkbox" id="grosiran-no" name="grosiran-no" onclick="set_grosir('no')">
			          <table class="table table-bordered table-hover" id="tabel-grosir-no" style="display:none">
			            <tr><th>Min Order</th><th>Harga</th></tr>
			            <tr>
			            	<input type="hidden" name="id_grosir1" value="0">
			                <td>
			                <input type="number" class="form-control" value="0" name="harga_grosir_min_1" id="harga_grosir_min_1" required="" />
			                </td>
			                <td>
			                <input type="number" class="form-control" value="0" name="harga_grosir_1" id="harga_grosir_1" required=""/>
			                </td>
			            </tr>
			            <tr>
			               <td>
			                <input type="number" class="form-control" value="0" name="harga_grosir_min_2" id="harga_grosir_min_2" />
			                </td>
			                <td>
			                <input type="number" class="form-control" value="0" name="harga_grosir_2" id="harga_grosir_2" />
			                </td>
			            </tr>
			            <tr>
			                <td>
			                <input type="number" class="form-control" value="0" name="harga_grosir_min_3" id="harga_grosir_min_3" />
			                </td>
			                <td>
			                <input type="number" class="form-control" value="0" name="harga_grosir_3" id="harga_grosir_3" />
			                </td>
			            </tr>
			            <tr>
			                <td>
			                <input type="number" class="form-control" value="0" name="harga_grosir_min_4" id="harga_grosir_min_4" />
			                </td>
			                <td>
			                <input type="number" class="form-control" value="0" name="harga_grosir_4" id="harga_grosir_4" />
			                </td>
			            </tr>
			          </table>
	          	<?php
	          	}
	           ?>

        	</div>
	      </div>
	      <div class="panel-body">
	        <center>
	          <?php echo anchor('Produk/daftar_produk', 'Kembali',['class'=>'btn btn-danger']); ?>
	          <button class="btn btn-primary" type="reset">Reset</button>
	          <button type="submit" class="btn btn-success">Simpan</button>
	        </center>
	      </div>
	    </div>
	  </div>
  </form>
  <?php
  	}
   ?>

</div>
<?php require_once('template/footer.php') ?>
<script src="<?php echo base_url() ?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
            selector: "textarea",


            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | inserttime preview | forecolor backcolor",


            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],

            templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
            ]
    });

    function kategorichange(){
        if($('#kategori').val() == 0) $('.kategoribaru').show();
        else $('.kategoribaru').hide();
    }

    function varian_change(){
        if($('#varian').val() == 'Y') {
          $('.ukuran').show();
          $('.warna').show();
        } else {
          $('.ukuran').hide();
          $('.warna').hide();
        }

    }
    function set_grosir(id){
        if($('#grosiran-'+id).is(':checked')) {
            $('#tabel-grosir-'+id).show();
        }else{
            $('#tabel-grosir-'+id).hide();
        }
    }
    </script>
