<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once('template/header.php') ?>
<style media="screen">
  footer {
    margin-top: 0px;
  }
  th {
    text-align: center;
  }
  .btn-info {
    background: #21b9bb linear-gradient(#21b9bb,#0fa6a8);
    border-color: #23c6c8;
    color: #FFFFFF;
  }
  #datatable .dropdown-menu {
    position: relative; float: none; width: 110px; min-width:20px;
  }
</style>
<div class="right_col" role="main">
  <div class="row">
  <?php if ($response = $this->session->flashdata('add_produk')) { ?>
        <div class="row">
            <div class="col-lg-12" align="center">
            <div class="alert alert-success" role="alert">
              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
              <span class="sr-only">success:</span>
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <?php echo $response; ?>
            </div>
            </div>
          </div>
    <?php } elseif ($response = $this->session->flashdata('edit_produk')) { ?>
             <div class="row">
            <div class="col-lg-12" align="center">
            <div class="alert alert-warning" role="alert">
              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
              <span class="sr-only">success:</span>
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <?php echo $response; ?>
            </div>
            </div>
          </div>
    <?php } elseif ($response = $this->session->flashdata('delete_produk')) { ?>
          <div class="row">
            <div class="col-lg-12" align="center">
            <div class="alert alert-danger" role="alert">
              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
              <span class="sr-only">success:</span>
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <?php echo $response; ?>
            </div>
            </div>
          </div>
     <?php } ?>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      <div class="x_title">
	        <h2>Daftar Produk <small></small></h2>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">
           <div class="btn-group">
             <a href="<?php echo base_url() ?>Produk/tambah_produk" class="btn btn-primary"><span class="fa fa-plus"></span> Tambah Produk</a>  
           </div>
           <div class="btn-group">
             <a href="<?php echo base_url() ?>Produk/daftar_kategori" class="btn btn-info"><span class="fa fa-list"></span> Daftar Kategori</a>
           </div>
	         <div class="btn-group">
              <a href="" data-toggle="dropdown" class="btn btn-info dropdown-toggle" aria-expanded="false"><i class="glyphicon glyphicon-th"></i> Lainnya <span class="caret"></span></a>
                <ul class="dropdown-menu in">
                    <br>
                    <li><a href="<?php echo base_url() ?>Produk/update_stok" ><i class="fa fa-edit"></i>&nbsp&nbsp&nbsp&nbspUpdate Stok</a></li> <br>
                    <li><a ><i class="fa fa-file-excel-o"></i>&nbsp&nbsp&nbsp&nbspUpload Data (Excel)</a></li> <br>
                </ul>
          </div>
          <br> <br>
	        <table id="datatable" class="table table-striped table-bordered">
	          <thead>
	            <tr>
	              <th>No</th>
	              <th>Nama Produk</th>
	              <th>Jenis</th>
	              <th>Harga Ecer</th>
                <th>Grosir</th>
	              <th>Stok</th>
	              <th>Aksi</th>
	          </thead>
	          <tbody>
	          	 <?php
              $no = 1;
              if ($data->num_rows() > 0) {
                foreach ($data->result() as $row) {
                  ?>
                  <tr>
                    <td align="center"><?php echo $no++; ?></td>
                    <td><b><?php echo ucfirst($row->nama_produk); ?></b> <?php if ($row->diskon != 0) { echo '<span class="badge progress-bar-danger">DISC '.$row->diskon .'%</span>'; } ?> <br> <small>
                      <?php 
                        echo $row->kode_produk; 
                        if ($row->ukuran != NULL) {
                          echo ' | Ukuran: '.$row->ukuran;
                        } 
                        if ($row->warna != NULL) {
                          echo ' | Warna: '.$row->warna;
                        }
                       ?>
                        
                      </small></td>
                   	<td align="center"><?php echo $row->jenis; ?></td>
                   	<td><?php echo 'Rp. '.$row->harga; ?></td>
                    <td align="center" style="vertical-align: middle"><?php 
                      if ($row->grosir == 'YA') {
                        echo '<i class="fa fa-circle" style="color:#2EBDFF"></i>';
                      } else {
                        echo '<i class="fa fa-circle" style="color:#888888"></i>';
                      }
                    ?></td>
                   	<td><?php echo $row->stok; ?></td>
					          <td align="center">
		                <li style="list-style: none" class="dropdown">
		                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							    Aksi
							</button>
		                    <ul class="dropdown-menu">
		                        <li><a href="<?php echo base_url().'Produk/ubah_produk/'.$row->kode_produk;?>"><span class="fa fa-pencil"></span>&nbsp&nbsp&nbsp&nbspEdit</a></li>
		                        <li><a href="<?php echo base_url().'Produk/hapus_produk/'.$row->kode_produk;?>"><span class="fa fa-trash"></span>&nbsp&nbsp&nbsp&nbspHapus</a></li>
		                        <li><a onclick="generate_barcode('<?php echo base_url()?>','<?php echo $row->kode_produk ?>','<?php echo $row->nama_produk ?>')"><span class="fa fa-barcode"></span>&nbsp&nbsp&nbsp&nbspBarcode</a></li>
		                    </ul>
		                </li>				            
					          </td>
                  </tr>
                  <?php
                }
              }
               ?>
	          </tbody>
	        </table>

          <div class="modal fade" id="myModal" role="dialog">
              <div class="modal-dialog ">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title"></h4>
                      </div>
                      <div id="modal-body">
                          <p align="center"><img style="margin: 20px;" width="260" id="barcode" alt="<?php echo $row->barcode ?>"></p>
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                  </div>
              </div>
          </div>
	      </div>
	    </div>
	  </div>
	</div>
</div>
<?php require_once('template/footer.php') ?>
<script type="text/javascript">
  function generate_barcode(base_url, kode_produk, nama_produk) {
      var mymodal = $('#myModal');
      document.getElementById("barcode").src=""+base_url+"uploads/barcode/barcode_"+kode_produk+".png";
      mymodal.find('.modal-title').text(kode_produk +' - '+ nama_produk);
      mymodal.modal('show');
  }

</script>