<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once('template/header.php') ?>
<style media="screen">
  footer {
    margin-top: 0px;
  }
  th {
    text-align: center;
  }
</style>
<div class="right_col" role="main">
  <h1>Pengaturan</h1>
  <div class="col-lg-12">
  <div class="clearfix">&nbsp;</div>
    <div class="panel with-nav-tabs panel-default">
      <div class="panel-heading">
      <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="#kontak" data-toggle="tab">Kontak &amp; RekapShop Profile</a></li>
        <li role="presentation"><a href="#bank" data-toggle="tab">Bank</a></li>
      </ul>
      </div>
      <div class="panel-body">
        <div class="tab-content">
          <div class="ibox float-e-margins tab-pane active" id="kontak">
            <form action="<?php echo site_url('Pengaturan/simpan_data_user') ?>" method="post" accept-charset="utf-8">
              <div class="ibox-content">
              <div class="row">
                <?php foreach ($data_user->result() as $pemilik): ?>
                  <div class="col-md-6 col-md-12">
                      <div class="form-group no-margins">
                        <label class="control-label" for="site_phone">Nama Admin*</label>
                        <input name="nama_admin" id="nama_admin" class="form-control" value="<?php echo $pemilik->nama?>" type="text">
                      </div>
                    </div>
                    <div class="col-md-6 col-md-12">
                      <div class="form-group no-margins">
                        <label class="control-label" for="site_phone">Nama Toko*</label>
                        <input name="nama_toko" id="nama_toko" class="form-control" value="<?php echo $pemilik->nama_toko ?>" type="text">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group no-margins">
                        <label class="control-label" for="deskripsi">Deskripsi Toko</label>
                        <textarea type="text" name="deskripsi" id="deskripsi" class="form-control"><?php echo $pemilik->deskripsi_toko ?></textarea>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group no-margins">
                        <label class="control-label" for="deskripsi">Alamat Toko</label>
                        <textarea name="alamat" class="form-control"><?php echo $pemilik->alamat ?></textarea>
                      </div>
                    </div>
                    <div class="col-md-6 col-md-12">
                    <div class="form-group no-margins">
                      <div class="form-group no-margins">
                        <label class="control-label" for="site_phone">Provinsi* : <strong></strong></label>
                        <select name="provinsi" id="provinsi" class="form-control" onchange="provinsiSelected(this)">
                          <option>- Provinsi -</option>
                          <?php foreach ($province->rajaongkir->results as $prov): ?>
                            <option value="<?php echo $prov->province_id; ?>"><?php echo $prov->province; ?></option>
                          <?php endforeach; ?>
                          <input type="hidden" name="provinsi_selected" id="provinsi_selected" value="">
                        </select>
                      </div>  
                    </div>
                    <div class="form-group no-margins">
                      <div class="form-group no-margins">
                        <label class="control-label" for="site_phone">Kota* : <strong></strong></label>
                        <select name="kota" id="kota" onchange="kotaSelected(this)" class="form-control">
                          <option>- Kota -</option>
                          <input type="hidden" id="kota_selected" name="kota_selected" value="">
                        </select>
                      </div>  
                    </div>
                    </div>
                    <div class="col-md-6 col-md-12">
                      <div class="form-group no-margins">
                        <label class="control-label" for="site_phone">Kecamatan* :</label>
                        <input id="kecamatan" name="kecamatan" value="<?php echo $pemilik->kecamatan ?>" class="form-control" type="text">                    
                      </div>
                    </div>
                    <div class="col-md-6 col-md-12">
                      <div class="form-group no-margins">
                        <label class="control-label" for="site_phone">No Handphone*</label>
                        <input name="nohape" id="no_hp" class="form-control" value="<?php echo $pemilik->no_hp ?>" type="text">
                      </div>
                    </div>
                    <div class="col-md-12"><hr></div>
                    <div class="col-md-6 col-md-12">
                      <div class="form-group no-margins">
                        <label class="control-label" for="site_email">Email*</label>
                        <input name="email" id="email" class="form-control" value="<?php echo $pemilik->email ?>" type="email">
                      </div>
                    </div>
                <?php endforeach ?>
                <?php foreach ($data_sosmed->result() as $sosmed): ?>
                  <div class="col-md-6 col-md-12">
                    <div class="form-group no-margins">
                      <label class="control-label" for="web"><?php echo $sosmed->sosmed ?></label>
                      <input name="sosmed[]" class="form-control" value="<?php echo $sosmed->link ?>" type="text">
                      <input type="hidden" name="id_sosmed[]" value="<?php echo $sosmed->id ?>">
                    </div>
                  </div>
                <?php endforeach ?>
              </div>
            </div>
            <div>
              <br><br><br>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>      
            </form>
          </div>
          <div class="ibox float-e-margins tab-pane" id="bank">
            <form action="<?php echo site_url('Pengaturan/simpan_data_bank') ?>" method="post" accept-charset="utf-8">
              <div class="ibox-content">
              <div class="row">
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th width="5%">No</th>
                      <th width="25%">Bank</th>
                      <th width="55%">No Rekening</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; ?>
                    <?php foreach ($data_bank->result() as $bank): ?>
                      <tr>
                        <td align="center"><?php echo $no++; ?></td>
                        <td><strong><?php echo $bank->nama_bank; ?></strong></td>
                        <td><input type="text" name="no_rek[]" class="form-control" value="<?php echo $bank->no_rek; ?>" placeholder="1051251115 (John Doe)"> 
                          <input type="hidden" name="id_bank[]" value="<?php echo $bank->id_bank; ?>"></td>
                      </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div>
              <br><br><br>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once('template/footer.php') ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
$(document).ready(function() {
    $.get('<?php echo site_url('Pengaturan/get_data_byid/1') ?>', function(resp){
      var data = JSON.parse(resp);
      var prov = data.provinsi;
      var kota = data.kota;
      $('#provinsi_selected').val(prov);
      $('#kota_selected').val(kota);
      prov = prov.replace(/\s/g, '');
      kota = kota.replace(/\s/g, '');
      $.get('<?php echo site_url('Sales/get_prov_id/'); ?>'+prov, function(prov_id) {
        $('#provinsi').val(prov_id);                 
        $.get('<?php echo site_url('Sales/get_city_id/'); ?>'+kota+'/'+prov_id, function(city_id) {
          $.get('<?php echo site_url('Sales/get_city_penerima/') ?>'+prov_id+'/'+city_id, function(resp){
            $('#kota').html(resp);                                   
          });
        });
      }); 
    });

    $('#provinsi').change(function(){
      var province_id=$('#provinsi').val();
      $.get('<?php echo site_url('Pengaturan/get_city_by_province/') ?>'+province_id, function(resp){
        $('#kota').html(resp);
      });
    });
  });

  function provinsiSelected(ddl) {
    document.getElementById('provinsi_selected').value = ddl.options[ddl.selectedIndex].text;
    document.getElementById('ubah_provinsi_selected').value = ddl.options[ddl.selectedIndex].text;
  }

  function kotaSelected(ddl) {
    document.getElementById('kota_selected').value = ddl.options[ddl.selectedIndex].text;
    document.getElementById('ubah_kota_selected').value = ddl.options[ddl.selectedIndex].text;
  }
</script>