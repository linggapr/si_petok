<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once('template/header.php') ?>
<style type="text/css" media="screen">
</style>
<div class="right_col" role="main">
	<div class="row">
		<div class="x_panel">
      <div class="x_title">
        <h2>HISTORY TRANSAKSI<small></small></h2>
        <div class="clearfix"></div>
      </div>
      <section class="content">
      	<div class="row">
		    	<div class="col-lg-12">
						<div class="bs-docs-section">
					   	<div class="panel with-nav-tabs panel-default">
					      <div class="panel-heading">
		       			</div>
		       			<div class="panel-body">
		       				<div class="tab-content">
				            <form method="post" accept-charset="utf-8">
											<table id="datatable" class="table table-bordered table-striped table-hover">
												<thead>
													<tr>
														<th>#</th>
														<th>No Invoice</th>
														<th>Tgl Order</th>
														<th>Pelanggan</th>
														<th>Kota</th>
														<th>Kurir</th>
														<th>Tgl Kirim</th>
														<th>No Resi</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody align="center">
													<?php 
														$no = 1;
														if ($data->num_rows() > 0) {
															foreach ($data->result() as $row) {
																?>
																<tr>
																	<td><?php echo $no; ?></td>
																	<td><?php echo $row->no_invoice; ?></td>
																	<td><?php echo $row->tanggal; ?></td>
																	<td><?php echo $row->nama; ?></td>
																	<td><?php echo $row->kota; ?></td>
																	<td><?php echo $row->kurir; ?></td>
																	<td><?php if ($row->tgl_kirim == "") { echo '-';} else {echo $row->tgl_kirim;} ?></td>
																	<td><?php if ($row->no_resi == "") { echo '-';} else {echo $row->no_resi;} ?></td>
																	<?php 
																		switch ($row->status_penjualan) {
																			case "MENUNGGU":
																					?>
																						<td><span class="badge progress-bar-info">MENUNGGU</span></td>
																					<?php
																				break;
																			case "BATAL":
																					?>
																						<td><span class="badge progress-bar-danger">BATAL</span></td>
																					<?php
																				break;
																			case "BERHASIL":
																					?>
																						<td><span class="badge progress-bar-success">BERHASIL</span></td>
																					<?php
																				break;
																		}
																	?>
																</tr>
																<?php
																$no++;
															}
														}
													 ?>
												</tbody>
											</table>				
										</form>	
		       				</div>
		       			</div>																						
		       		</div>
		       	</div>
		       </div>
		     </div>
			</section>
		</div>
	</div>
</div>
<?php require_once('template/footer.php') ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
