<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once('template/header.php') ?>
<style media="screen">
  footer {
    margin-top: 0px;
  }
  .dropdown-menu {
    min-width:20px;
  }
  .btn-info {
    background: #21b9bb linear-gradient(#21b9bb,#0fa6a8);
    border-color: #23c6c8;
    color: #FFFFFF;
  }

  .hr-line-dashed {
    border-top: 1px dashed #d8d8d8;
    color: #ffffff;
    background-color: #ffffff;
    height: 1px;
    margin: 10px 0;
  }
</style>

<div class="right_col" role="main">
  <div class="row">
    <?php if ($response = $this->session->flashdata('tambah_pengeluaran')) { ?>
    <div class="row">
        <div class="col-lg-12" align="center">
        <div class="alert alert-success" role="alert">
          <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
          <span class="sr-only">success:</span>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo $response; ?>
        </div>
        </div>
      </div>
<?php } ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>PENGELUARAN <small></small></h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="btn-group">
            <a href="<?php echo site_url('Pengeluaran') ?>" class="btn btn-info"> Input Pengeluaran</a>  
           </div>
          <div class="btn-group">
            <a href="<?php echo site_url('Pengeluaran/jenis_pengeluaran') ?>" class="btn btn-info"> Jenis Pengeluaran</a>  
          </div>
          <div class="btn-group">
            <a href="<?php echo site_url('Pengeluaran/history_pengeluaran') ?>" class="btn btn-info"><span class="glyphicon glyphicon-list"></span> History Pengeluaran</a>  
         </div>
          <hr class="hr-line-dashed">
          <div class="tab-content">
            <form method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>Pengeluaran/tambah_pengeluaran" autocomplete="off">
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="exampleInputEmail1">No Bukti</label>
                    <input type="text" class="form-control" readonly="" placeholder="Diisi oleh sistem" name="nobukti">
                  </div>
                  <div class="form-group">
                    <label>Tanggal</label>
                    <div class="datepicker input-group date">
                        <input type="text" class="form-control" name="tanggal" readonly value="<?php echo(date("Y-m-d")); ?>">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Dikeluarkan Dari</label>
                    <select class="form-control" name="dikeluarkan_dari">
                      <option value="BNI">BNI</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Notes</label>
                    <textarea type="text" class="form-control" id="catatan_tambah" name="catatan" placeholder="Catatan..."></textarea>
                  </div>
                </div>
              <div class="col-md-9">
                <div class="list-table">  
                  <br>
                  <div class="optionBox">
                    <table id="tabelProduknya" class="table table-hover table-bordered">
                      <thead class="cf">
                        <tr>
                          <th style="width:3%" align="center">#</th>
                          <th style="width:35%">Jenis Pengeluaran</th>
                          <th style="width:15%">Jumlah (Rp)</th>
                          <th style="width:5%"></th>
                        </tr></thead>
                      <tbody id="tbody_pengeluaran">
                        <tr id="tr1">
                          <td align="center" data-title="#">1</td>
                          <td data-title="Produk*">
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-search"></i></span>
                              <input type="text" class="form-control" style="font-size: 12px" placeholder="Masukkan Jenis Pengeluaran" id="jenis_pengeluaran1" name="jenis_pengeluaran[]">
                            </div>
                            <input type="hidden" class="form-control" id="kode_pengeluaran1" name="kode_pengeluaran[]">
                          </td>
                          <td data-title="Stok">
                            <input type="number" id="jumlah1" name="jumlah[]" value="1" class="jumlah form-control large" size="2" style="text-align:right" min="1">
                          </td>
                          <td align="center">
                            <a href="javascript:void(0);" onclick="T_removeElement_awal();" class="btn btn-danger" id="del_row1">
                            <i class="glyphicon glyphicon-minus"></i>
                            </a>
                          </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="form-group col-md-12"><button type="button" class="badge progress-bar-warning" onclick="addJenis();" id="add_rowProduk"><i class="glyphicon glyphicon-plus"></i> Tambah Jenis Pengeluaran Lain</button></div>  
                <br> <br>
                <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-save"></i> Simpan</button>
              </div>  
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once('template/footer.php') ?>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
      $( "#jenis_pengeluaran1" ).autocomplete({
        autoFocus: true,
        source: "<?php echo site_url('Pengeluaran/get_autocomplete/?');?>",
        select: function (e, ui) {
         var produk = ui.item.value;
         var kode = produk.substr(1, 2);
         $('#kode_pengeluaran1').val(kode);
        },
        messages: {
          noResults: '',
          results: function() {}
        }
       
      });
  });


  $('.datepicker').datepicker({
      autoclose: true,
      format: "yyyy-mm-dd",
      todayHighlight: true,
      orientation: "top auto",
      todayBtn: true,
      todayHighlight: true,
  });

  var count_id = 1;

  function T_removeElement_awal() {
    if($('#tbody_pengeluaran').children('tr').length > 1) {
      document.getElementById('tbody_pengeluaran').removeChild(document.getElementById('tr1'));
      sumall();
    } else {
      alert("Tidak Ada Form Yang Di Hapus ");
    }
  }

  function T_removeElement(i) {
    if($('#tbody_pengeluaran').children('tr').length > 1) {
      document.getElementById('tbody_pengeluaran').removeChild(document.getElementById('tr' + i));
      count_id--;
      sumall();
    } else {
      alert("Tidak Ada Form Yang Di Hapus ");
    }
  }

function addJenis() {
    count_id++;

    var objNewDiv = document.createElement('tr');
    objNewDiv.setAttribute('id', 'tr' + count_id);
    objNewDiv.innerHTML = '<td align="center">'+count_id+'</td>';
    objNewDiv.innerHTML += '<td data-title="Produk*"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input type="text" class="form-control" style="font-size: 12px" placeholder="Masukkan Jenis Pengeluaran" id="jenis_pengeluaran'+count_id+'" name="jenis_pengeluaran[]" autocomplete="off"></div><input type="hidden" class="form-control" id="kode_pengeluaran'+count_id+'" name="kode_pengeluaran[]"></td>';
    objNewDiv.innerHTML += '<td data-title="Stok"><input type="number" min="1" id="jumlah'+count_id+'" name="jumlah[]" value="1" class="jumlah form-control large" size="2" style="text-align:right"></td>';

    objNewDiv.innerHTML += '<td align="center"><a href="javascript:void(0);" onclick="T_removeElement('+count_id+');" class="btn btn-danger" id="del_row'+count_id+'"><i class="glyphicon glyphicon-minus"></i></a></td></tr>';
    document.getElementById('tbody_pengeluaran').appendChild(objNewDiv);

    $( "#jenis_pengeluaran"+count_id ).autocomplete({
      autoFocus: true,
        source: "<?php echo site_url('Pengeluaran/get_autocomplete/?');?>",
        select: function (e, ui) {
         var produk = ui.item.value;
         var kode = produk.substr(1, 2);
         $('#kode_pengeluaran'+count_id).val(kode);
        },
        messages: {
          noResults: '',
          results: function() {}
        }
    });
  }
</script>