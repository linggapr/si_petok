<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once('template/header.php') ?>
<style media="screen">
  footer {
    margin-top: 0px;
  }
  th {
    text-align: center;
  }
</style>
<div class="right_col" role="main">
  
  <?php echo form_open_multipart('Produk/added_produk');?>
    <div class="col-md-12">
      <div class="panel panel-default" id="info-produk" >
        <div class="panel-body">
        <label>Ada Varian Ukuran/Warna :</label>
        <select class="form-control" name="varianchange" id="varian" onchange="varian_change()">
          <option value="N">Tidak</option>
          <option value="Y">Ada</option>
        </select>
      </div>                   
    </div>                     
    <div class="panel panel-default" id="info-produk" style="">
      <div class="panel-body">    
        <div class="col-sm-3">
          <strong>Kategori Produk*</strong>
          <select class="form-control" name="kategori" id="kategori" onchange="kategorichange()">
            <?php 
              foreach ($kategori->result() as $row) {
                ?>
                <option value="<?php echo $row->kode_kategori; ?>"><?php echo strtoupper($row->nama_kategori) ?></option>
             <?php
              }
             ?>            
            
            <option value="0">Tambah Kategori Baru</option>                       
          </select>
          <span class="help-block with-errors"></span>
        </div>
        <div class="col-sm-3 kategoribaru" style="display:none;">
          <strong>Kategori Baru*</strong>
          <input type="text" class="form-control" name="kategori_baru" id="kategori_baru" placeholder="Kategori Produk"><span class="help-block with-errors"></span>
        </div>
        <div class="col-sm-4">
          <strong>Nama Produk*</strong>
          <input type="text" class="form-control" name="nama_produk" required placeholder="Nama Produk" ><span class="help-block with-errors"></span>
        </div>
          <div class="col-md-12">
          Deskripsi Produk
            <textarea name="deskripsi" ></textarea>
          </div>
        </div>
    </div>  
    
    <div class="panel panel-default" id="info-produk" style="">
      <span class="btn btn-primary"> K E T E R A N G A N </span>
      <div class="panel-body">    
        <div class="col-sm-4">
          <strong>SKU (Kode Produk)* </strong>
           <input type="text" class="form-control" name="kode_produk" disabled="" placeholder="Diisi Oleh Sistem"><span class="help-block with-errors"></span>
        </div>  
        <div class="col-sm-4 ukuran" style="display:none;">
          <strong>Ukuran</strong>
          <input type="text" class="form-control" name="ukuran" <span class="help-block with-errors"></span>
        </div>
        <div class="col-sm-4 warna" style="display:none;">
          <strong>Warna</strong>
          <input type="text" class="form-control" name="warna" ><span class="help-block with-errors"></span>
        </div>
      </div>
      <div class="panel-body">    
        <div class="col-sm-3">
          <strong>Gambar Produk</strong>
           <input type="file" class="form-control" name="gambar"><span class="help-block with-errors"></span>
        </div>  
        <div class="col-sm-3">
          <strong>Berat (Gram)*</strong>
          <input type="number" class="form-control" name="berat" value="0"><span class="help-block with-errors"></span>
        </div> 
        <div class="col-sm-3">
          <strong>Jenis*</strong>
          <select class="form-control" name="jenis">
            <option value="1" selected="">Ready Stok</option>
            <option value="2">Pre Order</option>
            <option value="3">Dropship</option>                       
          </select>
        </div>
        <div class="col-sm-3">
          <strong>Stok</strong>
          <input type="number" class="form-control" name="stok"  value="0"><span class="help-block with-errors"></span>
        </div>  
      </div>
      <span class="btn btn-primary"> H A R G A </span>
      <div class="panel-body">    
        <div class="col-sm-3">
          <strong>Harga Eceran*</strong> 
          <input type="number" class="form-control" name="harga" value="0"><span class="help-block with-errors"></span>
        </div> 
        <div class="col-sm-3">
          <strong>Diskon (%)</strong>
          <input type="number" class="form-control" name="diskon" value="0"><span class="help-block with-errors"></span>
        </div>
        <div class="col-sm-6 col-md-6">
          <span>Harga Grosir</span> <input type="checkbox" id="grosiran-no" name="grosiran-no" onclick="set_grosir('no')">
          <table class="table table-bordered table-hover" id="tabel-grosir-no" style="display:none">
            <tr><th>Min Order</th><th>Harga</th></tr>
            <tr>
                <td>
                <input type="text" class="form-control" value="0" name="harga_grosir_min_1" id="harga_grosir_min_1"  />
                </td>
                <td>
                <input type="text" class="form-control" value="0" name="harga_grosir_1" id="harga_grosir_1" />
                </td>
            </tr>
            <tr>
               <td>
                <input type="text" class="form-control" value="0" name="harga_grosir_min_2" id="harga_grosir_min_2" />
                </td>
                <td>
                <input type="text" class="form-control" value="0" name="harga_grosir_2" id="harga_grosir_2" />
                </td>
            </tr>
            <tr>
                <td>
                <input type="text" class="form-control" value="0" name="harga_grosir_min_3" id="harga_grosir_min_3" />
                </td>
                <td>
                <input type="text" class="form-control" value="0" name="harga_grosir_3" id="harga_grosir_3" />
                </td>
            </tr>
            <tr>
                <td>
                <input type="text" class="form-control" value="0" name="harga_grosir_min_4" id="harga_grosir_min_4" />
                </td>
                <td>
                <input type="text" class="form-control" value="0" name="harga_grosir_4" id="harga_grosir_4" />
                </td>
            </tr>
          </table>    
        </div>
      </div>
      <div class="panel-body">
        <center>
          <?php echo anchor('Produk/daftar_produk', 'Kembali',['class'=>'btn btn-danger']); ?>
          <button class="btn btn-primary" type="reset">Reset</button>
          <button type="submit" class="btn btn-success">Simpan</button>
        </center>
      </div>
    </div>  
  </div>
  </form>
</div>
<?php require_once('template/footer.php') ?>
<script src="<?php echo base_url() ?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
            selector: "textarea",

 
            toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | inserttime preview | forecolor backcolor",
            
 
            menubar: false,
            toolbar_items_size: 'small',
 
            style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
 
            templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
            ]
    });

    function kategorichange(){
        if($('#kategori').val() == 0) $('.kategoribaru').show();
        else $('.kategoribaru').hide();
    }

    function varian_change(){
        if($('#varian').val() == 'Y') {
          $('.ukuran').show();
          $('.warna').show();
        } else {
          $('.ukuran').hide();
          $('.warna').hide();
        } 
        
    }
    function set_grosir(id){
        if($('#grosiran-'+id).is(':checked')) {
            $('#tabel-grosir-'+id).show();  
        }else{
            $('#tabel-grosir-'+id).hide();
        }
    }
    </script>