<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once('template/header.php') ?>
<style media="screen">
  footer {
    margin-top: 0px;
  }
  .dropdown-menu {
    min-width:20px;
  }
  .btn-info {
    background: #21b9bb linear-gradient(#21b9bb,#0fa6a8);
    border-color: #23c6c8;
    color: #FFFFFF;
  }

  .hr-line-dashed {
    border-top: 1px dashed #d8d8d8;
    color: #ffffff;
    background-color: #ffffff;
    height: 1px;
    margin: 10px 0;
  }
</style>

<div class="right_col" role="main">
  <div class="row">
    <?php if ($response = $this->session->flashdata('tambah_jenis_pemasukkan')) { ?>
    <div class="row">
        <div class="col-lg-12" align="center">
        <div class="alert alert-success" role="alert">
          <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
          <span class="sr-only">success:</span>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo $response; ?>
        </div>
        </div>
      </div>
    <?php } elseif ($response = $this->session->flashdata('pemasukkan_batal')) {?>
      <div class="row">
          <div class="col-lg-12" align="center">
          <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <span class="sr-only">success:</span>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $response; ?>
          </div>
          </div>
        </div>
    <?php  } ?>
    
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>History Pemasukkan <small></small></h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="btn-group">
            <a href="<?php echo site_url('Pemasukkan') ?>" class="btn btn-info"> Input Pemasukkan</a>  
           </div>
          <div class="btn-group">
            <a href="<?php echo site_url('Pemasukkan/jenis_pemasukkan') ?>" class="btn btn-info"> Jenis Pemasukkan</a>  
          </div>
          <div class="btn-group">
            <a href="<?php echo site_url('Pemasukkan/history_pemasukkan') ?>" class="btn btn-info"><span class="glyphicon glyphicon-list"></span> History Pemasukkan</a>  
         </div>
          <hr class="hr-line-dashed">
          <div class="panel-body">
            <div class="tab-content">
              <form method="post" accept-charset="utf-8">
                <table id="datatable" class="table table-bordered table-striped table-hover">
                  <thead>
                    <tr>
                      <th width="5%">#</th>
                      <th width="10%">No Bukti</th>
                      <th width="15%">Tanggal</th>
                      <th>Catatan</th>
                      <th width="15%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      $no = 1;
                      if ($data->num_rows() > 0) {
                        foreach ($data->result() as $row) {
                          ?>
                          <tr>
                            <td align="center"><?php echo $no++ ?></td>
                            <td><?php echo $row->no_bukti ?></td>
                            <td align="center"><?php echo $row->tanggal ?></td>
                            <td><?php echo $row->catatan ?></td>
                            <td align="center">
                              <button type="button" class="ubah_data btn btn-sm btn-info" data-toggle="modal" data-id="<?php echo $row->no_bukti; ?>">Detail
                              </button>
                                <a href="<?php echo site_url('Pemasukkan/pemasukkan_batal/'.$row->no_bukti.' ') ?>" class="btn btn-sm btn-danger" title="Hapus">Batal</a>
                            </td>
                          </tr>
                          <?php
                        }
                      }
                     ?>
                  </tbody>
                </table>        
              </form> 
            </div>
          </div>  
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 350px" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Detail Histori Pemasukkan</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-sm-12 no-padding">
                <div class="form-group has-feedback">
                  <label class="col-sm-12" for="password">No Bukti</label>
                  <div class="col-sm-12">
                    <input id="no_bukti" name="no_bukti" class="form-control" readonly="" autofocus="" type="text">
                    <span class="help-block with-errors"></span>
                  </div>
                </div>
              <div class="form-group has-feedback">
                <label class="col-sm-12" for="tanggal">Tanggal</label>
                <div class="col-sm-12">
                  <input id="tanggal" name="tanggal" class="form-control" value="" readonly=""  autofocus="" type="text">
                  <span class="help-block with-errors"></span>
                </div>
              </div>
              <div class="form-group has-feedback">
                <label class="col-sm-12" for="tanggal">Penerima</label>
                <div class="col-sm-12">
                  <input id="penerima" name="penerima" class="form-control" value="" readonly="" type="text">
                  <span class="help-block with-errors"></span>
                </div>
              </div>
              <div class="form-group has-feedback">
                <label class="col-sm-12" for="catatan">Catatan</label>
                <div class="col-sm-12">
                  <input id="catatan" name="catatan" value="" readonly="" class="form-control" required="" autofocus="" type="text">
                  <span class="help-block with-errors"></span>
                </div>
              </div>
              <div class="form-group has-feedback">
                <label class="col-sm-12" for="catatan">Pemasukkan</label>
                <div class="col-sm-12">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>#</th><th>Nama</th><th>Jumlah (Rp)</th>
                      </tr>
                    </thead>
                    <tbody class="detailBody">
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<?php require_once('template/footer.php') ?>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.ubah_data').click(function() {
      $('.table').find('.detailBody').html("");
      var no_bukti = $(this).data('id');
      var no = 1;
      $.get('<?php echo site_url('Pemasukkan/get_history_pemasukkan/') ?>'+no_bukti, function(resp){
        var data = JSON.parse(resp);
        $('#no_bukti').val(data['0'].no_bukti);
        $('#tanggal').val(data['0'].tanggal);
        $('#penerima').val(data['0'].penerima);
        $('#catatan').val(data['0'].catatan);
        for (var i = 0; i < data.length; i++) {
          $('.table').find('.detailBody').append("<tr><td>"+no+"</td><td>"+data[i].nama_pemasukkan+"</td><td align='right'>"+data[i].jumlah+"</td></tr>");
          no++;
        }
        $('.table').find('tfoot').html('<tr><td></td><td align="right"><strong>Total</strong></td><td align="right"><strong>'+data['0'].total+'</strong></td></tr>');
        $('#detailModal').modal('show');
      }); 
    });
  });
</script>