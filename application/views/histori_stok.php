<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once('template/header.php') ?>
<style media="screen">
  footer {
    margin-top: 0px;
  }
  th {
    text-align: center;
  }
  .dropdown-menu {
    min-width:20px;
  }
  .btn-info {
    background: #21b9bb linear-gradient(#21b9bb,#0fa6a8);
    border-color: #23c6c8;
    color: #FFFFFF;
  }
</style>
<div class="right_col" role="main">
  <div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
	      <div class="x_title">
	        <h2>Riwayat Update Stok <small></small></h2>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">
	      	<table id="datatable" class="table table-striped table-bordered">
		      	<thead>
		      		<th>No</th>
		      		<th>No Bukti</th>
		      		<th>Tanggal</th>
		      		<th>Catatan</th>
		      		<th>Jenis</th>
		      		<th>Jumlah Stok</th>
		      		<th>Detail</th>
		      	</thead>
		      	<tbody>
		      		<?php 
		      		$no = 1;
		      		foreach ($history->result() as $row) {
		      		   ?>
					<tr>
						<td align="center"><?php echo $no++ ?></td>
						<td><?php echo $row->kode_history ?></td>
						<td><?php echo $row->tanggal ?></td>
						<td><?php echo $row->catatan ?></td>
						<td><?php echo ucfirst(strtolower($row->jenis)) ?></td>
						<td><?php echo $row->jumlah ?></td>
						<td align="center"><button class="btn btn-info" 
							onclick="detailModal('<?php echo $row->kode_history ?>','<?php echo $row->tanggal ?>','<?php echo $row->catatan ?>','<?php echo ucfirst(strtolower($row->jenis)) ?>','<?php echo $row->nama_produk ?>','<?php echo $row->jumlah ?>')"><span class="fa fa-info-circle"></span>    <strong>Detail</strong></button></td>
					</tr>
		      		<?php
		      		}
		      		 ?>
		      	</tbody>
	      	</table>	
	      </div>
	     </div>
	  </div>
  </div>
</div>
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog modal-sm">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">DETAIL HISTORI STOK</h4>
          </div>
          <div id="modal-body">
              <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-12">
                        	<div class="form-group has-feedback">
                                <label class="col-sm-12" for="password">No Bukti</label>
                                <div class="col-sm-12">
                                	  <input type="text" id="no_bukti" name="nobukti" class="form-control" readonly="" required="" autofocus="">
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <label class="col-sm-12" for="tanggal">Tanggal</label>

                                <div class="col-sm-12">
                                    <input type="text" id="tgl" name="tanggal" class="form-control" readonly="" required="" autofocus="">
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <label class="col-sm-12" for="jenis">Jenis</label>

                                <div class="col-sm-12">
                                    <input type="text" id="jenis" name="jenis" class="form-control" readonly="" required="" autofocus="">
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <label class="col-sm-12" for="catatan">Catatan</label>

                                <div class="col-sm-12">
                                	<textarea id="catatan" readonly="" class="form-control" required="" autofocus=""></textarea>
                                    <span class="help-block with-errors"></span>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <label class="col-sm-12" for="catatan">Produk</label>

                                <div class="col-sm-12">
                                	<table class="table table-bordered">
                                	<thead>
                                		<th align="left" >#</th>
                                		<th align="left" >Nama</th>
                                		<th align="left" >Jumlah</th>
                                	</thead>
                                	<tbody>
                                		<td align="center">1</td>
                                		<td id="nama_produk"></td>
                                		<td id="jumlah"></td>
                                	</tbody>
                                	</table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
      </div>
  </div>
</div>
<?php require_once('template/footer.php') ?>
<script type="text/javascript">
	function detailModal(no_bukti, tgl, jenis, catatan, nama_produk, jumlah) {
		var mymodal = $('#myModal');	     
	    document.getElementById("no_bukti").value = no_bukti;
	    document.getElementById("tgl").value = tgl;
	    document.getElementById("jenis").value = jenis;
	    document.getElementById("catatan").value = catatan;
	    document.getElementById("nama_produk").innerHTML = nama_produk;
	    document.getElementById("jumlah").innerHTML = jumlah;
	    mymodal.modal('show');
	}
</script>

