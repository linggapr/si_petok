<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <ul class="nav side-menu">
      <li><a href="<?php echo base_url('Dashboard') ?>"><i class="fa fa-dashboard"></i>Dashboard</a>
      <li><a><i class="fa fa-suitcase"></i>Data Produk<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
         <li><?php echo anchor('Produk/daftar_produk', 'Daftar Produk') ?></a></li>
         <li><a href="<?php echo base_url() ?>Produk/update_stok">Update Stok</a></li>
         <li><a href="<?php echo base_url() ?>Produk/histori_stok">History Stok</a></li>
        </ul>
      <li><a><i class="fa fa-shopping-cart"></i>Data Penjualan<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="<?php echo base_url() ?>Sales">Pencatatan Penjualan</a></li>
          <li><a href="<?php echo base_url() ?>Sales/history_transaksi">History Transaksi</a></li>
          <li class="hidden"><a href="<?php echo base_url() ?>Sales/konfirmasi_pengiriman"></a></li>
          <li class="hidden"><a href="<?php echo base_url() ?>Sales/penjualan_selesai"></a></li>
        </ul>
      </li>
      <li><a href="<?php echo base_url() ?>Pengeluaran"><i class="fa fa-heartbeat"></i>Pengeluaran</a></li>
      <li><a href="<?php echo base_url() ?>Pemasukkan"><i class="fa fa-dollar"></i>Pemasukkan (NonSales)</a></li>
      <li><a href="<?php echo base_url() ?>Pelanggan"><i class="fa fa-group"></i>Pelanggan</a></li>
      <li><a><i class="fa fa-file"></i>Laporan<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="<?php echo base_url() ?>Produk/rekap_stok">Rekap Stok</a></li>
          <li><a href="<?php echo base_url() ?>Sales/rekap_penjualan">Rekap Transaksi</a></li>
        </ul>
      </li>
      <li><a href="<?php echo base_url() ?>Pengaturan"><i class="fa fa-gears"></i>Pengaturan</a></li>
    </ul>
  </div>
</div>
