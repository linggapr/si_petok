 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<?php require_once('template/header.php') ?>
<style media="screen">
  footer {
    margin-top: 0px;
  }
  th {
    text-align: center;
  }
  .hr-flex {
    border-top: 1px dashed #d8d8d8;
    color: #ffffff;
    background-color: #ffffff;
    height: 1px;
    margin: 10px 0;
    display: block;
    flex: 1;
  }

  .panel-menu {
    padding: 10px 15px;
    background-color: #f5f5f5;
    border-radius: 10px;
	}

  .text-info {
    color: #069d9f;
	}
	.table td{
	  position:relative;
	}
</style>
<div class="right_col" role="main">
	<div class="row">
		<?php if ($response = $this->session->flashdata('update_transaksi')) { ?>
    <div class="row">
        <div class="col-lg-12" align="center">
        <div class="alert alert-success" role="alert">
          <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
          <span class="sr-only">success:</span>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo $response; ?>
        </div>
        </div>
      </div>
		<?php } ?>
		<div class="x_panel">
	      <div class="x_title">
	        <h2>PENJUALAN <small></small></h2>
	        <div class="clearfix"></div>
	      </div>
	      <section class="content">
    	<div class="row">
    	<div class="col-lg-12">
			<div class="bs-docs-section">
			   <div class="panel with-nav-tabs panel-default">
			       <div class="panel-heading">
		          <ul class="nav nav-pills">
		              <li class="active"><a href="#tab0default" data-toggle="tab" aria-expanded="true">1. CATAT PENJUALAN <i class="glyphicon glyphicon-chevron-right"></i></a></li>
		              <li class=""><a href="<?php echo site_url('Sales/konfirmasi_pengiriman') ?>">2. KONFIRMASI PENGIRIMAN <i class="glyphicon glyphicon-chevron-right"></i></a></li>
		              <li class=""><a href="<?php echo site_url('Sales/penjualan_selesai') ?>">3. PENJUALAN SELESAI <i class="glyphicon glyphicon-ok"></i></a></li>
		          </ul>
       			</div>
       		<div class="panel-body">
           <div class="tab-content">
             <div class="tab-pane fade active in" id="tab0default">
             	<form method="post" class="form-horizontal" action="<?php echo site_url('Sales/catat_penjualan'); ?>" id="insert_pemesanan">
								<div class="col-lg-12 panel-menu">
					    		<button type="button" id="id_buttonAdd" style="display:none" onclick="transaksi_new()" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i> Transaksi Baru</button>
		  				  	<button type="button" id="id_buttonEdit" data-modal-size="modal-lg" class="btn btn-default"><i class="glyphicon glyphicon-pencil"></i> Ubah Transaksi</button>
		  				  	<button type="button" id="id_buttonLunas" class="btn btn-danger" href="#tab1default" data-toggle="tab" aria-expanded="true">Transaksi Belum Lunas <i class="glyphicon glyphicon-chevron-right"></i></button>
	  						</div>

					  		<div class="clearfix">&nbsp;</div>
		          		<div class="col-sm-12 headings"><span class="badge progress-bar-warning">T R A N S A K S I </span><hr class="hr-flex"></div>
		          		<div class="col-md-12" id="div-transaksi">
				       			<div class="form-group">
											<div class="col-sm-3">
									  <label class="control-label" ><strong>No Invoice</strong></label>
										<input style="text-align : center" type="text" class="form-control" readonly="" id="no_invoice" name="no_invoice" placeholder="Auto generate">
									</div>
									<div class="col-sm-3">
										<label class="control-label"><strong>Tanggal Transaksi</strong></label>
                    <div class="datepicker input-group date">
                        <input style="text-align : center" type="text" class="form-control" id="tanggal" name="tanggal" readonly value="<?php echo(date("Y-m-d")); ?>">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
									</div>
							    <div class="col-sm-3">
							      <label class="control-label">Asal Transaksi</label>
							      <select required="" id="asal_pesanan" name="asal_pesanan" class="form-control">
							      	<option value="TOKO">Toko</option>
							      	<option value="INSTAGRAM">Instagram</option>
										</select>
					    		</div>
							    <div class="col-sm-3" >
						        <label class="control-label" for="tipe_transaksi">Tipe Transaksi</label>
						   			<select name="tipe_transaksi" class="form-control">
							   			<option value="ritel">Ritel/Ecer</option>
						   			</select>
									</div>
								</div>
		          </div>
		          <!-- Data Pelanggan -->
		          <div class="col-sm-12 headings"><span class="badge progress-bar-warning">D A T A &nbsp; P E L A N G G A N</span><hr class="hr-flex"></div>
              	<div class="col-md-6">
               		<div class="panel panel-default">
		               	<div class="panel-body">
										 	<div class="form-group">
										    <label class="col-sm-3 control-label" for="nohape"><b>Pelanggan</b></label>
										    <div class="col-sm-3">
										    	<select id="pilih_pelanggan" name="pilih_pelanggan" class="form-control" onchange="showPelanggan()">
										    		<option value="baru">Baru</option>
										    		<option value="lama">Lama</option>
										    	</select><br>
										    </div>
										    <div class="col-sm-6">
										        <div class="input-group" style="display:none;" id="cari_pelanggan">
										        <span class="input-group-addon"><i class="fa fa-search"></i></span>
										        <input type="text" placeholder="Cari Nama/No HP" id="search_pelanggan" class="form-control" name="nama_pelanggan" autocomplete="off">
										        </div>
										    </div>
										 	</div>
												<div class="form-group">
										    	  <input type="hidden" name="kode_penerima" id="kode_penerima">
												    <label class="col-sm-3 control-label" for="nohape">Nama</label>
												    <div class="col-sm-9">
												      <input type="text" placeholder="Wajib Isi" id="nama_penerima" class="form-control" name="nama_penerima">
												    </div>
												</div>
												<div class="form-group">
											    <label class="col-sm-3 control-label" for="nohape">No HP/WA</label>
											    <div class="col-sm-9">
											      <input type="text" placeholder="Wajib Isi" id="telp_penerima" class="form-control" name="telp_penerima">
											    </div>
												</div>
                        <div class="form-group">
											    <label class="col-sm-3 control-label" for="kota">Provinsi</label>
											    <div class="col-sm-9">
                            <select id="origin_province" name="origin_province" class="form-control" >
                              <option>- Provinsi -</option>
                              <?php foreach ($province->result() as $prov): ?>
                                <option value="<?php echo $prov->province_id; ?>"><?php echo $prov->province; ?></option>
                              <?php endforeach; ?>
                            </select>
													</div>
												</div>
												<div class="form-group">
											    <label class="col-sm-3 control-label" for="kota">Kota</label>
											    <div class="col-sm-9">
                            <select id="origin_city" name="destination" class="form-control">
                              <option>- Kota -</option>
                            </select>
													</div>
												</div>
												<div class="form-group">
											    <label class="col-sm-3 control-label" for="nohape">Kec.</label>
											    <div class="col-sm-9">
											   		<input type="text" id="kecamatan" name="kecamatan" placeholder="Kecamatan" class="form-control">
													</div>
												</div>
												<div class="form-group">
											    <label class="col-sm-3 control-label" for="nohape">Alamat</label>
											    <div class="col-sm-9">
											      <textarea placeholder="Wajib Isi. Max 500 karakter" id="alamat_penerima" name="alamat_penerima" class="form-control" rows="1"></textarea>
														<span id="charNum"></span>
											    </div>
												</div>
												 <div class="form-group">
												    <label class="col-sm-3 control-label" for="kodepos">Kode Pos</label>
												    <div class="col-sm-9">
												      <input type="text" id="kodepos" class="form-control" name="kodepos">
												    </div>
												 </div>
												 <div class="form-group">
												    <label class="col-sm-3 control-label" for="emailnya">Email</label>
												    <div class="col-sm-9">
												      <input type="text" id="email" class="form-control" name="email">
												    </div>
												 </div>
		               		</div>
	               		</div>
	              	</div>
                  <?php foreach ($pengirim->result() as $toko): ?>
                    <div class="col-md-6">
                      <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                              <label class="col-sm-4 control-label" for="nohape"><strong>Pengirim</strong></label>
                              <div class="col-sm-8">
                              	<input type="hidden" name="nama_pengirim" id="nama_pengirim" value="<?php echo $toko->nama ?>">
                                <input type="text" readonly name="pengirim_toko" id="pengirim_toko" value="<?php echo $toko->nama_toko; ?>" class="form-control">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label" for="nohape">Data Pengirim</label>
                              <div class="col-sm-8">
                                <?php foreach ($province->result() as $prov_pengirim): ?>
                                  <?php if ($toko->provinsi == $prov_pengirim->province): ?>
                                    <input type="hidden"id="provinsi_pengirim" name="provinsi_pengirim" value="<?php echo $prov_pengirim->province_id; ?>">
                                    <?php break; ?>
                                  <?php endif; ?>
                                <?php endforeach; ?>
                                <input type="hidden" name="kode_pengirim" value="<?php echo $toko->id_user; ?>">
                                <input type="hidden" id="kota" name="kota" value="<?php echo $toko->kota; ?>">
                                <input type="hidden" id="origin_pengirim" name="origin_pengirim" value="">
                                <textarea  name="alamat" readonly class="form-control" rows="3"><?php echo $toko->alamat; ?></textarea>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label" for="nohape"><strong>No Hp</strong></label>
                              <div class="col-sm-8">
                                <input type="text" name="nohp_pengirim" value="<?php echo $toko->no_hp; ?>" class="form-control">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label" >Catatan Toko</label>
                              <div class="col-sm-8">
                                <textarea name="cat_pengirim" id="catatan_toko" class="form-control" rows="2"></textarea>
                              <span id="charNum"></span>
                              </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-4 control-label" for="noteslabel">Notes Label Paket</label>
                                <div class="col-sm-8">
                                  <textarea id="noteslabel" name="noteslabel" class="form-control noteslabel" rows="3" placeholder="Ex: Pecah Belah, Makanan, Packing Kayu"></textarea>
                                </div>
                             </div>
                        </div>
                      </div>
                    </div>
                  <?php endforeach; ?>
	              	<div class="col-sm-12 headings"><span class="badge badge-info">D E T A I L &nbsp; B E L A N J A </span><hr class="hr-flex"></div>
              		<div class="col-md-12">
               			<div class="div_detail_belanja">
											<div class="form-group">
									    <div id="tabelProduk" class="col-sm-12 no-more-tables">
									    <small><i class="fa fa-info-circle"></i> <i>(*) Sistem hanya mengeluarkan yang jumlah stoknya &gt; 0. ---
										(**) Harga Produk Disesuaikan Dengan Tipe Transaksi</i></small>
												<table id="tabelProduknya" class="table table-hover table-bordered">
											    	<thead class="cf">
											    		<tr>
											    			<th style="width:3%" align="center">#</th>
											    			<th style="width:35%">Produk*</th>
											    			<th style="width:7%">Jumlah</th>
											    			<th style="width:15%">Harga**</th>
											    			<th style="width:8%">Total (gr)</th>
											    			<th style="width:15%">Total (Rp)</th>
											    			<th style="width:5%"></th>
											    		</tr></thead>
													  <tbody id="tbody_pemesanan">
											    		<tr id="tr1">
																<td align="center" data-title="#">1</td>
																<td data-title="Produk*">
															    <div class="input-group">
	                                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
	                            	    <input type="text" class="form-control" style="font-size: 12px" placeholder="Masukkan 4 karakter dalam Nama/SKU" id="cari_produk1" name="nama_produk[]">
																	</div>
																	<div id="show_detail1" class="text-info"></div>
																	<div id="jenis_produk_show1" class="text-info"></div>
														    	<input id="jenis_produk_hide1" type="hidden">
													    		<input type="hidden" class="form-control" id="kode_produk1" name="kode_produk[]">
													    		<input type="hidden" name="stok_produk1" id="stok_produk1">
													  		</td>
												    		<td data-title="Jumlah">
												          <input type="number" id="jumlah_barang1" name="jumlah_barang[]" value="1" class="jumlah form-control large" size="2" style="text-align:right" onchange="hitung_subtotal(1); jumlah_stok(1)" min="1">
												    		</td>
												    		<td data-title="Harga**">
															    <input type="text" id="harga1" name="harga[]" style="text-align:right" value="0" class="form-control large" readonly>
																</td>
																<td data-title="Total (gr)">
													    	    <input type="text" id="berat1" name="berat[]" readonly="" value="0" class="berat_total form-control large" size="2" style="text-align:right">
												    		</td>
																<td data-title="Total"><input type="text" id="harga_total1" name="harga_total[]" style="text-align:right" value="0" class="hargatot form-control large" size="3" readonly=""></td>
																<td data-title="">
																	<a href="javascript:void(0);" onclick="T_removeElement_awal();" class="btn btn-danger" id="del_row1">
																	<i class="glyphicon glyphicon-minus"></i>
																	</a>
																</td>
														</tr>
											    </tbody>
											    <tfoot class="cf">
										    		<tr>
										    			<th style="width:3%" align="center"></th>
										    			<th style="width:35%"><strong>T O T A L</strong></th>
										    			<th style="width:8%"><input type="text" style="text-align:right" id="total_barang_all" class="form-control" value="0" readonly="" name="total_barang_all"></th>
										    			<th style="width:8%"></th>
										    			<th style="width:8%"><input type="text" style="text-align:right" id="total_berat" class="form-control" readonly="" name="total_berat" value="0"></th>
										    			<th style="width:10%">
										    				<input type="text" style="text-align:right" id="total_harga_all" class="form-control" readonly="" value="0" name="total_harga_all">
										    			</th>					    				
										    			<th style="width:5%"></th>
										    		</tr><
										    	</tfoot>
											  </table>
									    </div>
                      <div class="form-group col-md-12"><button type="button" class="btn btn-warning" onclick="addProduk();" id="add_rowProduk"><i class="glyphicon glyphicon-plus"></i>Tambah Produk Lain</button></div>

									 </div>
									<!-- ONGKIR -->
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <div class="form-group">
                        <label class="col-sm-2 text-left"><strong>Kurir</strong></label>
                        <div class="col-sm-2">
                          <select class="form-control" id="kurir" name="kurir">
                            <option value="0">- Pilih Kurir -</option>
                            <option value="jne">JNE</option>
                            <option value="tiki">TIKI</option>
                            <option value="pos">POS Indonesia</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 text-left"><strong>Service</strong></label>
                        <div class="col-sm-4">
                          <select class="form-control" name="service" id="service">
                            <option value="0">- Service -</option>
                          </select>
                        </div>
                      </div>
                      <hr>
                      <div class="form-group">
     								    <div class="col-sm-3">
     								      <label class="text-left" for="ongkir" style="color:RED"><b>Ongkos Kirim</b></label>
     								      <div class="input-group">
     								          <span class="input-group-addon"><i class="fa fa-truck" aria-hidden="true"></i> Rp </span>
     								          <input type="text"  onchange="hitung_total()" id="ongkir" class="form-control" readonly name="ongkir" value="0"  style="text-align:right">
     								      </div>
     								    </div>
     								    <div class="col-sm-3">
     								        <label class="text-left" for="diskon_all">Diskon Tambahan (Rp)</label>
     								        <div class="input-group">
     								        	<span class="input-group-addon">Rp</span>
     								        	<input type="number" min="0" id="diskon_all" class="form-control" onchange="hitung_total()" value="0" name="diskon_all" style="text-align:right">
     								        </div>
     								        
     								    </div>
     								    <div class="col-sm-3">
     								        <label class="text-left" for="expense">Biaya Tambahan (Rp)</label>
     								        <div class="input-group">
     								        	<span class="input-group-addon">Rp</span>
     								        	<input type="number" id="biaya_tambahan" class="form-control" onchange="hitung_total()" value="0" name="biaya_tambahan" style="text-align:right;" >
     								        </div>
     								    </div>
     								    <div class="col-sm-3">
     								      <label class="text-left" for="biaya_pajak">Pajak</label> <input type="checkbox" onchange="hitung_total()" checked="" id="nilai_pajak" onclick="nilaipajak()"> <small>(%)</small>
     								      <div class="input-group">
     						            <input type="number" min="0" max="100" onchange="hitung_total()" class="form-control" value="0" placeholder="Persentase" size="2" name="persen_pajak" id="persen_pajak">
     						            <span class="input-group-addon" id="persen">%</span>
     						            <input type="number" min="0" onkeyup="hitung_total()" id="biaya_pajak" class="form-control" value="0" name="biaya_pajak" style="text-align:right" readonly="">
     						          </div>
     								    </div>
     								  </div>
     								  <div class="form-group" id="forpo">
     								    <div class="col-sm-3">
     								      <label class="text-left" for="nohape"><b>TOTAL</b></label>
     								      <div class="input-group">
     								      	<span class="input-group-addon">Rp</span>
     								        <input type="text"  id="total_all" class="form-control" value="0" readonly="" name="total_all" style="text-align:right">
     								      </div>
     								    </div>
     								    <div class="col-sm-3">
     								       <label class="text-left" for="ongkir" style="color:RED"><b>Jumlah Bayar</b> <input type="checkbox" onclick="lunasin()" id="lunascheck"> Lunas</label>
     								       <div class="input-group">
     								       	<span class="input-group-addon">Rp</span>
     								       	<input type="number" id="jml_bayar" min="0" class="form-control" name="jml_bayar" value="0" onchange="kurangBayar($('#total_all').val())" style="text-align:right">
     								       </div>
     								    </div>
     								    <div class="col-sm-3">
     								       <label class="text-left" for="diskon_all"><b>Kurang Bayar</b></label>
     								       <div class="input-group">
     								       	<span class="input-group-addon">Rp</span>
     								        <input type="number" readonly="" id="kurang_bayar" class="form-control" value="0" name="kurang_bayar" style="text-align:right">
     								       </div>
     								    </div>
     										<div class="col-sm-3">
     									    <label class="control-label" for="nohape">Cara Bayar</label>
     											<select id="bank" name="bank" class="form-control">
     											<?php foreach ($bank->result() as $row): ?>
     												<?php if ($row->row->id == '7' || $row->nama_bank == 'Cash') : ?>
     													<option value="<?php echo $row->id_bank ?>"><?php echo $row->nama_bank ?></option>		
     												<?php else : ?>
     												  <option value="<?php echo $row->id_bank ?>"><?php echo $row->nama_bank.' - '.$row->no_rek.' ('.$row->atas_nama.')';?></option>  
     												<?php endif ?>
     												
     											<?php endforeach ?>
     											</select>
     										</div>
     								  </div>
                    </div>
                  </div>
								 	<div class="form-group">
								 		<label class="col-sm-12 text-right"></label>
								    <div class="col-sm-12">
								    	<button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-save"></i> Simpan</button>
									  </div>
								 	</div>
								</div> <!-- div-detail-belanja -->
							</div>
						</form>
					</div>
          <div class="tab-pane fade" id="tab3default">			
          	<table id="daftar_data_penjualan_selesai" class="table table-bordered table-striped table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>No Invoice</th>
										<th>Tgl Order</th>
										<th>Pelanggan</th>
										<th>Kota</th>
										<th>Kurir</th>
										<th>Tgl Kirim</th>
										<th>No Resi</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
					</div>
   				<!-- end -->
				</div>
			</div>
		</div>
	</div>
</div>
</div>									
</section>
</div>
</div>
</div>

<!--  Modal Ubah Transaksi--> 
<div class="modal fade" id="ubah_transaksi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 900px" role="document">
    <div class="modal-content">
    	<div class="modal-header">
    		<h4 class="modal-title">DATA TRANSAKSI (YANG BELUM STATUS SELESAI)</h4>
    	</div>
    	<div class="modal-body">
    		<div class="row">
    			<div class="col-lg-12">
    				<table id="datatable" class="table table-bordered table-hover table-striped">
    				<thead>
    					<tr>
    						<th>No</th>
    						<th>No Invoice</th>
    						<th>Tgl Order</th>
    						<th>Pelanggan</th>
    						<th>Kurir</th>
    						<th>Aksi</th>
    					</tr>
    				</thead>
    				<tbody align="center">
    					<?php 
    						if ($data_transaksi->num_rows() > 0) {
    							$no = 1;
    							?>
    							<?php foreach ($data_transaksi->result() as $row): ?>
    											<tr>
    												<td width="3%" ><?php echo $no++ ?></td>
    												<td><?php echo $row->no_invoice ?></td>
    												<td><?php echo $row->tanggal ?></td>
    												<td><?php echo $row->nama ?></td>
    												<td><?php echo $row->kurir ?></td>
    												<td width="5%"><a data-id="<?php echo $row->no_invoice; ?>" class="ubah_transaksi badge progress-bar-success"><i class="glyphicon glyphicon-pencil"></i> Ubah</a></td>
    											</tr>
    							<?php endforeach ?>
    						
    							<?php
    						}
    					 ?>
    				</tbody>
    			</table>
    			</div>
    		</div>
    	</div>
		  <div class="modal-footer">
		  	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Close</button>
		  </div>
		</div><!-- /.modal-content -->
  </div>
</div>

<!-- Modal Label-->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 700px" role="document">
    <div class="modal-content">
		  <div class="modal-header">
		      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		      <h4 class="modal-title">Detail Penjualan</h4>
		  </div>
		  <div class="modal-body">
	      <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-sm-12">
              	<!-- List group -->
							  <ul class="list-group">
							    <li class="list-group-item text-right"><span style="font-size:20px" id="det_namatoko"></span> <br>
							    	<span><small id="det_tanggal"></span> <br>
							    	<span id="det_noinvoice"></span></small><br>
							    	<small class="text-info"><i>Dicatat Oleh: <strong id="det_pemiliktoko"></strong></i>
									</li>
							    <li class="list-group-item"><h6><strong>Yth. Pelanggan</strong></h6><span class="text-right" id="det_penerima"></span>
							    <br>
							    <h6><strong>Alamat</strong></h6><span class="text-right" id="det_alamat"></span> <br> <span id="det_kecataman"></span></li>
							    <li class="list-group-item">
							      <h6><strong>Asal Pesanan</strong></h6>
							      <p id="det_asaltransaksi"> </p>												        
							      <h6><strong>Deskripsi Belanja</strong></h6>
						      	<table class="table">
						          <thead>
						          	<tr>
							          	<th style="text-align:center">No</th>
							          	<th style="text-align:center" width="50%">Produk</th>
							          	<th style="text-align:center">Qty</th>
							          	<th style="text-align:center">Satuan</th>
							          	<th style="text-align:center">Total</th>
							         	</tr>
						          </thead>
						          <tbody id="det_produk" >
						          </tbody>
						          <tfoot>
						          	<tr>
							    				<td colspan="4" align="right"><small>Total Belanja</small></td>
							    				<td align="right" id="det_totalbelanja"></td>
							    			</tr>
									    	<tr><td colspan="4" align="right"><small>Ongkos Kirim</small></td><td align="right" id="det_ongkir"></td></tr>
									    	<tr><td colspan="4" align="right"><small>Diskon (-)</small><td align="right" id="det_diskon"></td></tr>
									    	<tr><td colspan="4" align="right"><small>Biaya Tambahan</small></td><td align="right" id="det_biaya_tambahan"></td></tr>
									    	<tr><td colspan="4" align="right"><small>Pajak</small></td><td align="right" id="det_pajak"></td></tr>
									    	<tr><td colspan="4" align="right"><strong>TOTAL</strong></td><td align="right" id="det_totalall"><strong></strong></td></tr>
						          </tfoot>
							    	</table>
							    </li>
							    <li class="list-group-item col-sm-12"><h6><b>Notes</b></h6>
							      <span id="det_notes"></span>
							    </li>
							    <li class="list-group-item col-sm-3"><h6><strong>Jumlah Transfer</strong></h6><span id="det_jumlahtf"></span></li>
							    <li class="list-group-item col-sm-3"><h6><strong>Bank Transfer / COD</strong></h6><span id="det_carabayar"></span></li>
							    <li class="list-group-item col-sm-3"><h6><strong>Kurir</strong></h6><span id="det_kurir"></span></li>
							    <li class="list-group-item col-sm-3"><h6><strong>Resi/Status</strong></h6><span>-</span></li>
							  </ul>
              </div>
            </div>
          </div>
	      </div>
		  </div>
		  <div class="modal-footer">
		  	<button type="button" class="btn btn-warning pull-right"><i class="glyphicon glyphicon-print"></i> Print</button>
		  </div>
		</div><!-- /.modal-content -->
  </div>
</div>



<?php require_once('template/footer.php') ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
  	$(window).keydown(function(event) {
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
  	});

  	$('#id_buttonEdit').click(function(){
  		$('#ubah_transaksi').modal('show');	
      $('.ubah_transaksi').click(function() {
      	$('#id_buttonAdd').show();
      	var no_invoice = $(this).data('id');
      	$.get('<?php echo site_url('Sales/ubah_transaksi/') ?>'+no_invoice, function(resp) {
      		var data = JSON.parse(resp);
      		$('#no_invoice').val(data[0].no_invoice);
      		$('#tanggal').val(data[0].tanggal);
      		$('#pilih_pelanggan').val('lama');
      		$("option[value='baru']").attr("disabled", "disabled").siblings().removeAttr("disabled")
      		$('#nama_penerima').val(data[0].nama);
      		$('#telp_penerima').val(data[0].no_hp);
        	$.get('<?php echo site_url('Sales/get_city_by_id/') ?>'+data[0].kota+'/'+data[0].provinsi, function(resp){
          	$('#origin_city').html(resp);					          			          
	        });
      		$('#kecamatan').val(data[0].kecamatan);
      		$('#alamat_penerima').val(data[0].alamat);
      		$('#kodepos').val(data[0].kodepos);
      		$('#email').val(data[0].email);
      		$('#catatan_toko').val(data[0].catatan_toko);
      		$('#noteslabel').val(data[0].catatan_label);
      		var count = 1;
      		for (var i = 0; i < data.length; i++) {
      			if (i != data.length-1) {
      				addProduk();
      			}
      			$('#cari_produk'+count).val(data[i].nama_produk+' (SKU: '+data[i].kode_produk+')'); 
      			$('#kode_produk'+count).val(data[i].kode_produk);
      			$('#jumlah_barang'+count).val(data[i].jumlah);
      			$('#berat'+count).val(parseInt(data[i].berat * data[i].jumlah));
      			$('#harga'+count).val(data[i].harga);
      			$('#harga_total'+count).val(data[i].total_harga);
      			sumall();
           	hitung_total();
      			count++;
      		}      		
      		
      		$('#origin_province').val(data[0].provinsi);
    			var get_kurir = data[0].kurir;
      		var kurir = get_kurir.substr(0, 4).trim().toLowerCase();
      		$('#kurir').val(kurir);
	        $.get('<?php echo site_url('Sales/get_service/'); ?>'+ data[0].kota +'/'+ data[0].provinsi +'/'+ $('#total_berat').val() +'/'+kurir, function(resp) {
		          $('#service').html(resp);
	           	var get_service = data[0].kurir;
		      		var service = get_service.substr(6, 12).trim();
		     			$('#service').val(service);
		          hitung_total();
		      });
      		$('#ongkir').val(data[0].biaya_ongkir);
      		$('#diskon_all').val(data[0].diskon_tambahan);
      		$('#biaya_tambahan').val(data[0].biaya_tambahan);
      		$('#biaya_pajak').val(data[0].pajak);
      		$('#jml_bayar').val(data[0].jumlah_bayar);
      		$('#kurang_bayar').val(data[0].kurang_bayar);
      		if (data[0].kurang_bayar == 0) {
      			$('#lunascheck').prop('checked', true);
      		}
      		$('#bank').val(data[0].cara_bayar);
      		$('#ubah_transaksi').modal('hide');	
      	});
      });
      
    });

  	$('form#insert_pemesanan').submit(function(e) {
  		var form = $(this);
  		var url = form.attr('action');
  		$.ajax({
  			type: "POST",
  			url: url,
  			data: form.serialize(),
  			dataType: "html",
  			success: function(data) {
  				var dataModal = JSON.parse(data);
  				$('#det_noinvoice').html('No Transaksi: '+dataModal.no_invoice);
  				$('#det_tanggal').html('Tanggal Order: '+dataModal.tanggal);
  				$('#det_namatoko').html(dataModal.nama_toko);
  				$('#det_pemiliktoko').html(dataModal.nama_pengirim);
  				$('#det_alamat').html(dataModal.alamat_penerima);
  				$('#det_penerima').html(dataModal.nama_penerima);
  				$('#det_kecataman').html(dataModal.kecamatan+' - '+dataModal.kota);
  				$('#det_asaltransaksi').html(dataModal.asal_transaksi);
  				$('#det_ongkir').html(dataModal.ongkir);
  				$('#det_kurir').html(dataModal.kurir);
  				$('#det_diskon').html(dataModal.diskon_tambahan);
  				$('#det_biaya_tambahan').html(dataModal.biaya_tambahan);
  				$('#det_totalall').html('Rp. '+dataModal.total_all);
  				$('#det_jumlahtf').html(dataModal.total_all);
  				$('#det_totalbelanja').html(dataModal.total_belanja);
  				$('#det_pajak').html(dataModal.biaya_pajak);
  				$('#det_carabayar').html(dataModal.cara_bayar);
  				// produknya
  				$('#det_produk').html(dataModal.produks);
  				$('#det_notes').html(dataModal.notes);
  				$('#detailModal').modal('show');
  				$('#detailModal').on('hidden.bs.modal', function () {
					  location.reload();
					});

  			}
  		});
  		e.preventDefault();
  	}); 

  	$( "#search_pelanggan" ).autocomplete({
        autoFocus: true,
        source: "<?php echo site_url('Sales/get_autocomplete_pelanggan/?');?>",
        select: function (e, ui) {
           var no_hp = ui.item.value;
           var n = no_hp.indexOf("/");
           var keyword = no_hp.substr(n+2, 15);
           console.log(keyword);           
           $.ajax({
           	type: "GET",
           	data: "",
           	url: "<?php echo site_url('Sales/get_data_pelanggan/'); ?>"+keyword,
           	success: function(results) {
           		var data = JSON.parse(results);
           		console.log(data);
           		$('#origin_province').val(data.provinsi);
           		$.get('<?php echo site_url('Sales/get_city_by_id/'); ?>'+data.kota+'/'+data.provinsi, function(resp) {
				       	$('#origin_city').html(resp);
				   		});					   			
		       		$('#kode_penerima').val(data.kode_pelanggan);
           		$('#nama_penerima').val(data.nama);
           		$('#nama_penerima').prop('readonly',true);
           		$('#telp_penerima').val(data.no_hp);
           		$('#telp_penerima').prop('readonly',true);
           		$('#kecamatan').val(data.kecamatan);
           		$('#kecamatan').prop('readonly',true);
           		$('#alamat_penerima').val(data.alamat);
           		$('#alamat_penerima').prop('readonly',true);
           		$('#kodepos').val(data.kodepos);
           		$('#kodepos').prop('readonly',true);
           		$('#email').val(data.email);
           		$('#email').prop('readonly',true);
           	}
           })
        },
        messages: {
          noResults: '',
          results: function() {}
        }
      });
      $( "#cari_produk1" ).autocomplete({
        autoFocus: true,
        source: "<?php echo site_url('Sales/get_autocomplete/?');?>",
        select: function (e, ui) {
           var produk = ui.item.value;
           var n = produk.indexOf("SKU");
           var kode = produk.substr(n+5, 7);           
           $.ajax({
           	type: "GET",
           	data: "",
           	url: "<?php echo site_url('Sales/get_data_produk/'); ?>"+kode,
           	success: function(results) {
           		var data = JSON.parse(results);
           		$('#kode_produk1').val(data.kode_produk);
           		$('#harga1').val(data.harga);
           		$('#berat1').val(data.berat);
           		$('#stok_produk1').val(data.stok);
           		var diskon = data.diskon > 0 ? "| Diskon : <strong><span class='badge progress-bar-danger'>"+data.diskon+"%</span></strong>" : ""; 
           		var detail = '<small> Harga Ecer : <strong>Rp. '+data.harga+'</strong> | Berat : <strong>'+data.berat+' gr</strong> | Stok : <strong>'+data.stok+'</strong> '+diskon+' </small>';
           		$('#show_detail1').html(detail);
           		$('#harga_total1').val(data.harga);	
           		sumall();
           		hitung_total();
           	}
           })
        },
        messages: {
          noResults: '',
          results: function() {}
        }
      });

      $('.datepicker').datepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
          todayHighlight: true,
          orientation: "top auto",
          todayBtn: true,
          todayHighlight: true,
      });


      $('#origin_province').change(function(){
        var province_id=$('#origin_province').val();
        $.get('<?php echo site_url('Sales/get_city_by_province/') ?>'+province_id, function(resp){
          $('#origin_city').html(resp);
        });
      });

      $('#kurir, #origin_city, #total_harga_all').change(function() {
        var kurir = $('#kurir').val();
        var berat = $('#total_berat').val();
        var service = $('#service').val();
        $.get('<?php echo site_url('Sales/get_service/'); ?>'+ $('#origin_city').val() +'/'+ $('#kota').val() +'/'+berat+'/'+kurir, function(resp) {
          $('#service').html(resp);
          hitung_total();
        });
        if (kurir == '0' || service == '0') {
        	$('#ongkir').val('0');
        	hitung_total();
        }
      });

      $('#service, #origin_city, #total_harga_all').change(function() {
        var kurir = $('#kurir').val();
        var berat = $('#total_berat').val();
        var service = $('#service').val();
        var provinsi = $('#provinsi_pengirim').val();
        if (kurir != '0' || service != '0') {
        	$.get('<?php echo site_url('Sales/get_harga_service/'); ?>'+ $('#kota').val() +'/'+ $('#origin_city').val() +'/'+berat+'/'+kurir+'/'+service, function(resp) {
	          $('#ongkir').val(resp);
	          hitung_total();
	        });
        } else {
        	$('#ongkir').val('0');
        	hitung_total();
        }
      });
  }); 
	

  function hitung_total() {
  	var ongkir = parseInt($('#ongkir').val());
  	var subtotal = parseInt($('#total_harga_all').val());
  	var diskon_tambahan = parseInt($('#diskon_all').val());
  	var biaya_tambahan = parseInt($('#biaya_tambahan').val());
  	var persen_pajak = parseInt($('#persen_pajak').val());
  	var biaya_pajak = parseInt($('#biaya_pajak').val());
  	var total_bayar = $('#total_all');	
  	var total;
  	if (persen_pajak == 0) {
  		total = subtotal + ongkir - diskon_tambahan + biaya_tambahan + biaya_pajak;
  		total_bayar.val(parseInt(total));
  		kurangBayar(parseInt(total));
  	} else {
  		var pajak = (persen_pajak / 100) * (subtotal + ongkir - diskon_tambahan + biaya_tambahan + biaya_pajak);
  		total = subtotal + ongkir - diskon_tambahan + biaya_tambahan + pajak;
  		$('#biaya_pajak').val(parseInt(pajak));
  		total_bayar.val(parseInt(total));
  		kurangBayar(parseInt(total));
  	}
  }

  function kurangBayar(total) {
  	var jml_bayar = $('#jml_bayar').val();
  	$('#kurang_bayar').val(parseInt(total - jml_bayar));
  }

  function showPelanggan() {
    if ($('#pilih_pelanggan').val() == 'lama') {
      $('#cari_pelanggan').show();
    } else {
    	$('#nama_penerima').prop('readonly',false);
			$('#telp_penerima').prop('readonly',false);
			$('#kecamatan').prop('readonly',false);
			$('#alamat_penerima').prop('readonly',false);
			$('#kodepos').prop('readonly',false);
			$('#email').prop('readonly',false);
      $('#cari_pelanggan').hide();
    }
  }

	function nilaipajak(){
    $('#persen_pajak,#biaya_pajak').val(0);
    if($("#nilai_pajak").is(':checked')){
        $('#biaya_pajak').attr('readonly', true);
        $('#persen_pajak').attr('readonly', false);
        $('#biaya_pajak').val(0);   

    }else{
    		$('#persen_pajak').val(0);
        $('#biaya_pajak').attr('readonly', false);
        $('#persen_pajak').attr('readonly', true);
    }
	}

	function hitung_subtotal(i) {
  	var produk = document.getElementById('cari_produk'+i).value;
	  var n = produk.indexOf("SKU");
	  var kode = produk.substr(n+5, 7);       
  	$.ajax({
	   	type: "GET",
	   	data: "",
	   	url: "<?php echo site_url('Sales/get_data_produk/'); ?>"+kode,
	   	success: function(results) {
	   		var data = JSON.parse(results);
	  		var jumlah = document.getElementById('jumlah_barang'+i).value;
				$('#berat'+i).val(jumlah * data.berat);
				$('#harga_total'+i).val(jumlah * data.harga);			
				sumall(); 		
				hitung_total();
	   	}
	   })
	}

	function jumlah_stok(i) {
		var jumlah = parseInt($('#jumlah_barang'+i).val());
		var stok = parseInt($('#stok_produk'+i).val()); 
		if (jumlah > stok) {
			alert('Jumlah Barang Melebihi Stok');
			$('#jumlah_barang'+i).val(1);
		}
	}
  var count_id = 1;

  function T_removeElement_awal() {
		if($('#tbody_pemesanan').children('tr').length > 1) {
			document.getElementById('tbody_pemesanan').removeChild(document.getElementById('tr1'));
			sumall();
		} else {
			alert("Tidak Ada Form Yang Di Hapus ");
		}
	}

  function T_removeElement(i) {
		if($('#tbody_pemesanan').children('tr').length > 1) {
			document.getElementById('tbody_pemesanan').removeChild(document.getElementById('tr' + i));
      count_id--;
      sumall();
		} else {
			alert("Tidak Ada Form Yang Di Hapus ");
		}
	}

  function addProduk() {
		count_id++;

		var objNewDiv = document.createElement('tr');
		objNewDiv.setAttribute('id', 'tr' + count_id);
    objNewDiv.innerHTML = '<td align="center">'+count_id+'</td>';
    objNewDiv.innerHTML += '<td data-title="Produk*"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input type="text" class="form-control" style="font-size: 12px" placeholder="Masukkan 4 karakter dalam Nama/SKU" id="cari_produk'+count_id+'" name="nama_produk[]" autocomplete="off"></div><div class="text-info" id="show_detail'+count_id+'"> </div><input type="hidden" class="form-control" id="kode_produk'+count_id+'" name="kode_produk[]"></td>';
    objNewDiv.innerHTML += '<input type="hidden" name="stok_produk'+count_id+'" id="stok_produk'+count_id+'">';
    objNewDiv.innerHTML += '<td data-title="Jumlah"><input type="number" min="1" id="jumlah_barang'+count_id+'" name="jumlah_barang[]" value="1" class="jumlah form-control large" size="2" style="text-align:right" onchange="hitung_subtotal('+count_id+'); jumlah_stok('+count_id+')"></td>';
    objNewDiv.innerHTML += '<td data-title="Harga**"><input type="text" id="harga'+count_id+'" name="harga[]" style="text-align:right" value="0" class="form-control large" readonly=""></td>';
    objNewDiv.innerHTML += '<td data-title="Total (gr)"><input type="text" id="berat'+count_id+'" name="berat[]" readonly="" value="0" class="berat_total form-control large" size="2" style="text-align:right"></td>';
    objNewDiv.innerHTML += '<td data-title="Total"><input type="text" id="harga_total'+count_id+'" name="harga_total[]" style="text-align:right" value="0" class="hargatot form-control large" size="3" readonly=""></td>';
    objNewDiv.innerHTML += '<td><a href="javascript:void(0);" onclick="T_removeElement('+count_id+');" class="btn btn-danger" id="del_row'+count_id+'"><i class="glyphicon glyphicon-minus"></i></a></td></tr>';
		document.getElementById('tbody_pemesanan').appendChild(objNewDiv);

    $( "#cari_produk"+count_id ).autocomplete({
      autoFocus: true,
      source: "<?php echo site_url('Sales/get_autocomplete/?');?>",
      select: function (e, ui) {
           var produk = ui.item.value;
           var n = produk.indexOf("SKU");
           var kode = produk.substr(n+5, 7);           
           $.ajax({
           	type: "GET",
           	data: "",
           	url: "<?php echo site_url('Sales/get_data_produk/'); ?>"+kode,
           	success: function(results) {
           		var data = JSON.parse(results);
           		$('#kode_produk'+count_id).val(data.kode_produk);
	         		$('#harga'+count_id).val(data.harga);
	         		$('#berat'+count_id).val(data.berat);
	         		$('#stok_produk'+count_id).val(data.stok);
	         		var diskon = data.diskon > 0 ? "| Diskon : <strong><span class='badge progress-bar-danger'>"+data.diskon+"%</span></strong>" : "";
	           	var detail = '<small> Harga Ecer : <strong>Rp. '+data.harga+'</strong> | Berat : <strong>'+data.berat+' gr</strong> | Stok : <strong>'+data.stok+'</strong> '+diskon+' </small>';
	           	$('#show_detail'+count_id).html(detail);
	           	$('#harga_total'+count_id).val(data.harga);	
	           	sumall();
	           	hitung_total();
           	}
           })
        },
      
      messages: {
        noResults: '',
        results: function() {}
      }
    });
	}

	function transaksi_new() {
		$('#id_buttonAdd').hide();
		location.reload();
	}

	function sumall(){
		var t=0;
		var jumlah_tot=0;
		var berat_tot=0;
		
		$('.hargatot').each(function(i,e) {
		  var amt = $(this).val();
		  t += parseFloat(amt);        
		});
		
		$('.berat_total').each(function(i,e) {
		  var berat = $(this).val();
		  berat_tot += parseFloat(berat);        
		});
		
		$('.jumlah').each(function(i,e) {
		  var jumlah = $(this).val();
		  jumlah_tot += parseFloat(jumlah);        
		});
		
		$('#total_harga_all').val(t) ;
		$('#total_berat').val(berat_tot) ;
		$('#total_barang_all').val(jumlah_tot) ;
	
	}

	function lunasin(){
	  if($('#lunascheck').is(":checked")){
	      $('#jml_bayar').val($('#total_all').val());
	      $('#kurang_bayar').val(0);
	  }else{
	      $('#jml_bayar').val(0);
	      
	  }
	}

</script>
