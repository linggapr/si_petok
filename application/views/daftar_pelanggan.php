<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once('template/header.php') ?>
<style type="text/css" media="screen">
	.panel-menu {
    padding: 10px 15px;
    background-color: #f5f5f5;
    border-radius: 10px;
	}
	#datatable .dropdown-menu {
    position: relative; float: none; width: 145px; min-width:20px;
  }
</style>
<div class="right_col" role="main">
	<div class="row">
		<?php if ($response = $this->session->flashdata('tambah_konsumen')) { ?>
    <div class="row">
        <div class="col-lg-12" align="center">
        <div class="alert alert-success" role="alert">
          <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
          <span class="sr-only">success:</span>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo $response; ?>
        </div>
        </div>
      </div>
<?php } elseif ($response = $this->session->flashdata('gagal_tambah')) { ?>
         <div class="row">
        <div class="col-lg-12" align="center">
        <div class="alert alert-danger" role="alert">
          <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
          <span class="sr-only">success:</span>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo $response; ?>
        </div>
        </div>
      </div>
<?php } elseif ($response = $this->session->flashdata('edit_konsumen')) { ?>
         <div class="row">
        <div class="col-lg-12" align="center">
        <div class="alert alert-success" role="alert">
          <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
          <span class="sr-only">success:</span>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo $response; ?>
        </div>
        </div>
      </div>
<?php } elseif ($response = $this->session->flashdata('hapus_pelanggan')) { ?>
         <div class="row">
        <div class="col-lg-12" align="center">
        <div class="alert alert-danger" role="alert">
          <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
          <span class="sr-only">success:</span>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo $response; ?>
        </div>
        </div>
      </div>
<?php } ?>
		<div class="x_panel">
      <div class="x_title">
        <h2>KONSUMEN<small></small></h2>
        <div class="clearfix"></div>
      </div>
      <section class="content">
      	<div class="row">
		    	<div class="col-lg-12">
						<div class="bs-docs-section">
					   	<div class="panel with-nav-tabs panel-default">
		       			<div class="panel-body">
		       				<div class="tab-content">
		       					<div class="col-lg-12 panel-menu">
						    		  <a type="button" data-toggle="modal" data-target="#detailModal" data-modal-size="modal-lg" class="btn btn-default" href=""><i class="glyphicon glyphicon-plus"></i>  Konsumen Baru</a>						    		  
						    		</div>
				            <form method="post" accept-charset="utf-8">
											<br><br><br><br>
											<table id="datatable" class="table table-bordered table-striped table-hover">
												<thead>
													<tr>
														<th>No</th>
														<th>Nama</th>
														<th>No HP</th>
														<th>Kota</th>
														<th>Kecamatan</th>
														<th>Aksi</th>
													</tr>
												</thead>
												<tbody>
													<?php 
														$no = 1;
														if ($data->num_rows() > 0) {
															foreach ($data->result() as $row) {
																?>
																<tr>
																	<td align="center"><?php echo $no; ?><input type="hidden" id="kode_pelanggan" name="kode_pelanggan[]" value="<?php echo $row->kode_pelanggan ?>"></td>
																	<td><strong><?php echo $row->nama; ?></strong><br><small>Kode: <?php echo $row->kode_pelanggan; ?></small></td>
																	<td><?php echo $row->no_hp; ?></td>
																	<td><?php echo $row->type.' '.$row->city_name; ?></td>
																	<td><?php echo $row->kecamatan; ?></td>
																	<td align="center">
									                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Aksi</button>
									                    <ul class="dropdown-menu dropup">
									                        <li><a class="ubah_data" data-toggle="modal" data-target="#ubah_dataModal"data-kode_pelanggan="<?php echo $row->kode_pelanggan;?>"><span class="fa fa-pencil"></span>&nbsp&nbsp&nbsp&nbspEdit</a></li>
									                        <li><a href="<?php echo base_url().'Pelanggan/hapus_pelanggan/'.$row->kode_pelanggan;?>"><span class="fa fa-trash"></span>&nbsp&nbsp&nbsp&nbspHapus</a></li>
									                        <li><a href=""><span class="fa fa-list"></span>&nbsp&nbsp&nbsp&nbspHistory Belanja</a></li>
									                    </ul>
										                	
																	</td>
																</tr>
																<?php
																$no++;
															}
														}
													 ?>
												</tbody>
											</table>				
										</form>	
		       				</div>
		       			</div>																						
		       		</div>
		       	</div>
		       </div>
		     </div>
			</section>
		</div>
	</div>
</div>
<!-- form tambah pelanggan -->
<form id="input_konsumen" action="<?php echo site_url('Pelanggan/simpan_data_pelanggan') ?>" method="post" accept-charset="utf-8">
<!-- Modal -->
	<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" style="width: 700px" role="document">
	    <div class="modal-content">
			  <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
			      <h4 class="modal-title">Input Konsumen Baru</h4>
			  </div>
			  <div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama</label>
								<input class="form-control" placeholder="Isi nama konsumen" id="nama" name="nama" value="" type="text">
								<input id="id_konsumen" name="id_konsumen" value="" type="hidden">
							</div>
							<div class="form-group">
								<label>No HP / WhatsApp</label>
								<input class="form-control" placeholder="Isi angka saja" id="nohp" name="no_hp" value="" type="text">
							</div>
							<div class="form-group">
								<label>Email</label>
								<input class="form-control" placeholder="Isi dengan email" id="email" name="email" value="" type="text">
							</div>
							<div class="form-group">
								<label>Kode POS</label>
								<input class="form-control" placeholder="Kode Pos" id="kode_pos" name="kode_pos" value="" type="text">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Provinsi</label>
								<select name="provinsi" id="provinsi" class="form-control">
									<option>- Provinsi -</option>
                  <?php foreach ($province->result() as $prov): ?>
                    <option value="<?php echo $prov->province_id; ?>"><?php echo $prov->province; ?></option>
                  <?php endforeach; ?>
								</select>
							</div>
							<div class="form-group">
								<label>Kota</label>
								<select name="kota" id="kota" class="form-control">
									<option>- Kota -</option>
								</select>
							</div>
							<div class="form-group">
								<label>Kecamatan</label>
								<input type="text" class="form-control" name="kecamatan" value="" placeholder="Kecamatan">
							</div>
							<div class="form-group">
								<label>Alamat</label>
								<textarea type="text" class="form-control" id="alamat" name="alamat" placeholder="Isi alamat lengkap"></textarea>
							</div>	
						</div>
			  </div>
			  <div class="modal-footer">
			  	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Batal</button>
			  	<button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-save"></i> Simpan</button>
			  	
			  </div>
			</div><!-- /.modal-content -->
	  </div>
	</div>
</div>
</form>

<!-- form ubah pelanggan -->
<form id="input_konsumen" action="<?php echo site_url('Pelanggan/ubah_pelanggan') ?>" method="post" accept-charset="utf-8">
<!-- Modal -->
	<div class="modal fade" id="ubah_dataModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" style="width: 700px" role="document">
	    <div class="modal-content">
			  <div class="modal-header">
			  	
		      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		      <h4 class="modal-title">Ubah Data Konsumen</h4>
			  </div>
			  <div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Nama</label>
								<input class="form-control" placeholder="Isi nama konsumen" id="ubah_nama" name="nama" value="" type="text">
								<input id="ubah_kode_pelanggan" name="ubah_kode_pelanggan" value="" type="hidden">
							</div>
							<div class="form-group">
								<label>No HP / WhatsApp</label>
								<input class="form-control" placeholder="Isi angka saja" id="ubah_nohp" name="no_hp" value="" type="text">
							</div>
							<div class="form-group">
								<label>Email</label>
								<input class="form-control" placeholder="Isi dengan email" id="ubah_email" name="email" value="" type="text">
							</div>
							<div class="form-group">
								<label>Kode POS</label>
								<input class="form-control" placeholder="Kode Pos" id="ubah_kodepos" name="kode_pos" value="" type="text">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Provinsi</label>
								<select name="provinsi" id="ubah_provinsi" class="form-control">
									<option>- Provinsi -</option>
                  <?php foreach ($province->result() as $prov): ?>
                    <option value="<?php echo $prov->province_id; ?>"><?php echo $prov->province; ?></option>
                  <?php endforeach; ?>
								</select>
							</div>
							<div class="form-group">
								<label>Kota</label>
								<select name="kota" id="ubah_kota" class="form-control">
									<option>- Kota -</option>
								</select>
							</div>
							<div class="form-group">
								<label>Kecamatan</label>
								<input type="text" class="form-control" id="ubah_kecamatan" name="kecamatan" value="" placeholder="Kecamatan">
							</div>
							<div class="form-group">
								<label>Alamat</label>
								<textarea type="text" class="form-control" id="ubah_alamat" name="alamat" placeholder="Isi alamat lengkap"></textarea>
							</div>	
						</div>
			  </div>
			  <div class="modal-footer">
			  	<button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Batal</button>
			  	<button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-save"></i> Simpan</button>
			  	
			  </div>
			</div><!-- /.modal-content -->
	  </div>
	</div>
</div>
</form>

<?php require_once('template/footer.php') ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" >
	$(document).ready(function() {
		$('#provinsi').change(function(){
      var province_id=$('#provinsi').val();
      $.get('<?php echo site_url('Pelanggan/get_city_by_province/') ?>'+province_id, function(resp){
        $('#kota').html(resp);
      });
    });

		$('#ubah_provinsi').change(function(){
      var province_id = $('#ubah_provinsi').val();
      $.get('<?php echo site_url('Pelanggan/get_city_by_province/') ?>'+province_id, function(resp){
        $('#ubah_kota').html(resp);
      });
    });

		$('.ubah_data').click(function() {
			var kode_pelanggan = $(this).data('kode_pelanggan');
			$.get('<?php echo site_url('Pelanggan/hal_ubah_pelanggan/') ?>'+kode_pelanggan, function(resp){
        var data = JSON.parse(resp);
     		$('#ubah_provinsi').val(data.provinsi);
     		$.get('<?php echo site_url('Pelanggan/get_city_by_id/') ?>'+data.kota+'/'+data.provinsi, function(resp) {
     			$('#ubah_kota').html(resp);
     		});
     		$('#ubah_kode_pelanggan').val(data.kode_pelanggan);
     		$('#ubah_nama').val(data.nama);
     		$('#ubah_nohp').val(data.no_hp);
     		$('#ubah_email').val(data.email);
     		$('#ubah_kodepos').val(data.kodepos);
     		$('#ubah_kecamatan').val(data.kecamatan);
     		$('#ubah_alamat').val(data.alamat);
     		$('#ubah_dataModal').modal('show');

			});
		});	
	});
</script>