<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once('template/header.php') ?>
<style media="screen">
  footer {
    margin-top: 0px;
  }
  .dropdown-menu {
    min-width:20px;
  }
  .btn-info {
    background: #21b9bb linear-gradient(#21b9bb,#0fa6a8);
    border-color: #23c6c8;
    color: #FFFFFF;
  }

  .panel-menu {
    padding: 10px 15px;
    background-color: #f5f5f5;
    border-radius: 10px;
  }
  .hr-line-dashed {
    border-top: 1px dashed #d8d8d8;
    color: #ffffff;
    background-color: #ffffff;
    height: 1px;
    margin: 10px 0;
  }
</style>

<div class="right_col" role="main">
  <div class="row">
    <?php if ($response = $this->session->flashdata('tambah_jenis_pengeluaran')) { ?>
    <div class="row">
        <div class="col-lg-12" align="center">
        <div class="alert alert-success" role="alert">
          <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
          <span class="sr-only">success:</span>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo $response; ?>
        </div>
        </div>
      </div>
    <?php } elseif ($response = $this->session->flashdata('hapus_jenis_pengeluaran')) {?>
      <div class="row">
          <div class="col-lg-12" align="center">
          <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <span class="sr-only">success:</span>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $response; ?>
          </div>
          </div>
        </div>
    <?php } elseif ($response = $this->session->flashdata('edit_jenispengeluaran')) {?>
      <div class="row">
          <div class="col-lg-12" align="center">
          <div class="alert alert-success" role="alert">
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            <span class="sr-only">success:</span>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $response; ?>
          </div>
          </div>
        </div>
    <?php } ?>
    
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Jenis Pengeluaran <small></small></h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="btn-group">
            <a href="<?php echo site_url('Pengeluaran') ?>" class="btn btn-info"> Input Pengeluaran</a>  
           </div>
          <div class="btn-group">
            <a href="<?php echo site_url('Pengeluaran/jenis_pengeluaran') ?>" class="btn btn-info"> Jenis Pengeluaran</a>  
          </div>
          <div class="btn-group">
            <a href="<?php echo site_url('Pengeluaran/history_pengeluaran') ?>" class="btn btn-info"><span class="glyphicon glyphicon-list"></span> History Pengeluaran</a>  
         </div>
          <hr class="hr-line-dashed">
          <div class="panel-body">
            <div class="tab-content">
              <div class="col-lg-12 panel-menu">
                <a type="button" data-toggle="modal" data-target="#detailModal" data-modal-size="modal-lg" class="btn btn-default" href=""><i class="glyphicon glyphicon-plus"></i>  Tambah Jenis Pengeluaran</a>                      
              </div>
              <form method="post" accept-charset="utf-8">
                <br><br><br><br>
                <table id="datatable" class="table table-bordered table-striped table-hover">
                  <thead>
                    <tr>
                      <th width="5%">No</th>
                      <th width="20%">Nama Pengeluaran</th>
                      <th>Deskripsi</th>
                      <th width="12%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      $no = 1;
                      if ($nama_pengeluaran->num_rows() > 0) {
                        foreach ($nama_pengeluaran->result() as $jenis) {
                          ?>
                          <tr>
                            <td align="center"><?php echo $no++ ?> <input type="hidden" id="id_pengeluaran" name="id" value="<?php echo $jenis->id ?>"></td>
                            <td><?php echo $jenis->nama_pengeluaran ?></td>
                            <td><?php echo $jenis->deskripsi ?></td>
                            <td align="center">
                              <button type="button" class="ubah_data btn btn-sm btn-primary" data-toggle="modal" data-target="#ubah_dataModal"
                                data-id="<?php echo $jenis->id; ?>"><i class="glyphicon glyphicon-pencil"></i>
                              </button>
                                <a href="<?php echo site_url('Pengeluaran/hapus_jenis_pengeluaran/'.$jenis->id.' ') ?>" class="btn btn-sm btn-danger" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a>
                            </td>
                          </tr>
                          <?php
                        }
                      }
                     ?>
                  </tbody>
                </table>        
              </form> 
            </div>
          </div>  
        </div>
      </div>
    </div>
  </div>
</div>

<!-- form tambah jenis pengeluaran -->
<form  action="<?php echo site_url('Pengeluaran/tambah_jenis_pengeluaran') ?>" method="post" accept-charset="utf-8">
<!-- Modal -->
  <div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 700px" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Input Jenis Pengeluaran</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Nama Pengeluaran</label>
                <input class="form-control" placeholder="Nama Pengeluaran" id="nama_pengeluaran" name="nama_pengeluaran" value="" type="text">
              </div>
              <div class="form-group">
                <label>Deskripsi</label>
                <textarea class="form-control" placeholder="Deskripsi Pengeluaran" id="deskripsi" name="deskripsi"></textarea>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-save"></i> Simpan</button>
        </div>
      </div><!-- /.modal-content -->
    </div>
  </div>
</div>
</form>

<!-- form ubah jenis pengeluaran -->
<form  action="<?php echo site_url('Pengeluaran/ubah_jenis_pengeluaran') ?>" method="post" accept-charset="utf-8">
<!-- Modal -->
  <div class="modal fade" id="ubah_dataModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 700px" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Input Jenis Pengeluaran</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Nama Pengeluaran</label>
                <input class="form-control" placeholder="Nama Pengeluaran" id="ubah_nama_pengeluaran" name="nama_pengeluaran" value="" type="text">
                <input type="hidden" name="id_jenis_pengeluaran" id="id_jenis_pengeluaran" value="">
              </div>
              <div class="form-group">
                <label>Deskripsi</label>
                <textarea class="form-control" placeholder="Deskripsi Pengeluaran" id="ubah_deskripsi" name="deskripsi"></textarea>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-save"></i> Simpan</button>
        </div>
      </div><!-- /.modal-content -->
    </div>
  </div>
</div>
</form>

<?php require_once('template/footer.php') ?>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.ubah_data').click(function() {
      var id = $(this).data('id');
      $.get('<?php echo site_url('Pengeluaran/get_jenis_pengeluaran/') ?>'+id, function(resp){
        var data = JSON.parse(resp);
        $('#id_jenis_pengeluaran').val(data.id);
        $('#ubah_nama_pengeluaran').val(data.nama_pengeluaran);
        $('#ubah_deskripsi').val(data.deskripsi);
        $('#ubah_dataModal').modal('show');  
      }); 
    });
  });
</script>