<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php require_once('template/header.php') ?>
<style type="text/css" media="screen">
  .panel-menu {
    padding: 10px 15px;
    background-color: #f5f5f5;
    border-radius: 10px;
  }
   .btn-info {
    background: #21b9bb linear-gradient(#21b9bb,#0fa6a8);
    border-color: #23c6c8;
    color: #FFFFFF;
  }
  #datatable .dropdown-menu {
    position: relative; float: none; width: 145px; min-width:20px;
  }
</style>
<div class="right_col" role="main">
  <div class="row">
    <?php if ($response = $this->session->flashdata('kategori_tambah')) { ?>
    <div class="row">
        <div class="col-lg-12" align="center">
        <div class="alert alert-success" role="alert">
          <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
          <span class="sr-only">success:</span>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo $response; ?>
        </div>
        </div>
      </div>
<?php } elseif ($response = $this->session->flashdata('kategori_update')) { ?>
         <div class="row">
        <div class="col-lg-12" align="center">
        <div class="alert alert-success" role="alert">
          <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
          <span class="sr-only">success:</span>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo $response; ?>
        </div>
        </div>
      </div>
<?php } elseif ($response = $this->session->flashdata('kategori_delete')) { ?>
      <div class="row">
        <div class="col-lg-12" align="center">
        <div class="alert alert-danger" role="alert">
          <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
          <span class="sr-only">success:</span>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?php echo $response; ?>
        </div>
        </div>
      </div>
<?php }   ?>
    <div class="x_panel">
      <div class="x_title">
        <h2>Daftar Kategori<small></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
       <div class="btn-group">
         <a href="<?php echo base_url() ?>Produk/daftar_produk" class="btn btn-primary"><span class="fa fa-list"></span> Daftar Produk</a>  
       </div>
       <div class="btn-group">
         <a href="<?php echo base_url() ?>Produk/daftar_kategori" class="btn btn-info"><span class="fa fa-list"></span> Daftar Kategori</a>
       </div>
       <div class="btn-group">
          <a href="" data-toggle="dropdown" class="btn btn-info dropdown-toggle" aria-expanded="false"><i class="glyphicon glyphicon-th"></i> Lainnya <span class="caret"></span></a>
            <ul class="dropdown-menu in">
                <br>
                <li><a href="<?php echo base_url() ?>Produk/update_stok" ><i class="fa fa-edit"></i>&nbsp&nbsp&nbsp&nbspUpdate Stok</a></li> <br>
                <li><a ><i class="fa fa-file-excel-o"></i>&nbsp&nbsp&nbsp&nbspUpload Data (Excel)</a></li> <br>
            </ul>
        </div>
        <br> <br>
      <section class="content">
        <div class="row">
          <div class="col-lg-12">
            <div class="bs-docs-section">
              <div class="panel with-nav-tabs panel-default">
                <div class="panel-body">
                  <div class="tab-content">
                    <div class="col-lg-12 panel-menu">
                      <a type="button" data-toggle="modal" data-target="#detailModal" data-modal-size="modal-lg" class="btn btn-default" href=""><i class="glyphicon glyphicon-plus"></i>  Kategori Baru</a>                      
                    </div>
                    <form method="post" accept-charset="utf-8">
                      <br><br><br><br>
                      <table id="datatable" class="table table-bordered table-striped table-hover">
                        <thead>
                          <tr>
                            <th width="3%">No</th>
                            <th width="12%">Kode Kategori</th>
                            <th>Nama Kategori</th>
                            <th width="15%">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $no = 1;
                            if ($kategori->num_rows() > 0) {
                              foreach ($kategori->result() as $row) {
                                ?>
                                <tr>
                                  <td align="center"><?php echo $no; ?><input type="hidden" id="kode_kategori" name="kode_kategori[]" value="<?php echo $row->kode_kategori ?>"></td>
                                  <td align="center"><?php echo $row->kode_kategori; ?></td>
                                  <td><?php echo strtoupper($row->nama_kategori); ?></td>
                                  <td align="center">
                                    <button type="button" class="ubah_data btn btn-sm btn-primary" data-toggle="modal" data-target="#ubah_dataModal"
                                      data-id="<?php echo $row->kode_kategori; ?>"><i class="glyphicon glyphicon-pencil"></i>
                                    </button>
                                      <a href="<?php echo site_url('Produk/hapus_kategori/'.$row->kode_kategori.' ') ?>" class="btn btn-sm btn-danger" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a>
                                  </td>
                                </tr>
                                <?php
                                $no++;
                              }
                            }
                           ?>
                        </tbody>
                      </table>        
                    </form> 
                  </div>
                </div>                                            
              </div>
            </div>
           </div>
         </div>
      </section>
    </div>
  </div>
</div>
<!-- form tambah jenis pemasukkan -->
<form  action="<?php echo site_url('Produk/tambah_kategori') ?>" method="post" accept-charset="utf-8">
<!-- Modal -->
  <div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 700px" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Input Kategori Baru</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Nama Kategori</label>
                <input class="form-control" placeholder="Nama Pemasukkan" id="nama_kategori" name="nama_kategori" value="" type="text">
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-save"></i> Simpan</button>
        </div>
      </div><!-- /.modal-content -->
    </div>
  </div>
</div>
</form>

<!-- form ubah jenis pemasukkan -->
<form  action="<?php echo site_url('Produk/ubah_kategori') ?>" method="post" accept-charset="utf-8">
<!-- Modal -->
  <div class="modal fade" id="ubah_dataModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 700px" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Ubah Kategori</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Nama Kategori</label>
                <input class="form-control" placeholder="Nama Kategori" id="ubah_nama_kategori" name="nama_kategori" value="" type="text">
                <input type="hidden" id="ubah_kode_kategori" name="kode_kategori" value="">
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-save"></i> Simpan</button>
        </div>
      </div><!-- /.modal-content -->
    </div>
  </div>
</div>
</form>

<?php require_once('template/footer.php') ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script type="text/javascript" >
  $(document).ready(function() {
    
    $('.ubah_data').click(function() {
      var kode_kategori = $(this).data('id');
      $.get('<?php echo site_url('Produk/hal_ubah_kategori/') ?>'+kode_kategori, function(resp){
        var data = JSON.parse(resp);
        $('#ubah_kode_kategori').val(data.kode_kategori);
        $('#ubah_nama_kategori').val(data.nama_kategori);
        $('#ubah_dataModal').modal('show');

      });
    }); 
  });
</script>