<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengaturanModel extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}	

	public function get_data_user() {
		$this->db->from('users');
		return $this->db->get();
	}

	public function get_data_bank() {
		$this->db->from('tb_bank');
		return $this->db->get();	
	}

	public function get_data_sosmed() {
		$this->db->from('tb_sosmed');
		return $this->db->get();	
	}
	
	public function simpan_data_user($id, $data) {
		$this->db->where(array('id_user' => $id));
    $this->db->update('users',$data);
    return true;
	}
	public function simpan_data_sosmed($id, $data) {
		$this->db->where(array('id' => $id));
    $this->db->update('tb_sosmed',$data);
    return true;
	}
	public function simpan_data_bank($id, $data) {
		$this->db->where(array('id_bank' => $id));
    $this->db->update('tb_bank', $data);
    return true;
	}
}
