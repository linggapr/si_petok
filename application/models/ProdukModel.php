<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProdukModel extends CI_Model {

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function get_produk() {
  	$this->db->from('tb_produk');
    $this->db->join('tb_jenis','tb_jenis.kode_jenis = tb_produk.kode_jenis');
	  $this->db->join('tb_kategori','tb_kategori.kode_kategori = tb_produk.kode_kategori');
    $this->db->order_by("kode_produk", "desc");
	  return $this->db->get();
  }

  public function added_produk($data) {
    return $this->db->insert('tb_produk', $data);
  }

  public function ubah_produk($id) {
    return $this->db->get_where('tb_produk', $id);
  }

  public function update_data($id,$data,$table){
    $this->db->where($id);
    $this->db->update($table,$data);
    return true;
  }

  public function delete_produk($id) {
    return $this->db->delete('tb_produk', array('kode_produk'=>$id));
  }

  public function tampil_kategori() {
  	$this->db->from('tb_kategori');
  	$this->db->order_by("kode_kategori", "asc");
  	return $this->db->get();
  }

  public function hapus_kategori($kode_kategori) {
    return $this->db->delete('tb_kategori', array('kode_kategori' => $kode_kategori));
  }
  public function get_kategori_byid($kode_kategori) {
    $this->db->from('tb_kategori');
    $this->db->where('kode_kategori = "'.$kode_kategori.'"');
    return $this->db->get();
  }

  public function tampil_jenis() {
    $this->db->from('tb_jenis');
    $this->db->order_by("kode_jenis", "asc");
    return $this->db->get();
  }

  public function tampil_grosir() {
    $this->db->from('tb_harga_grosir');
    $this->db->join('tb_produk','tb_produk.kode_produk = tb_harga_grosir.kode_produk');
    $this->db->order_by("id", "asc");
    return $this->db->get();
  }

  public function added_kategori($data) {
    return $this->db->insert('tb_kategori', $data);
  }

  public function added_harga_grosir($data) {
    return $this->db->insert('tb_harga_grosir', $data);
  }

  public function kode_produk_terakhir() {
  	$query = $this->db->query("SELECT kode_produk FROM tb_produk ORDER BY kode_produk DESC LIMIT 1");
  	if ($query->num_rows() != 0) {
      foreach ($query->result_array() as $row) {
            $data = $row;
      }
      return $data['kode_produk'];
    } else {
      return 'B000000';
    }
  }

  public function kode_kategori_terakhir() {
  	$query = $this->db->query("SELECT kode_kategori FROM tb_kategori ORDER BY kode_kategori DESC LIMIT 1");
  	if ($query->num_rows() != 0) {
      foreach ($query->result_array() as $row) {
            $data = $row;
      }
      return $data['kode_kategori'];
    } else {
      return 'CA000000';
    }
  }





   // Histori Stok

  public function daftar_history_stok() {
    $this->db->select('kode_history, tanggal, jenis, catatan, jumlah, nama_produk');
    $this->db->from('tb_riwayat_stok');
    $this->db->join('tb_produk', 'tb_produk.kode_produk = tb_riwayat_stok.kode_produk');
    $this->db->order_by("kode_history", "desc");
    return $this->db->get();
  }

  public function get_stok_terakhir($kode_produk) {
    $query = $this->db->query("SELECT stok FROM tb_produk WHERE kode_produk = '".$kode_produk."' ");
    if ($query->num_rows() != 0) {
      foreach ($query->result_array() as $row) {
            $data = $row;
      }
      return $data['stok'];
    }
  }
  public function search_produk($keyword) {
    $query = $this->db->query("SELECT * FROM tb_produk WHERE stok > 0 AND (kode_produk LIKE '%$keyword%' OR nama_produk LIKE '%$keyword%')");
    return $query->result();
  }

  public function kode_history_terakhir() {
      $query = $this->db->query("SELECT kode_history FROM tb_riwayat_stok ORDER BY kode_history DESC LIMIT 1");
      if ($query->num_rows() != 0) {
        foreach ($query->result_array() as $row) {
              $data = $row;
        }
        return $data['kode_history'];
    } else {
      return 'HY000000';
    }
  }

  public function catat_history($data) {
    return $this->db->insert('tb_riwayat_stok', $data);
  }

  public function rekap_stok($jenis, $tanggalAwal, $tanggalAkhir) {
    $this->db->select('kode_history, tb_riwayat_stok.kode_produk, nama_produk, tanggal, jenis, catatan, jumlah');
    $this->db->from('tb_riwayat_stok');
    $this->db->join('tb_produk', 'tb_produk.kode_produk = tb_riwayat_stok.kode_produk');
    if ($jenis != 'ALL') {
      $this->db->where('jenis = "'.$jenis.'" AND (tanggal BETWEEN "'.$tanggalAwal.'" AND "'.$tanggalAkhir.'")'); 
    } else {
      $this->db->where('tanggal BETWEEN "'.$tanggalAwal.'" AND "'.$tanggalAkhir.'" '); 
    }
    return $this->db->get();
  }
}
