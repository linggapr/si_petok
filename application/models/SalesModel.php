<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SalesModel extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function tampil_transaksi() {
		$this->db->from('tb_transaksi');
		$this->db->join('tb_detail_transaksi', 'tb_detail_transaksi.no_invoice = tb_transaksi.no_invoice');
		$this->db->join('tb_produk', 'tb_produk.kode_produk = tb_produk.kode_produk');
		return $this->db->get();
	}

  public function data_transaksi() {
    $this->db->select('no_invoice, tanggal, nama, kurir');
    $this->db->from('tb_transaksi');
    $this->db->join('tb_pelanggan', 'tb_transaksi.kode_pelanggan = tb_pelanggan.kode_pelanggan');
    return $this->db->get();
  }

  public function get_data_transaksi($no_invoice) {
    $this->db->from('tb_transaksi');
    $this->db->join('tb_pelanggan', 'tb_pelanggan.kode_pelanggan = tb_transaksi.kode_pelanggan');
    $this->db->join('tb_detail_transaksi', 'tb_detail_transaksi.no_invoice = tb_transaksi.no_invoice');
    $this->db->join('tb_produk', 'tb_produk.kode_produk = tb_detail_transaksi.kode_produk');
    $this->db->where('tb_transaksi.no_invoice = "'.$no_invoice.'"');
    return $this->db->get();
  }

	public function get_data_pengirim() {
		$this->db->from('users');
		return $this->db->get();
	}

	public function get_data_produk($id) {
		$this->db->select('*');
    $data = array();
		$query = $this->db->get_where('tb_produk', array('kode_produk' => $id));
		foreach ($query->result_array() as $row) {
            $data = $row;
    }
		return $data;
	}

  public function get_data_pelanggan($no_hp) {
    $this->db->select('*');
    $data = array();
    $query = $this->db->get_where('tb_pelanggan', array('no_hp' => $no_hp));
    foreach ($query->result_array() as $row) {
            $data = $row;
    }
    return $data;
  }

	public function simpan_transaksi($data) {
		return $this->db->insert('tb_transaksi', $data);
	}

	public function simpan_det_transaksi($data) {
		return $this->db->insert('tb_detail_transaksi', $data);
	}

	public function simpan_riwayat_transaksi($data) {
		return $this->db->insert('tb_riwayat_transaksi', $data);	
	}

  public function update_transaksi($no_invoice, $data) {
    $this->db->where(array('no_invoice' => $no_invoice));
    $this->db->update('tb_transaksi', $data);
    return true;
  }

  public function update_det_transaksi($no_invoice, $kode_produk, $data) {
    $this->db->where(['no_invoice' => $no_invoice, 'kode_produk' => $kode_produk]);
    $this->db->update('tb_detail_transaksi', $data);
    return true;
  }

	public function kurangi_stok($id, $data){
    $this->db->where($id);
    $this->db->update('tb_produk', $data);
    return true;
  }

  public function update_stok($id, $data){
    $this->db->where(array('kode_produk' => $id));
    $this->db->update('tb_produk', $data);
    return true;
  }

  public function daftar_konfirmasi_pengiriman() {
    $this->db->from('tb_transaksi');
    $this->db->join('tb_pelanggan','tb_transaksi.kode_pelanggan = tb_pelanggan.kode_pelanggan');
    $this->db->join('tb_riwayat_transaksi','tb_riwayat_transaksi.no_invoice = tb_transaksi.no_invoice');
    $this->db->where('status_penjualan = "MENUNGGU"');
    $this->db->order_by('tb_transaksi.no_invoice', 'desc');
    return $this->db->get();
  }

  public function get_pengirim() {
     $query = $this->db->query("SELECT nama_toko FROM users ORDER BY id_user ASC LIMIT 1");
    if ($query->num_rows() != 0) {
      foreach ($query->result_array() as $row) {
            $data = $row;
      }
      return $data['nama_toko'];
    } else {
      return false;
    }
  }

  public function daftar_penjualan_selesai() {
    $this->db->from('tb_transaksi');
    $this->db->join('tb_pelanggan','tb_transaksi.kode_pelanggan = tb_pelanggan.kode_pelanggan');
    $this->db->join('tb_riwayat_transaksi','tb_riwayat_transaksi.no_invoice = tb_transaksi.no_invoice');
    $this->db->where('status_penjualan = "BERHASIL"');
    $this->db->order_by('tb_transaksi.no_invoice', 'desc');
    return $this->db->get();
  }

  public function history_transaksi() {
    $this->db->from('tb_transaksi');
    $this->db->join('tb_pelanggan','tb_transaksi.kode_pelanggan = tb_pelanggan.kode_pelanggan');
    $this->db->join('tb_riwayat_transaksi','tb_riwayat_transaksi.no_invoice = tb_transaksi.no_invoice');
    $this->db->order_by('tb_transaksi.no_invoice', 'desc');
    return $this->db->get();
  }

  public function ganti_status_pesanan($no_invoice, $status, $ket) {
    $this->db->set('status_penjualan', $status);
    $this->db->set('ket', $ket);
    $this->db->where('no_invoice', $no_invoice);
    $this->db->update('tb_riwayat_transaksi');
    return true;
  }

  public function simpan_konfirmasi_pengiriman_tglkirim($no_invoice, $tgl_kirim) {
    $this->db->set('tgl_kirim', $tgl_kirim);
    $this->db->where('no_invoice', $no_invoice);
    $this->db->update('tb_riwayat_transaksi');
    return true;
  }

  public function simpan_konfirmasi_pengiriman_noresi($no_invoice, $no_resi) {
    $this->db->set('no_resi', $no_resi);
    $this->db->where('no_invoice', $no_invoice);
    $this->db->update('tb_transaksi');
    return true;
  }

	public function noinvoice_terakhir() {
    $query = $this->db->query("SELECT no_invoice FROM tb_transaksi ORDER BY no_invoice DESC LIMIT 1");
    if ($query->num_rows() != 0) {
      foreach ($query->result_array() as $row) {
            $data = $row;
      }
      return $data['no_invoice'];
    } else {
      return 'T29384420';
    }
  }

  public function det_transaksi_terakhir() {
    $query = $this->db->query("SELECT kode_detail_transaksi FROM tb_detail_transaksi ORDER BY kode_detail_transaksi DESC LIMIT 1");
    if ($query->num_rows() != 0) {
      foreach ($query->result_array() as $row) {
            $data = $row;
      }
      return $data['kode_detail_transaksi'];
    } else {
      return 'DT00000000';
    }
  }

  public function kode_history_terakhir() {
    $query = $this->db->query("SELECT kode_history FROM tb_riwayat_transaksi ORDER BY kode_history DESC LIMIT 1");
    if ($query->num_rows() != 0) {
      foreach ($query->result_array() as $row) {
            $data = $row;
      }
      return $data['kode_history'];
    } else {
      return '0';
    }
  }

  public function rekap_transaksi($status_penjualan, $tanggalAwal, $tanggalAkhir) {
    $this->db->select('tb_transaksi.tanggal, tb_transaksi.no_invoice, nama, kota, total_biaya_transaksi');
    $this->db->from('tb_transaksi');
    $this->db->join('tb_pelanggan', 'tb_transaksi.kode_pelanggan = tb_pelanggan.kode_pelanggan');
    $this->db->join('tb_riwayat_transaksi', 'tb_transaksi.no_invoice = tb_riwayat_transaksi.no_invoice');
    if ($status_penjualan != 'ALL') {
      $this->db->where('status_penjualan = "'.$status_penjualan.'" AND (tanggal BETWEEN "'.$tanggalAwal.'" AND "'.$tanggalAkhir.'")'); 
    } else {
      $this->db->where('tanggal BETWEEN "'.$tanggalAwal.'" AND "'.$tanggalAkhir.'" '); 
    }
    return $this->db->get();
  }

  public function cek_kode_produk($no_invoice, $data) {
    $this->db->from('tb_detail_transaksi');
    $this->db->where_in('kode_produk', $data);
    $this->db->where(array('no_invoice' => $no_invoice));
    return $this->db->get();
  }

  public function get_kode_produk($no_invoice) {
    $this->db->select('kode_produk');
    $data = array();
    $query = $this->db->get_where('tb_detail_transaksi', array('no_invoice' => $no_invoice));
    foreach ($query->result_array() as $row) {
      $data[] = $row;
    }
    $in = array_column($data, 'kode_produk');
    return $in;
  }

  public function get_data_bank() {
    $this->db->from('tb_bank');
    $where = "no_rek IS NOT NULL AND TRIM(no_rek) != '' OR nama_bank = 'Cash'";
    $this->db->where($where);
    return $this->db->get();
  }


}
