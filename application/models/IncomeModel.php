<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IncomeModel extends CI_Model {

	public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function daftar_jenis_pengeluaran() {
    $this->db->from('tb_jenis_pengeluaran');
    return $this->db->get();
  }

  public function daftar_jenis_pemasukkan() {
    $this->db->from('tb_jenis_pemasukkan');
    return $this->db->get();
  }

  public function simpan_pemasukkan($data) {
		return $this->db->insert('tb_pemasukkan', $data);
	}

  public function simpan_det_pemasukkan($data) {
    return $this->db->insert('tb_detail_pemasukkan', $data);
  }

	public function simpan_pengeluaran($data) {
		return $this->db->insert('tb_pengeluaran', $data);
	}

  public function simpan_det_pengeluaran($data) {
    return $this->db->insert('tb_detail_pengeluaran', $data);
  }

  public function simpan_jenis_pengeluaran($data) {
    return $this->db->insert('tb_jenis_pengeluaran', $data);
  }

  public function simpan_jenis_pemasukkan($data) {
    return $this->db->insert('tb_jenis_pemasukkan', $data);
  }

  public function get_jenis_pengeluaran($id) {
    return $this->db->get_where('tb_jenis_pengeluaran', array('id' => $id));
  }

  public function get_history_pengeluaran($no_bukti) {
    $this->db->select('tb_pengeluaran.no_bukti, tanggal, dikeluarkan_dari, catatan, nama_pengeluaran, jumlah, total');
    $this->db->from('tb_detail_pengeluaran');
    $this->db->join('tb_pengeluaran', 'tb_detail_pengeluaran.no_bukti = tb_pengeluaran.no_bukti');
    $this->db->join('tb_jenis_pengeluaran', 'tb_detail_pengeluaran.id_jenis_pengeluaran = tb_jenis_pengeluaran.id');
    $this->db->where('tb_pengeluaran.no_bukti = "'.$no_bukti.'" ');
    return $this->db->get();
  }

   public function get_history_pemasukkan($no_bukti) {
    $this->db->select('tb_pemasukkan.no_bukti, tanggal, penerima, catatan, nama_pemasukkan, jumlah, total');
    $this->db->from('tb_detail_pemasukkan');
    $this->db->join('tb_pemasukkan', 'tb_detail_pemasukkan.no_bukti = tb_pemasukkan.no_bukti');
    $this->db->join('tb_jenis_pemasukkan', 'tb_detail_pemasukkan.id_jenis_pemasukkan = tb_jenis_pemasukkan.id');
    $this->db->where('tb_pemasukkan.no_bukti = "'.$no_bukti.'" ');
    return $this->db->get();
  }

  public function get_jenis_pemasukkan($id) {
    return $this->db->get_where('tb_jenis_pemasukkan', array('id' => $id));
  }

  public function hapus_jenis_pengeluaran($id) {
    return $this->db->delete('tb_jenis_pengeluaran', array('id' => $id));
  }

  public function hapus_jenis_pemasukkan($id) {
    return $this->db->delete('tb_jenis_pemasukkan', array('id' => $id));
  }

  public function search_jenis_pemasukkan($keyword) {
    $query = $this->db->query("SELECT * FROM tb_jenis_pemasukkan WHERE id LIKE '$keyword%' OR nama_pemasukkan LIKE '%$keyword%'");
    return $query->result();
  } 

  public function ubah_jenis_pengeluaran($id, $data){
    $this->db->where(array('id' => $id ));
    $this->db->update('tb_jenis_pengeluaran', $data);
    return true;
  }

  public function ubah_jenis_pemasukkan($id, $data){
    $this->db->where(array('id' => $id ));
    $this->db->update('tb_jenis_pemasukkan', $data);
    return true;
  }

  public function history_pengeluaran() {
    $this->db->from('tb_pengeluaran');
    $this->db->order_by('tanggal DESC');
    return $this->db->get();
  }

  public function history_pemasukkan() {
    $this->db->from('tb_pemasukkan');
    $this->db->order_by('tanggal DESC');
    return $this->db->get();
  } 

  public function pengeluaran_batal($no_bukti) {
    $this->db->delete('tb_detail_pengeluaran', array('no_bukti' => $no_bukti));
    $this->db->delete('tb_pengeluaran', array('no_bukti' => $no_bukti));
  }

  public function pemasukkan_batal($no_bukti) {
    $this->db->delete('tb_detail_pemasukkan', array('no_bukti' => $no_bukti));
    $this->db->delete('tb_pemasukkan', array('no_bukti' => $no_bukti));
  }

	public function search_jenis_pengeluaran($keyword) {
    $query = $this->db->query("SELECT * FROM tb_jenis_pengeluaran WHERE id LIKE '$keyword%' OR nama_pengeluaran LIKE '%$keyword%'");
    return $query->result();
  }	

  public function id_jenis_pemasukkan_terakhir() {
    $query = $this->db->query("SELECT id FROM tb_jenis_pemasukkan ORDER BY id DESC LIMIT 1");
    if ($query->num_rows() != 0) {
      foreach ($query->result_array() as $row) {
            $data = $row;
      }
      return $data['id'];
    } else {
      return '0';
    }
  }

  public function id_jenis_pengeluaran_terakhir() {
    $query = $this->db->query("SELECT id FROM tb_jenis_pengeluaran ORDER BY id DESC LIMIT 1");
    if ($query->num_rows() != 0) {
      foreach ($query->result_array() as $row) {
            $data = $row;
      }
      return $data['id'];
    } else {
      return '0';
    }
  }
  public function get_last_nobukti_pemasukkan() {
    $query = $this->db->query("SELECT no_bukti FROM tb_pemasukkan ORDER BY no_bukti DESC LIMIT 1");
    if ($query->num_rows() != 0) {
      foreach ($query->result_array() as $row) {
            $data = $row;
      }
      return $data['no_bukti'];
    } else {
      return 'IC000000';
    }
  }

  public function get_last_nobukti_pengeluaran() {
    $query = $this->db->query("SELECT no_bukti FROM tb_pengeluaran ORDER BY no_bukti DESC LIMIT 1");
    if ($query->num_rows() != 0) {
      foreach ($query->result_array() as $row) {
            $data = $row;
      }
      return $data['no_bukti'];
    } else {
      return 'OC000000';
    }
  }

}
