<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PelangganModel extends CI_Model {
	public function __construct()
		{
			parent::__construct();
			$this->load->database();
		}	

	public function tampil_pelanggan() {
		$this->db->from('tb_pelanggan');
    $this->db->join('tb_provinsi', 'tb_pelanggan.provinsi = tb_provinsi.province_id');
    $this->db->join('tb_kota', 'tb_pelanggan.kota = tb_kota.city_id');
		$this->db->order_by('nama', 'desc');
		return $this->db->get();
	}	

  public function get_provinsi() {
    $this->db->from('tb_provinsi');
    $this->db->order_by('province', 'asc');
    return $this->db->get();
  }

  public function get_kota($province_id) {
    $this->db->from('tb_kota');
    $this->db->where(array('province_id' => $province_id));
    $this->db->order_by('city_name', 'asc');
    return $this->db->get();
  }

  public function get_city_by_id($id) {
    $this->db->from('tb_kota');
    $this->db->where(array('city_id' => $id));
    return $this->db->get();
  }

  public function tambah_pelanggan($data) {
  	return $this->db->insert('tb_pelanggan', $data);
  }

  public function ubah_pelanggan($id, $data ){
    $this->db->where(array('kode_pelanggan' => $id));
    $this->db->update('tb_pelanggan', $data);
    return true;
  }

  public function get_data_pelanggan($kode_pelanggan) {
    $this->db->from('tb_pelanggan');
    $this->db->where('kode_pelanggan = "'.$kode_pelanggan.'" ');
    return $this->db->get();
  }
  public function hapus_pelanggan($id) {
  	return $this->db->delete('tb_pelanggan',array('kode_pelanggan'=>$id));
  }

  public function update_pelanggan($id,$data){
    $this->db->where($id);
    $this->db->update('tb_pelanggan',$data);
    return true;
  }

  public function search_pelanggan($keyword) {
    $query = $this->db->query("SELECT * FROM tb_pelanggan WHERE no_hp LIKE '$keyword%' OR nama LIKE '%$keyword%'");
    return $query->result();
  }

  public function kode_pelanggan_terakhir() {
    $query = $this->db->query("SELECT kode_pelanggan FROM tb_pelanggan ORDER BY kode_pelanggan DESC LIMIT 1");
    if ($query->num_rows() != 0) {
      foreach ($query->result_array() as $row) {
            $data = $row;
      }
      return $data['kode_pelanggan'];
    } else {
      return 'PL000000';
    }
  }

}