<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller {

	public function __construct() {
    parent::__construct();
    $this->load->model('PengaturanModel');
  }

	public function index()
	{	
		$data['data_user'] = $this->PengaturanModel->get_data_user();
		$data['data_sosmed'] = $this->PengaturanModel->get_data_sosmed();
		$data['data_bank'] = $this->PengaturanModel->get_data_bank();
		$data['province'] = $this->get_province();
		$this->load->view('pengaturan', $data);
	}

	public function simpan_data_user() {
		$nama_admin = $this->input->post('nama_admin');
		$nama_toko = $this->input->post('nama_toko');
		$deskripsi_toko = $this->input->post('deskripsi_toko');
		$alamat = $this->input->post('alamat');
		$provinsi = $this->input->post('provinsi_selected');
		$kota = $this->input->post('kota_selected');
		$kecamatan = $this->input->post('kecamatan');
		$no_hp = $this->input->post('no_hp');
		$email = $this->input->post('email');

		//akun sosmed
		$sosmed = $this->input->post('sosmed'); //array
		$id_sosmed = $this->input->post('id_sosmed'); // array
		$i = 1;
		$data_sosmed = array_combine($id_sosmed, $sosmed);
		foreach ($data_sosmed as $value) {
			$result = array('link' => $value);
			$this->PengaturanModel->simpan_data_sosmed($i, $result);
			$i++;
		}
		redirect('Pengaturan');
	}

	public function simpan_data_bank() {
		$id_bank = $this->input->post('id_bank'); // arrat
		$no_rek = $this->input->post('no_rek'); //array

		$i=0;
		foreach ($no_rek as $value) {
			$result = array('no_rek' => $value);
			$this->PengaturanModel->simpan_data_bank($id_bank[$i], $result);
			$i++;
		}
		redirect('Pengaturan');
	}

	public function get_data_byid($id) {
		$data = $this->PengaturanModel->get_data_user($id);
		foreach ($data->result_array() as $value) {
			 $row = $value;
		}
		echo json_encode($row);
	}

	public function get_province() {
		$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    "key: cbf4cb300a3e9dfe9cd1f6bb1e6442a8"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			  die('Tidak Ada Sambungan Internet');
			} else {
			  return json_decode($response);
			}
	}

	public function get_city_by_province($province_id){
		$city = $this->get_city($province_id);
		print_r($city);
		$output = '<option>- Kota -</option>';
		foreach ($city->rajaongkir->results as $cty) {
			$output .='<option value="'.$cty->city_id.'">'.$cty->type.' '.$cty->city_name.'</option>';
		}

		echo $output;
	}

	public function get_city($province_id) {
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.rajaongkir.com/starter/city?province=$province_id",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "key: cbf4cb300a3e9dfe9cd1f6bb1e6442a8"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  die('Tidak Ada Sambungan Internet');
		} else {
		  return json_decode($response);
		}
	}
}
