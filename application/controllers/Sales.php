<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('SalesModel');
		date_default_timezone_set("Asia/Bangkok");
		$this->load->library('PHPExcel');
	}

	public function index() {
		$data['province'] = $this->get_province();
		$data['pengirim'] = $this->SalesModel->get_data_pengirim();
		$data['data_transaksi'] = $this->SalesModel->data_transaksi();
		$data['bank'] = $this->SalesModel->get_data_bank();
		$this->load->view('catat_penjualan', $data);
	}

	public function send_sms($no_hp, $nama, $kurir, $no_resi, $pengirim) {
		// include('smsgateway/autoload.php');
		// $clients = new SMSGatewayMe\Client\ClientProvider("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTU0MDAxNTQ3OCwiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjYxNDQzLCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.grZgPltXkhrsUuV-MGmmXLckneMhBXRy_Pv8sxxC51I");

		// $sendMessageRequest = new SMSGatewayMe\Client\Model\SendMessageRequest([
  //       'phoneNumber' => $no_hp, 
  //       'message' => 'Dear '.$nama.', paket anda telah dikirim: '.$kurir.'. No-Resi: '.$no_resi.'. Dari: '.$pengirim.' (NOREPLY)', 
  //       'deviceId' => 102269
  //   ]);
  //   $sentMessages = $clients->getMessageClient()->sendMessages([$sendMessageRequest]);
	}
	public function konfirmasi_pengiriman() {
		$data['data_konfirmasi'] = $this->SalesModel->daftar_konfirmasi_pengiriman(); 
		return $this->load->view('konfirmasi_pengiriman', $data);
	}

	public function penjualan_selesai() {
		$data['data'] = $this->SalesModel->daftar_penjualan_selesai();
		return $this->load->view('penjualan_selesai', $data);
	}
	// fungsionalitas
	// insert data pelanggan baru
	public function tambah_pelanggan($kode_pelanggan, $nama, $no_hp, $provinsi, $kota, $kecamatan, $alamat, $kodepos, $email) {
		$this->load->model('PelangganModel');
		$pelanggan = array(
			'kode_pelanggan' => $kode_pelanggan,
			'nama' => $nama,
			'no_hp' => $no_hp,
			'provinsi' => $provinsi,
			'kota' => $kota,
			'kecamatan' => $kecamatan,
			'alamat' => $alamat,
			'kodepos' => $kodepos,
			'email' => $email
		);
		return $this->PelangganModel->tambah_pelanggan($pelanggan);
	}

	public function ubah_transaksi($no_invoice) {
		$data = $this->SalesModel->get_data_transaksi($no_invoice);
		$row = array();
		foreach ($data->result_array() as $value) {
			$row[] = $value;
		}
		echo json_encode($row);
	}

	public function simpan_transaksi($no_invoice, $kode_pelanggan, $kode_pengirim, $tanggal, $asal_transaksi, $tipe_transaksi, $kurir, $biaya_ongkir, $biaya_tambahan, $diskon_tambahan, $pajak, $total_biaya_transaksi, $jumlah_bayar, $kurang_bayar, $cara_bayar, $status, $catatan_toko, $catatan_label) {
		$transaksi = array(
			'no_invoice' => $no_invoice,
			'kode_pelanggan' => $kode_pelanggan,
			'kode_pengirim' => $kode_pengirim,
			'tanggal' => $tanggal,
			'asal_transaksi' => $asal_transaksi,
			'tipe_transaksi' => $tipe_transaksi,
			'kurir' => $kurir,
			'biaya_ongkir' => $biaya_ongkir,
			'biaya_tambahan' => $biaya_tambahan,
			'diskon_tambahan' => $diskon_tambahan,
			'pajak' => $pajak,
			'total_biaya_transaksi' => $total_biaya_transaksi,
			'jumlah_bayar' => $jumlah_bayar,
			'kurang_bayar' => $kurang_bayar,
			'cara_bayar' => $cara_bayar,
			'status' => $status,
			'catatan_toko' => $catatan_toko,
			'catatan_label' => $catatan_label
		);
		return $this->SalesModel->simpan_transaksi($transaksi);
	}



	public function update_transaksi($no_invoice, $tanggal, $asal_transaksi, $tipe_transaksi, $kurir, $biaya_ongkir, $biaya_tambahan, $diskon_tambahan, $pajak, $total_biaya_transaksi, $jumlah_bayar, $kurang_bayar, $cara_bayar, $status, $catatan_toko, $catatan_label) {
		$transaksi = array(
			'tanggal' => $tanggal,
			'asal_transaksi' => $asal_transaksi,
			'tipe_transaksi' => $tipe_transaksi,
			'kurir' => $kurir,
			'biaya_ongkir' => $biaya_ongkir,
			'biaya_tambahan' => $biaya_tambahan,
			'diskon_tambahan' => $diskon_tambahan,
			'pajak' => $pajak,
			'total_biaya_transaksi' => $total_biaya_transaksi,
			'jumlah_bayar' => $jumlah_bayar,
			'kurang_bayar' => $kurang_bayar,
			'cara_bayar' => $cara_bayar,
			'status' => $status,
			'catatan_toko' => $catatan_toko,
			'catatan_label' => $catatan_label
		);
		return $this->SalesModel->update_transaksi($no_invoice, $transaksi);
	}

	public function simpan_det_transaksi($no_invoice, $kode_produk, $jumlah, $total_harga){
		$kode_detail = $this->SalesModel->det_transaksi_terakhir();
		$kode_detail++;

		$det_transaksi = array(
			'kode_detail_transaksi' => $kode_detail,
			'no_invoice' => $no_invoice,
			'kode_produk' => $kode_produk,
			'jumlah' => $jumlah,
			'total_harga' => $total_harga
		);
		return $this->SalesModel->simpan_det_transaksi($det_transaksi);
	}

	public function update_det_transaksi($no_invoice, $kode_produk, $jumlah, $total_harga){
		$det_transaksi = array(
			'kode_produk' => $kode_produk,
			'jumlah' => $jumlah,
			'total_harga' => $total_harga
		);
		$this->SalesModel->update_det_transaksi($no_invoice, $kode_produk, $det_transaksi);		
	}

	public function simpan_riwayat_transaksi($no_invoice) {
		$kode_history = $this->SalesModel->kode_history_terakhir();
		$kode_history++;

		$history = array(
			'kode_history' => $kode_history,
			'no_invoice' => $no_invoice,
			'status_penjualan' => 'MENUNGGU'
		);
		return $this->SalesModel->simpan_riwayat_transaksi($history);
	}


	public function kurangi_stok($kode_produk, $stok_keluar) {
		$this->load->model('ProdukModel');
		$stok_awal = $this->ProdukModel->get_stok_terakhir($kode_produk);
		$sisa_stok = $stok_awal - $stok_keluar;
		$data = array('stok' => $sisa_stok);
		$id = array('kode_produk' => $kode_produk);
		return $this->SalesModel->kurangi_stok($id, $data);
	}

	public function update_stok($kode_produk, $stok_keluar) {
		$this->load->model('ProdukModel');
		$stok_awal = $this->ProdukModel->get_stok_terakhir($kode_produk);
		$sisa_stok = $stok_awal - $stok_keluar;
		$data = array('stok' => $sisa_stok);
		return $this->SalesModel->update_stok($id, $data);
	}

	public function catat_penjualan() {
		$no_invoice = $this->SalesModel->noinvoice_terakhir();
		$no_invoice++;
		$tanggal_transaksi = $this->input->post('tanggal').' '.date('H:i:s');
		$simpleTanggal_transaksi = $this->input->post('tanggal').', '.date('H:i').' WIB';
		$asal_transaksi = $this->input->post('asal_pesanan');
		$tipe_transaksi = $this->input->post('tipe_transaksi');
		// penerima
		$this->load->model('PelangganModel');
		$kode_pelanggan = $this->input->post('kode_penerima');
		$tipe_pelanggan = $this->input->post('pilih_pelanggan');
		$nama_penerima = $this->input->post('nama_penerima');
		$no_hp = $this->input->post('telp_penerima');
		$provinsi = $this->input->post('origin_province');
		$kota = $this->input->post('origin_city');
		$kecamatan = $this->input->post('kecamatan');
		$alamat = $this->input->post('alamat_penerima');
		$kode_pos = $this->input->post('kodepos');			
		$email = $this->input->post('email');
		// pengirim
		$kode_pengirim = $this->input->post('kode_pengirim');
		$nama_pengirim = $this->input->post('nama_pengirim');
		$toko_pengirim = $this->input->post('pengirim_toko');
		$catatan = $this->input->post('cat_pengirim');	
		$notes = $this->input->post('noteslabel');

		//produk | array
		$kode_produk = $this->input->post('kode_produk');
		$nama_produk = $this->input->post('nama_produk'); 
		$jumlah_barang = $this->input->post('jumlah_barang');
		$harga = $this->input->post('harga');
		$berat = $this->input->post('berat');
		$total_harga = $this->input->post('harga_total');

		$total_barang_all = $this->input->post('total_barang_all');
		$total_berat = $this->input->post('total_berat'); //all
		$total_harga_all  = $this->input->post('total_harga_all');
		$result = array();
		// kurir 
		$jasa = $this->input->post('kurir');
		$service = $this->input->post('service');
		$kurir = strtoupper($jasa.' - '.$service);
		$ongkir = $this->input->post('ongkir');

		$diskon_tambahan = $this->input->post('diskon_all');
		$biaya_tambahan = $this->input->post('biaya_tambahan');
		$biaya_pajak = $this->input->post('biaya_pajak');

		$total_all = $this->input->post('total_all'); // total semuanya
		$jumlah_bayar = $this->input->post('jml_bayar');
		$kurang_bayar = $this->input->post('kurang_bayar');
		$cara_bayar = $this->input->post('bank');
		if ($tipe_pelanggan == 'baru') {
			$kode_pelanggan = $this->PelangganModel->kode_pelanggan_terakhir();
			$kode_pelanggan++;
			$this->tambah_pelanggan($kode_pelanggan, $nama_penerima, $no_hp, $provinsi, $kota, $kecamatan, $alamat, $kode_pos, $email);
		} 

		// simpan transaksi
		if ($this->input->post('no_invoice') != "" || $this->input->post('no_invoice') != null) {
			// update transaksi
			$status = $kurang_bayar == 0 ? "LUNAS" : "BELUM LUNAS";
			$no_invoice = $this->input->post('no_invoice');
			$this->update_transaksi($no_invoice, $tanggal_transaksi, $asal_transaksi, $tipe_transaksi, $kurir, $ongkir, $biaya_tambahan, $diskon_tambahan, $biaya_pajak, $total_all, $jumlah_bayar, $kurang_bayar, $cara_bayar, $status, $catatan, $notes);		
			$result = array();	
			$i=0;
			foreach ($kode_produk as $idx => $kode) {
				$result[] = array(
					'kode_produk' => $kode_produk[$idx],
					'qty' => $jumlah_barang[$idx],
					'total_harga' => $total_harga[$idx] 
				);
				$data_produk = $this->SalesModel->cek_kode_produk($no_invoice, $result[$i]['kode_produk']);
				if ($data_produk->num_rows() == 0) {
					$this->simpan_det_transaksi($no_invoice, $result[$i]['kode_produk'], $result[$i]['qty'], $result[$i]['total_harga']);
				} else {
					$this->update_det_transaksi($no_invoice, $result[$i]['kode_produk'], $result[$i]['qty'], $result[$i]['total_harga']);
				}				
				$this->kurangi_stok($result[$i]['kode_produk'], $result[$i]['qty']);
				$i++;
			}
			$this->session->set_flashdata('update_transaksi', '<div>Transaksi '.$nama_penerima.' Berhasil Diubah</div>');
		} else {
			// tambah transaksi baru
			$status = $kurang_bayar == 0 ? "LUNAS" : "BELUM LUNAS";
			$this->simpan_transaksi($no_invoice, $kode_pelanggan, $kode_pengirim, $tanggal_transaksi, $asal_transaksi, $tipe_transaksi, $kurir, $ongkir, $biaya_tambahan, $diskon_tambahan, $biaya_pajak, $total_all, $jumlah_bayar, $kurang_bayar, $cara_bayar, $status, $catatan, $notes);		
			$result = array();	
			$i=0;
			foreach ($kode_produk as $idx => $kode) {
				$result[] = array(
					'kode_produk' => $kode_produk[$idx],
					'qty' => $jumlah_barang[$idx],
					'total_harga' => $total_harga[$idx] 
				);
				$this->simpan_det_transaksi($no_invoice, $result[$i]['kode_produk'], $result[$i]['qty'], $result[$i]['total_harga']);
				$this->kurangi_stok($result[$i]['kode_produk'], $result[$i]['qty']);
				$i++;
			}
			$this->simpan_riwayat_transaksi($no_invoice);
		}
		
		// pass variable ke modal
		$tbody = "";
		$no = 1;
		foreach ($kode_produk as $idx => $kode) {
			$tbody .= 
			'<tr> 
				<td align="center">'.$no.'</td> <td><strong>'.$nama_produk[$idx].'</strong></td> <td align="center">'.$jumlah_barang[$idx].'</td> <td align="right">'.$this->rupiah($harga[$idx]).'</td><td align="right">'.$this->rupiah($total_harga[$idx]).'</td>
			</tr>';
			$no++;
		}
		$dataModal = array(
			'no_invoice' => $no_invoice, 
			'tanggal' => $simpleTanggal_transaksi, 
			'nama_pengirim' => $nama_pengirim, 
			'nama_toko' => $toko_pengirim, 
			'nama_penerima' => $nama_penerima, 
			'alamat_penerima' => $alamat,
			'kecamatan' => $kecamatan,
			'kota' => $kota,
			'notes' => $notes, 
			'cara_bayar' => $cara_bayar, 
			'kurir' => $kurir, 
			'ongkir' => $this->rupiah($ongkir),
			'asal_transaksi' => $asal_transaksi,
			'diskon_tambahan' => $this->rupiah($diskon_tambahan), 
			'biaya_tambahan' => $this->rupiah($biaya_tambahan), 
			'biaya_pajak' => $this->rupiah($biaya_pajak),
			'total_belanja' => $this->rupiah($total_harga_all),
			'total_all' => $this->rupiah($total_all),
			'produks' => $tbody
		);
		echo json_encode($dataModal);
	}
	
	public function simpan_konfirmasi_pengiriman() {
		$no_invoice = $this->input->post('no_invoice'); // array
		$tgl_kirim = $this->input->post('tgl_kirim'); // array
		$no_resi = $this->input->post('no_resi'); // array
		$no_hp = $this->input->post('no_hp'); //array
		$kurir = $this->input->post('kurir'); // array
		$no_invoice_pembatalan = $this->input->post('no_invoice_pembatalan');
		$ket = $this->input->post('keterangan');
		$kirim_sms = $this->input->post('kirimsms');
		$nama = $this->input->post('nama'); // array
		if ($no_invoice_pembatalan != "") {		
			$this->SalesModel->ganti_status_pesanan($no_invoice_pembatalan, 'BATAL', $ket);
		} else {
			$i = 0;
			foreach ($no_invoice as $key => $value) {
				$results = array(
					'no_invoice' => $no_invoice,
					'no_hp' => $no_hp,
					'tgl_kirim' => $tgl_kirim,
					'no_resi' => $no_resi,
					'kurir' => $kurir,
					'nama' => $nama
				);
				$this->SalesModel->simpan_konfirmasi_pengiriman_tglkirim($results['no_invoice'][$i], $results['tgl_kirim'][$i].' '.date('H:i:s'));
				if (!empty(trim($results['no_resi'][$i]))) {
					$ket = 'Penjualan Berhasil';
					$this->send_sms($results['no_hp'][$i], $results['nama'][$i], $results['kurir'][$i], $results['no_resi'][$i], $this->SalesModel->get_pengirim());
					$this->SalesModel->simpan_konfirmasi_pengiriman_noresi($results['no_invoice'][$i], $results['no_resi'][$i]);
					$this->SalesModel->ganti_status_pesanan($results['no_invoice'][$i], 'BERHASIL', $ket);
				}
				$i++;
			}
			redirect('Sales/konfirmasi_pengiriman');
		}
		
	}



	// format rupiah
	private function rupiah($angka){
		$hasil_rupiah = number_format($angka,0,',','.');
		$rp = strval($hasil_rupiah);	
		return $rp;
	}
	// API rajaongkir

	public function get_province() {
		$this->load->model('PelangganModel');
		return $this->PelangganModel->get_provinsi();
	}

	public function get_city_by_province($province_id){
		$this->load->model('PelangganModel');
		$city = $this->PelangganModel->get_kota($province_id);
		$output = '<option>- Kota -</option>';
		foreach ($city->result() as $cty) {
			$output .='<option value="'.$cty->city_id.'">'.$cty->type.' '.$cty->city_name.'</option>';
		}
		echo $output;
	}

	public function get_city_by_id($id, $province_id) {
		$this->load->model('PelangganModel');
		$kota_selected = $this->PelangganModel->get_city_by_id($id);
		foreach ($kota_selected->result() as $value) {
			$output = '<option value="'.$value->city_id.'" selected>'.$value->type.' '.$value->city_name.'</option>';
		}
		
		$city = $this->PelangganModel->get_kota($province_id);
		foreach ($city->result() as $cty) {
			$output .='<option value="'.$cty->city_id.'">'.$cty->type.' '.$cty->city_name.'</option>';
		}
		echo $output;
	}
	

	public function cek_ongkir($asal, $tujuan, $berat, $kurir) {
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "origin=$asal&destination=$tujuan&weight=$berat&courier=$kurir",
		  CURLOPT_HTTPHEADER => array(
		    "content-type: application/x-www-form-urlencoded",
		    "key: cbf4cb300a3e9dfe9cd1f6bb1e6442a8"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  return json_decode($response) ;
		}
	}

	public function get_service($asal, $tujuan, $berat, $kurir){
		$service = $this->cek_ongkir($asal, $tujuan, $berat, $kurir);
		$i=0;
		print_r($service);
		$output = '<option value="0">- Service -</option>';
		foreach ($service->rajaongkir->results[0]->costs as $serv) {
			if ($kurir == 'pos') {
				$output .='<option value="'.$serv->service.'">'.$serv->description.' | '.$serv->cost[0]->etd.'</option>';
			} else {
				$output .='<option value="'.$serv->service.'">'.$serv->description.' ('.$serv->service.') | '.$serv->cost[0]->etd.' Hari</option>';
			}

			$i++;
		}
		echo $output;	
	}

	public function get_harga_service($asal, $tujuan, $berat, $kurir, $service){
		$ongkir = $this->cek_ongkir($asal, $tujuan, $berat, $kurir);
		$i = 0;
		$cost = 0;
		foreach ($ongkir->rajaongkir->results[0]->costs as $data) {
			if ($ongkir->rajaongkir->results[0]->costs[$i]->service == $service) {
				$cost = $data->cost[0]->value;						
				
			}
			$i++;
		}
		echo $cost;
	}

	public function get_autocomplete(){
		$this->load->model('ProdukModel');
		if (isset($_GET['term'])) {
			$arr_result = array();
			$result = $this->ProdukModel->search_produk($_GET['term']);
			if (count($result) > 0) {
			foreach ($result as $row)
					$arr_result[] = $row->nama_produk.' (SKU: '.$row->kode_produk.')';
					echo json_encode($arr_result);
			}
		}
	}

	public function get_autocomplete_pelanggan(){
		$this->load->model('PelangganModel');
		if (isset($_GET['term'])) {
			$arr_result = array();
			$result = $this->PelangganModel->search_pelanggan($_GET['term']);
			if (count($result) > 0) {
			foreach ($result as $row)
					$arr_result[] = $row->nama.' / '.$row->no_hp;
					echo json_encode($arr_result);
			}
		}
	}

	public function get_data_produk($id) {
		$data = $this->SalesModel->get_data_produk($id);
		echo json_encode($data);
	}

	public function get_data_pelanggan($no_hp) {
		$data = $this->SalesModel->get_data_pelanggan($no_hp);
		echo json_encode($data);
	}

	public function history_transaksi() {
		$data['data'] = $this->SalesModel->history_transaksi();
		$this->load->view('history_transaksi', $data);
	}

	public function rekap_penjualan() {
		$this->load->view('rekap_penjualan');
	}

  public function export_penjualan() {
  	$status_penjualan = $this->input->post('status_penjualan');
  	$tanggalAwal = $this->input->post('tanggalAwal');
    $tanggalAkhir = $this->input->post('tanggalAkhir');
    switch ($status_penjualan) {
      case 'ALL':
          $ket = 'Semua Transaksi Penjualan';
        break;
      case 'BERHASIL':
          $ket = 'Transaksi Berhasil';
        break;
      case 'BATAL':
          $ket = 'Transaksi Batal ';
        break;
      case 'MENUNGGU':
          $ket = 'Transaksi Menunggu';
        break;  
    }
    $data = $this->SalesModel->rekap_transaksi($status_penjualan, $tanggalAwal, $tanggalAkhir);
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getStyle(1)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle(2)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle(3)->getFont()->setBold(true);
    $header = array(
      'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
      ),
      'font' => array(
          'bold' => true,
          'color' => array('rgb' => 'FF0000'),
          'name' => 'Verdana'
      )
    );
    $objPHPExcel->getActiveSheet()->getStyle("A1:D2")
            ->applyFromArray($header)
            ->getFont()->setSize(16);
    $objPHPExcel->getActiveSheet()->mergeCells('A1:F2');
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Rekap '.$ket.' Tanggal: '.date('d/m/Y').' ')
        ->setCellValue('A3', 'No.')
        ->setCellValue('B3', 'Tanggal Transaksi')
        ->setCellValue('C3', 'Invoice')
        ->setCellValue('D3', 'Nama')
        ->setCellValue('E3', 'Kota')
        ->setCellValue('F3', 'Penjualan (Rp)')
        ->getStyle('A3:F3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
    $ex = $objPHPExcel->setActiveSheetIndex(0);
    $no = 1;
    $counter = 4;
    $total = 0;
    foreach ($data->result() as $row) {
      $ex->setCellValue('A'.$counter, $no++);
      $ex->setCellValue('B'.$counter, $row->tanggal);
      $ex->setCellValue('C'.$counter, $row->no_invoice);
      $ex->setCellValue('D'.$counter, $row->nama);
      $ex->setCellValue('E'.$counter, $row->kota);
      $ex->setCellValue('F'.$counter, $row->total_biaya_transaksi);
      $total = $row->total_biaya_transaksi + $total;
      $counter = $counter+1;
    }
    $ex->setCellValue('E'.++$counter, 'TOTAL');
    $ex->setCellValue('F'.$counter, $total);            
    $objPHPExcel->getActiveSheet()->setTitle('Rekap Penjualan');
    
    $objWriter  = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    header('Last-Modified:'. gmdate("D, d M Y H:i:s").'GMT');
    header('Chace-Control: no-store, no-cache, must-revalation');
    header('Chace-Control: post-check=0, pre-check=0', FALSE);
    header('Pragma: no-cache');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="rekap_'.$ket.'_'. date('d-m-Y') .'.xlsx"');
    ob_end_clean();
    $objWriter->save('php://output');
  }

  public function cek() {
  	$data = $this->SalesModel->get_data_bank();
  	print_r($data->result());
	}


}
