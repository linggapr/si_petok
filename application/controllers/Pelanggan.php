<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('PelangganModel');
	}

	public function index() {
		$data['data'] = $this->PelangganModel->tampil_pelanggan();
		$data['province'] = $this->PelangganModel->get_provinsi();
		$this->load->view('daftar_pelanggan', $data);
	}

	public function simpan_data_pelanggan() {
		$kode_pelanggan = $this->PelangganModel->kode_pelanggan_terakhir();
		$nama = $this->input->post('nama');
		$no_hp = $this->input->post('no_hp');
		$email = $this->input->post('email');
		$provinsi = $this->input->post('provinsi');
		$kota = $this->input->post('kota');
		$kecamatan = $this->input->post('kecamatan');
		$alamat = $this->input->post('alamat');
		$kodepos = $this->input->post('kode_pos');

		$data = array(
			'kode_pelanggan' => ++$kode_pelanggan,
			'nama' => $nama,
			'no_hp' => $no_hp,
			'email' => $email,
			'provinsi' => $provinsi,
			'kota' => $kota,
			'kodepos' => $kodepos,
			'kecamatan' => $kecamatan,
			'alamat' => $alamat
		);

		if ($this->PelangganModel->tambah_pelanggan($data) == true) {
			$this->session->set_flashdata('tambah_konsumen', "<div>Pelanggan Berhasil Ditambahkan</div>");
		} else {
			$this->session->set_flashdata('gagal_tambah', "<div>Pelanggan Gagal Ditambahkan</div>");
		}
		redirect('Pelanggan');
	}

	public function hal_ubah_pelanggan($kode_pelanggan) {
		$data = $this->PelangganModel->get_data_pelanggan($kode_pelanggan);
		foreach ($data->result_array() as $value) {
			 $row = $value;
		}
		echo json_encode($row);
	}

	public function ubah_pelanggan() {
		$kode_pelanggan = $this->input->post('ubah_kode_pelanggan');
		$nama = $this->input->post('nama');
		$no_hp = $this->input->post('no_hp');
		$email = $this->input->post('email');
		$kode_pos = $this->input->post('kode_pos');
		$provinsi = $this->input->post('provinsi');
		$kota = $this->input->post('kota');
		$kecamatan = $this->input->post('kecamatan');
		$alamat = $this->input->post('alamat');

		$data = array(
			'nama' => $nama,
			'no_hp' => $no_hp,
			'email' => $email,
			'provinsi' => $provinsi,
			'kota' => $kota,
			'kodepos' => $kode_pos,
			'kecamatan' => $kecamatan,
			'alamat'=> $alamat
		);

		if ($this->PelangganModel->ubah_pelanggan($kode_pelanggan, $data) == true) {
			$this->session->set_flashdata('edit_konsumen', "<div>Pelanggan Berhasil Diubah</div>");
		} 
		redirect('Pelanggan');
	}
	public function hapus_pelanggan($kode_pelanggan) {
		if ($this->PelangganModel->hapus_pelanggan($kode_pelanggan) == true) {
			$this->session->set_flashdata('hapus_pelanggan', "<div>Pelanggan Berhasil Dihapus</div>");
		}
		redirect('Pelanggan');
	}
	
	public function get_city_by_province($province_id){
		$city = $this->PelangganModel->get_kota($province_id);
		$output = '<option>- Kota -</option>';
		foreach ($city->result() as $cty) {
			$output .='<option value="'.$cty->city_id.'">'.$cty->type.' '.$cty->city_name.'</option>';
		}
		echo $output;
	}

	public function get_city_by_id($id, $province_id) {
		$kota_selected = $this->PelangganModel->get_city_by_id($id);
		foreach ($kota_selected->result() as $value) {
			$output = '<option value="'.$value->city_id.'" selected>'.$value->type.' '.$value->city_name.'</option>';
		}
		
		$city = $this->PelangganModel->get_kota($province_id);
		foreach ($city->result() as $cty) {
			$output .='<option value="'.$cty->city_id.'">'.$cty->type.' '.$cty->city_name.'</option>';
		}
		echo $output;
	}

}
