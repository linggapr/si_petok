<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengeluaran extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('IncomeModel');
		date_default_timezone_set("Asia/Bangkok");
	}

	public function index()
	{
		$this->load->view('pengeluaran');
	}

	public function jenis_pengeluaran() {
		$data['nama_pengeluaran'] = $this->IncomeModel->daftar_jenis_pengeluaran();
		$this->load->view('jenis_pengeluaran', $data);
	}	

	public function tambah_jenis_pengeluaran() {
		$id = $this->IncomeModel->id_jenis_pengeluaran_terakhir();
		$nama_pengeluaran = $this->input->post('nama_pengeluaran');
		$deskripsi = $this->input->post('deskripsi');

		$data = array(
			'id' => ++$id,
			'nama_pengeluaran' => $nama_pengeluaran,
			'deskripsi' => $deskripsi
		);
		if ($this->IncomeModel->simpan_jenis_pengeluaran($data) == true) {
			$this->session->set_flashdata('tambah_jenis_pengeluaran', "<div>Jenis Pengeluaran Berhasil Ditambah</div>");
		}
		redirect('Pengeluaran/jenis_pengeluaran');
	}	

	public function get_jenis_pengeluaran($id) {
		$data = $this->IncomeModel->get_jenis_pengeluaran($id);
		foreach ($data->result_array() as $value) {
			 $row = $value;
		}
		echo json_encode($row);
	}

	public function ubah_jenis_pengeluaran() {
		$id = $this->input->post('id_jenis_pengeluaran');
		$nama_pengeluaran = $this->input->post('nama_pengeluaran');
		$deskripsi = $this->input->post('deskripsi');

		$data = array(
			'nama_pengeluaran' => $nama_pengeluaran,
			'deskripsi' => $deskripsi
		);
		if ($this->IncomeModel->ubah_jenis_pengeluaran($id, $data) == true) {
			$this->session->set_flashdata('edit_jenispengeluaran', '<div>Jenis Pengeluaran Berhasil Diubah</div>');
		}
		redirect('Pengeluaran/jenis_pengeluaran');
	}

	public function hapus_jenis_pengeluaran($id) {
		if ($this->IncomeModel->hapus_jenis_pengeluaran($id) == true) {
			$this->session->set_flashdata('hapus_jenis_pengeluaran', "<div>Jenis Pengeluaran Berhasil Dihapus</div>");
		}
		redirect('Pengeluaran/jenis_pengeluaran');
	}

	public function history_pengeluaran() {
		$data['data'] = $this->IncomeModel->history_pengeluaran();
		$this->load->view('history_pengeluaran', $data);
	}

	public function tambah_pengeluaran() {
		$no_bukti = $this->IncomeModel->get_last_nobukti_pengeluaran();
		$no_bukti++;
		$tanggal = $this->input->post('tanggal').' '.date('H:i:s');
		$dikeluarkan_dari = $this->input->post('dikeluarkan_dari');
		$notes = $this->input->post('catatan');
		$kode_jenis_pengeluaran = $this->input->post('kode_pengeluaran'); // array
		$jumlah_pengeluaran = $this->input->post('jumlah'); // array

		$data = array(
				'no_bukti' => $no_bukti,
				'tanggal' => $tanggal,
				'dikeluarkan_dari' => $dikeluarkan_dari,
				'catatan' => $notes,
				'total' => array_sum($jumlah_pengeluaran)
		);
		$this->IncomeModel->simpan_pengeluaran($data);
		$i=0; 
		foreach ($kode_jenis_pengeluaran as $idx => $kode) {
			$result[] = array(
				'id' => $kode_jenis_pengeluaran[$idx],
				'jumlah' => $jumlah_pengeluaran[$idx] 
			);

			$detail_pengeluaran = array(
				'no_bukti' => $no_bukti,
				'id_jenis_pengeluaran' => $result[$i]['id'],
				'jumlah' => $result[$i]['jumlah']
			);
			$this->IncomeModel->simpan_det_pengeluaran($detail_pengeluaran);	
			$i++;
		}
		$this->session->set_flashdata('tambah_pengeluaran', '<div>Pengeluaran Berhasil Di-input</div>');
		redirect('Pengeluaran');
	}

	public function get_history_pengeluaran($no_bukti) {
		$data = $this->IncomeModel->get_history_pengeluaran($no_bukti);
		foreach ($data->result_array() as $value) {
			$rows[] = $value;
		}
		echo json_encode($rows);
	}

	public function pengeluaran_batal($no_bukti) {
		$this->IncomeModel->pengeluaran_batal($no_bukti);
		$this->session->set_flashdata('pengeluaran_batal', '<div>Pengeluaran Dibatalkan!</div>');
		redirect('Pengeluaran/history_pengeluaran');
	}

	public function get_autocomplete() {
		if (isset($_GET['term'])) {
			$arr_result = array();
			$result = $this->IncomeModel->search_jenis_pengeluaran($_GET['term']);
			if (count($result) > 0) {
			foreach ($result as $row)
					$arr_result[] = '#'.$row->id.' | '.$row->nama_pengeluaran;
					echo json_encode($arr_result);
			}
		}
	}

}

