<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemasukkan extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('IncomeModel');
		date_default_timezone_set("Asia/Bangkok");
	}

	public function index()
	{
		$this->load->view('pemasukkan');
	}

	public function jenis_pemasukkan() {
		$data['nama_pemasukkan'] = $this->IncomeModel->daftar_jenis_pemasukkan();
		$this->load->view('jenis_pemasukkan', $data);
	}

	public function tambah_jenis_pemasukkan() {
		$id = $this->IncomeModel->id_jenis_pemasukkan_terakhir();
		$nama_pemasukkan = $this->input->post('nama_pemasukkan');
		$deskripsi = $this->input->post('deskripsi');

		$data = array(
			'id' => ++$id,
			'nama_pemasukkan' => $nama_pemasukkan,
			'deskripsi' => $deskripsi
		);
		if ($this->IncomeModel->simpan_jenis_pemasukkan($data) == true) {
			$this->session->set_flashdata('tambah_jenis_pemasukkan', "<div>Jenis Pemasukkan Berhasil Dihapus</div>");
		}
		redirect('Pemasukkan/jenis_pemasukkan');
	}

	public function ubah_jenis_pemasukkan() {
		$id = $this->input->post('id_jenis_pemasukkan');
		$nama_pemasukkan = $this->input->post('nama_pemasukkan');
		$deskripsi = $this->input->post('deskripsi');

		$data = array(
			'nama_pemasukkan' => $nama_pemasukkan,
			'deskripsi' => $deskripsi
		);
		if ($this->IncomeModel->ubah_jenis_pemasukkan($id, $data) == true) {
			$this->session->set_flashdata('edit_jenispemasukkan', '<div>Jenis Pemasukkan Berhasil Diubah</div>');
		}
		redirect('Pemasukkan/jenis_pemasukkan');
	}

	public function get_jenis_pemasukkan($id) {
		$data = $this->IncomeModel->get_jenis_pemasukkan($id);
		foreach ($data->result_array() as $value) {
			 $row = $value;
		}
		echo json_encode($row);
	}

	public function hapus_jenis_pemasukkan($id) {
		if ($this->IncomeModel->hapus_jenis_pemasukkan($id) == true) {
			$this->session->set_flashdata('hapus_jenis_pemasukkan', "<div>Jenis Pemasukkan Berhasil Dihapus</div>");
		}
		redirect('Pemasukkan/jenis_pemasukkan');
	}

	public function history_pemasukkan() {
		$data['data'] = $this->IncomeModel->history_pemasukkan();
		$this->load->view('history_pemasukkan', $data);
	}

	public function tambah_pemasukkan() {
		$no_bukti = $this->IncomeModel->get_last_nobukti_pemasukkan();
		$no_bukti++;
		$tanggal = $this->input->post('tanggal').' '.date('H:i:s');
		$penerima = $this->input->post('penerima');
		$notes = $this->input->post('catatan');
		$kode_jenis_pemasukkan = $this->input->post('kode_pemasukkan'); // array
		$jumlah_pemasukkan = $this->input->post('jumlah'); // array

		$data = array(
				'no_bukti' => $no_bukti,
				'tanggal' => $tanggal,
				'penerima' => $penerima,
				'catatan' => $notes,
				'total' => array_sum($jumlah_pemasukkan)
		);
		$this->IncomeModel->simpan_pemasukkan($data);

		$i=0;
		foreach ($kode_jenis_pemasukkan as $idx => $kode) {
			$result[] = array(
				'id' => $kode_jenis_pemasukkan[$idx],
				'jumlah' => $jumlah_pemasukkan[$idx] 
			);	

			$detail_pemasukkan = array(
				'no_bukti' => $no_bukti,
				'id_jenis_pemasukkan' => $result[$i]['id'],
				'jumlah' => $result[$i]['jumlah']
			);
			$this->IncomeModel->simpan_det_pemasukkan($detail_pemasukkan);	
			$i++;
		}
		$this->session->set_flashdata('tambah_pemasukkan', '<div>Pemasukkan Berhasil Di-input</div>');
		redirect('Pemasukkan');
	}

	public function get_history_pemasukkan($no_bukti) {
		$data = $this->IncomeModel->get_history_pemasukkan($no_bukti);
		foreach ($data->result_array() as $value) {
			$rows[] = $value;
		}
		echo json_encode($rows);
	}

	public function pemasukkan_batal($no_bukti) {
		$this->IncomeModel->pemasukkan_batal($no_bukti);
		$this->session->set_flashdata('pemasukkan_batal', '<div>Pemasukkan Dibatalkan!</div>');
		redirect('Pemasukkan/history_pemasukkan');
		
	}

	public function get_autocomplete() {
		if (isset($_GET['term'])) {
			$arr_result = array();
			$result = $this->IncomeModel->search_jenis_pemasukkan($_GET['term']);
			if (count($result) > 0) {
			foreach ($result as $row)
					$arr_result[] = '#'.$row->id.' | '.$row->nama_pemasukkan;
					echo json_encode($arr_result);
			}
		}
	}

}
