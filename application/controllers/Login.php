<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index() {
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		
		if ($this->session->userdata('username') != '') {
			$this->load->view('dashboard');		
		} else {
			$this->load->view('login');
		}		
	}

	public function login_validation() {
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run()) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$this->load->model('LoginModel');
			if ($this->LoginModel->login_sukses($username, $password)) {
				$session_data = array(
					'username' => $username
				);

				$this->session->set_userdata($session_data);
				redirect(base_url(), 'Login/index');
			} else {
				$this->session->set_flashdata('error', 'invalid Username or Password');
				redirect(base_url(),'Login/login');
			}
		} else {
			$this->login();
		}
	}

	public function logout() {
		$this->session->unset_userdata('username');
		redirect(base_url(),'Login/login');
	}

}
