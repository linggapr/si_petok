<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('ProdukModel');
    $this->load->library('Zend');
    $this->zend->load('Zend/Barcode');
    $this->load->library('PHPExcel');
  }


  public function daftar_produk() {
    $data['data'] = $this->ProdukModel->get_produk();
    $data['data_grosir'] = $this->ProdukModel->tampil_grosir();
    $this->load->view('daftar_produk', $data);
  }

  public function daftar_kategori() {
    $data['kategori'] = $this->ProdukModel->tampil_kategori();
    $this->load->view('daftar_kategori', $data);
  } 

  public function tambah_kategori() {
    $kode_kategori = $this->ProdukModel->kode_kategori_terakhir();
    $kode_kategori++;
    $nama_kategori = $this->input->post('nama_kategori');

    $data = array(
      'kode_kategori' => $kode_kategori,
      'nama_kategori' => $nama_kategori
    );

    if ($this->ProdukModel->added_kategori($data) == true) {
      $this->session->set_flashdata('kategori_tambah', '<div>Kategori Baru Berhasil Ditambahkan</div>');
    }

    redirect('Produk/daftar_kategori');
  }
  public function hal_ubah_kategori($kode_kategori) {
    $data = $this->ProdukModel->get_kategori_byid($kode_kategori);
    foreach ($data->result_array() as $value) {
      $row = $value;
    }
    echo json_encode($row);
  }

  public function ubah_kategori() {
    $kode_kategori = $this->input->post('kode_kategori');
    $nama_kategori = $this->input->post('nama_kategori');

    $data = array('nama_kategori' => $nama_kategori);
    $id = array('kode_kategori' => $kode_kategori);
    if ($this->ProdukModel->update_data($id, $data, 'tb_kategori') == true) {
      $this->session->set_flashdata('kategori_update', '<div>Kategori Berhasil Diubah</div>');
    }
    redirect('Produk/daftar_kategori');
  }

  public function hapus_kategori($kode_kategori) {
    if ($this->ProdukModel->hapus_kategori($kode_kategori) == true) {
      $this->session->set_flashdata('kategori_delete', '<div>Kategori Berhasil Dihapus</div>');
    }
    redirect('Produk/daftar_kategori');
  }

  public function tambah_produk() {
  	$data['kategori'] = $this->ProdukModel->tampil_kategori();
  	$this->load->view('tambah_produk', $data);
  } 


  public function added_produk() {
  	$kode_produk = $this->ProdukModel->kode_produk_terakhir();
  	$kode_produk++;
  	$nama_produk = $this->input->post('nama_produk');
  	$deskripsi = $this->input->post('deskripsi');
  	$berat = $this->input->post('berat');
  	$jenis = $this->input->post('jenis');
  	$stok = $this->input->post('stok');
  	$harga = $this->input->post('harga');
  	$diskon = $this->input->post('diskon');
  	$ukuran = $this->input->post('ukuran');
  	$warna = $this->input->post('warna');
    // harga grosir
    $min_order1 = $this->input->post('harga_grosir_min_1'); 
    $harga_grosir1 = $this->input->post('harga_grosir_1');
    $min_order2 = $this->input->post('harga_grosir_min_2'); 
    $harga_grosir2 = $this->input->post('harga_grosir_2');
    $min_order3 = $this->input->post('harga_grosir_min_3'); 
    $harga_grosir3 = $this->input->post('harga_grosir_3');
    $min_order4 = $this->input->post('harga_grosir_min_4'); 
    $harga_grosir4 = $this->input->post('harga_grosir_4');
    $status_grosir = ($min_order1 != 0 || $harga_grosir1 != 0) ? 'YA' : 'TIDAK' ;
    //gambar
    $gambar = '';
  	$config['upload_path'] = './uploads/';
  	$config['allowed_types'] = 'gif|jpeg|png|jpg';
  	$this->load->library('upload', $config);
  	if (!$this->upload->do_upload('gambar')){
  		$error = array('error' => $this->upload->display_errors());
  	} else {
  		$file_data = $this->upload->data();
  		$gambar = $file_data['file_name'];
  	}


  	if ($this->input->post('kategori') != '0') {
  		$kategori = $this->input->post('kategori');
  	} else {
  		$kategori = $this->input->post('kategori_baru');
  		$kode_kategori = $this->ProdukModel->kode_kategori_terakhir();
   		$kategori_baru = array ('kode_kategori' => ++$kode_kategori, 'nama_kategori' => $kategori);
  		$data_kategori = array_merge($kategori_baru);
  		$this->ProdukModel->added_kategori($data_kategori);
  		$kategori = $kode_kategori;
  	}

  	$produk = array(
  		'kode_produk' => $kode_produk,
  		'nama_produk' => $nama_produk,
  		'deskripsi' => $deskripsi,
  		'berat' => $berat,
  		'ukuran' => $ukuran,
  		'warna' => $warna,
  		'kode_jenis' => $jenis,
  		'harga' => $harga,
      'grosir' => $status_grosir,
  		'diskon' => $diskon,
  		'stok' => $stok,
  		'gambar' => $gambar,
      'barcode' => $this->generate_barcode($kode_produk),
  		'kode_kategori' => $kategori
  	);

  	$data  = array_merge($produk);
  	if ($this->ProdukModel->added_produk($data) == TRUE) {
  		//insert harga grosir
      if ($min_order1 != 0) {
        $grosir1 = array('min_order' => $min_order1, 'harga_grosir'=> $harga_grosir1, 'kode_produk'=> $kode_produk);
        $grosir2 = array('min_order' => $min_order2, 'harga_grosir'=> $harga_grosir2, 'kode_produk'=> $kode_produk);
        $grosir3 = array('min_order' => $min_order3, 'harga_grosir'=> $harga_grosir3, 'kode_produk'=> $kode_produk);
        $grosir4 = array('min_order' => $min_order4, 'harga_grosir'=> $harga_grosir4, 'kode_produk'=> $kode_produk);
        $this->ProdukModel->added_harga_grosir($grosir1);
        $this->ProdukModel->added_harga_grosir($grosir2);
        $this->ProdukModel->added_harga_grosir($grosir3);
        $this->ProdukModel->added_harga_grosir($grosir4);
      }
      // simpan history
      $kode_history = $this->ProdukModel->kode_history_terakhir();
      $catatan = 'Penambahan produk baru';
      $jenis = 'STOK MASUK';
      $jumlah = $stok;
      $stok_masuk = array(
        'kode_history' => ++$kode_history,
        'tanggal' => date("Y-m-d"),
        'catatan' => $catatan,
        'jenis' => $jenis,
        'jumlah' => $jumlah,
        'kode_produk' => $kode_produk
      );
      $this->ProdukModel->catat_history($stok_masuk);
      $this->session->set_flashdata('add_produk', "<div>Kode Produk $kode_produk Berhasil Disimpan</div>");
  	} else {
  		$this->session->set_flashdata('add_produk', "<div>Kode Produk $kode_produk tidak tersimpan</div>");
  	}
  	return redirect('Produk/daftar_produk');
  }

  public function ubah_produk($id) {
    $id = array('kode_produk' => $id);
    $data['produk'] = $this->ProdukModel->ubah_produk($id);
    $data['kategori'] = $this->ProdukModel->tampil_kategori();
    $data['jenises'] = $this->ProdukModel->tampil_jenis();
    $data['grosires'] = $this->ProdukModel->tampil_grosir();
    $this->load->view('ubah_produk', $data);
  }

  public function update() {
    $kode_produk = $this->input->post('kode_produk');
    $nama_produk = $this->input->post('nama_produk');
    $deskripsi = $this->input->post('deskripsi');
    $berat = $this->input->post('berat');
    $jenis = $this->input->post('jenis');
    $stok = $this->input->post('stok');
    $harga = $this->input->post('harga');
    $diskon = $this->input->post('diskon');
    $ukuran = $this->input->post('ukuran');
    $warna = $this->input->post('warna');
    // harga grosir
    $min_order1 = $this->input->post('harga_grosir_min_1'); 
    $harga_grosir1 = $this->input->post('harga_grosir_1');
    $min_order2 = $this->input->post('harga_grosir_min_2'); 
    $harga_grosir2 = $this->input->post('harga_grosir_2');
    $min_order3 = $this->input->post('harga_grosir_min_3'); 
    $harga_grosir3 = $this->input->post('harga_grosir_3');
    $min_order4 = $this->input->post('harga_grosir_min_4'); 
    $harga_grosir4 = $this->input->post('harga_grosir_4');
    $status_grosir = ($min_order1 != 0 || $harga_grosir1 != 0) ? 'YA' : 'TIDAK' ;
    //gambar
    $gambar = '';   
    $config['upload_path'] = './uploads/';
    $config['allowed_types'] = 'gif|jpeg|png|jpg';
    $this->load->library('upload', $config);
    if (!$this->upload->do_upload('gambar')){
      $error = array('error' => $this->upload->display_errors());
    } else {
      $file_data = $this->upload->data();
      $gambar = $file_data['file_name'];
    }


    if ($this->input->post('kategori') != '0') {
      $kategori = $this->input->post('kategori');
    } else {
      $kategori = $this->input->post('kategori_baru');
      $kode_kategori = implode("|", $this->ProdukModel->kode_kategori_terakhir());
      $kategori_baru = array ('kode_kategori' => ++$kode_kategori, 'nama_kategori' => $kategori);
      $data_kategori = array_merge($kategori_baru);
      $this->ProdukModel->added_kategori($data_kategori);
      $kategori = $kode_kategori;
    }

    if (!empty($_FILES['gambar']['name'])) {
      $produk = array(
        'kode_produk' => $kode_produk,
        'nama_produk' => $nama_produk,
        'deskripsi' => $deskripsi,
        'berat' => $berat,
        'ukuran' => $ukuran,
        'warna' => $warna,
        'kode_jenis' => $jenis,
        'harga' => $harga,
        'grosir' => $status_grosir,
        'diskon' => $diskon,
        'stok' => $stok,
        'gambar' => $gambar,
        'kode_kategori' => $kategori
      );
    } else {
      $produk = array(
        'kode_produk' => $kode_produk,
        'nama_produk' => $nama_produk,
        'deskripsi' => $deskripsi,
        'berat' => $berat,
        'ukuran' => $ukuran,
        'warna' => $warna,
        'kode_jenis' => $jenis,
        'harga' => $harga,
        'grosir' => $status_grosir,
        'diskon' => $diskon,
        'stok' => $stok,
        'kode_kategori' => $kategori
      );
    }
    $id = array ('kode_produk' => $kode_produk);
    if ($this->ProdukModel->update_data($id, $produk, 'tb_produk') == TRUE) {
      //update harga grosir
      $grosir1 = array('min_order' => $min_order1, 'harga_grosir'=> $harga_grosir1, 'kode_produk'=> $kode_produk);
      $grosir2 = array('min_order' => $min_order2, 'harga_grosir'=> $harga_grosir2, 'kode_produk'=> $kode_produk);
      $grosir3 = array('min_order' => $min_order3, 'harga_grosir'=> $harga_grosir3, 'kode_produk'=> $kode_produk);
      $grosir4 = array('min_order' => $min_order4, 'harga_grosir'=> $harga_grosir4, 'kode_produk'=> $kode_produk);

      if ($_POST['id_grosir1'] != 0) {
        $id_grosir1 = array('id' => $_POST['id_grosir1']);
        $id_grosir2 = array('id' => $_POST['id_grosir2']);
        $id_grosir3 = array('id' => $_POST['id_grosir3']);
        $id_grosir4 = array('id' => $_POST['id_grosir4']);

        $this->ProdukModel->update_data($id_grosir1, $grosir1, 'tb_harga_grosir');
        $this->ProdukModel->update_data($id_grosir2, $grosir2, 'tb_harga_grosir');
        $this->ProdukModel->update_data($id_grosir3, $grosir3, 'tb_harga_grosir');
        $this->ProdukModel->update_data($id_grosir4, $grosir4, 'tb_harga_grosir');
      } else {
        $this->ProdukModel->added_harga_grosir($grosir1);
        $this->ProdukModel->added_harga_grosir($grosir2);
        $this->ProdukModel->added_harga_grosir($grosir3);
        $this->ProdukModel->added_harga_grosir($grosir4);
      }
      $this->session->set_flashdata('edit_produk', "<div>Kode Produk $kode_produk Berhasil Diubah</div>");
    } else {
      $this->session->set_flashdata('edit_produk', "<div>Kode Produk $kode_produk Gagal diubah</div>");
    }
    return redirect('Produk/daftar_produk');
  }

  public function hapus_produk($id) {
  	$this->ProdukModel->delete_produk($id);
  	$this->session->set_flashdata('delete_produk', "<div>Data Berhasil Dihapus</div>");
    return redirect('Produk/daftar_produk');
  }  

  public function generate_barcode($kode) {
    $imageResource = Zend_Barcode::factory('code128', 'image', array('text'=>$kode), array())->draw();
    imagepng($imageResource, './uploads/barcode/barcode_'.$kode.'.png');
    return 'barcode_'.$kode.'.png';
  }
    
  // Update Stok
  public function get_autocomplete(){
    if (isset($_GET['term'])) {
        $result = $this->ProdukModel->search_produk($_GET['term']);
        if (count($result) > 0) {
        foreach ($result as $row)
            $arr_result[] = $row->kode_produk.' | '.$row->nama_produk.' | Stok : '. $row->stok;
            echo json_encode($arr_result);
        }
    }
  }

  public function update_stok() {
    $this->load->view('update_stok');
  }

  public function tambah_stok() {
    $tanggal = $this->input->post('tanggal');
    $catatan = $this->input->post('catatan');
    $jenis = 'STOK MASUK';

    $kode_produk = $this->input->post('kode_produk');
    $jum_stok = $this->input->post('jumlah_barang');
    $i=0;
    foreach ($kode_produk as $idx => $kode) {
      $result[] = array(
        'kode_produk' => $kode_produk[$idx],
        'stok' => $jum_stok[$idx]
      );
      $stok_awal = $this->ProdukModel->get_stok_terakhir($result[$i]['kode_produk']);
      $stok = $stok_awal + $result[$i]['stok'];
      
      $data_stok = array ('stok' => $stok);
      $kode_history = $this->ProdukModel->kode_history_terakhir();
      $data_history = array (
        'kode_history' => ++$kode_history, 
        'tanggal' => $tanggal, 
        'catatan' => $catatan,
        'jenis' => $jenis,
        'jumlah' =>  $result[$i]['stok'],
        'kode_produk' => $result[$i]['kode_produk']
      );
      $id = array('kode_produk' => $result[$i]['kode_produk']);
      $this->ProdukModel->update_data($id, $data_stok, 'tb_produk');
      $this->ProdukModel->catat_history($data_history);
      $i++;
    }
    $this->session->set_flashdata('stok_masuk', '<div>Stok Produk Berhasil Ditambahkan</div>');
    return redirect('Produk/update_stok');
  }

  public function kurangi_stok() {
    $tanggal = $this->input->post('tanggal');
    $catatan = $this->input->post('catatan');
    $jenis = 'STOK KELUAR';

    $kode_produk = $this->input->post('kode_produk');
    $jum_stok = $this->input->post('jumlah_barang');
    $i=0;
    foreach ($kode_produk as $idx => $kode) {
      $result[] = array(
        'kode_produk' => $kode_produk[$idx],
        'stok' => $jum_stok[$idx]
      );
      $stok_awal = $this->ProdukModel->get_stok_terakhir($result[$i]['kode_produk']);
      $stok = $stok_awal - $result[$i]['stok'];
      $data_stok = array ('stok' => $stok);
      $kode_history = $this->ProdukModel->kode_history_terakhir();
      $data_history = array (
        'kode_history' => ++$kode_history, 
        'tanggal' => $tanggal, 
        'catatan' => $catatan,
        'jenis' => $jenis,
        'jumlah' =>  $result[$i]['stok'],
        'kode_produk' => $result[$i]['kode_produk']
      );
      $id = array('kode_produk' => $result[$i]['kode_produk']);
      $this->ProdukModel->update_data($id, $data_stok, 'tb_produk');
      $this->ProdukModel->catat_history($data_history);
      $i++;
    }
   
    $this->session->set_flashdata('stok_keluar', '<div>Stok Produk Berhasil Dikurangi</div>');
    return redirect('Produk/stok_keluar');
  }

  public function sesuaikan_stok() {
    $tanggal = $this->input->post('tanggal');
    $catatan = $this->input->post('catatan');
    $jenis = 'PENYESUAIAN STOK';

    $kode_produk = $this->input->post('kode_produk');
    $jum_stok = $this->input->post('jumlah_barang');
    $i=0;
    foreach ($kode_produk as $idx => $kode) {
      $result[] = array(
        'kode_produk' => $kode_produk[$idx],
        'stok' => $jum_stok[$idx]
      );
      $stok_awal = $this->ProdukModel->get_stok_terakhir($result[$i]['kode_produk']);
      $stok = $result[$i]['stok'];
      $data_stok = array ('stok' => $stok);
      $kode_history = $this->ProdukModel->kode_history_terakhir();
      $data_history = array (
        'kode_history' => ++$kode_history, 
        'tanggal' => $tanggal, 
        'catatan' => $catatan,
        'jenis' => $jenis,
        'jumlah' =>  $result[$i]['stok'],
        'kode_produk' => $result[$i]['kode_produk']
      );
      $id = array('kode_produk' => $result[$i]['kode_produk']);
      $this->ProdukModel->update_data($id, $data_stok, 'tb_produk');
      $this->ProdukModel->catat_history($data_history);
      $i++;
    }
   
    $this->session->set_flashdata('stok_sesuai', '<div>Stok Produk Berhasil Disesuaikan</div>');
    return redirect('Produk/stok_sesuai');    
  }

  // history stok

  public function histori_stok() {
    $data['history'] = $this->ProdukModel->daftar_history_stok();
    return $this->load->view('histori_stok', $data);
  }

  // testing doang
  public function test() {
    echo(date("Y-m-d"));
  }

  public function stok_keluar() {
    $this->load->view('stok_keluar');
  }  
  public function stok_sesuai() {
    $this->load->view('stok_sesuai');
  }  

  public function rekap_stok() {
    $this->load->view('rekap_stok'); 
  }

  public function export_stok() {
    $jenis = $this->input->post('jenis');
    $tanggalAwal = $this->input->post('tanggalAwal');
    $tanggalAkhir = $this->input->post('tanggalAkhir');
    switch ($jenis) {
      case 'ALL':
          $ket = 'Stok Saat Ini';
        break;
      case 'STOK MASUK':
          $ket = 'Stok Masuk';
        break;
      case 'STOK KELUAR':
          $ket = 'Stok Keluar';
        break;
      case 'PENYESUAIAN STOK':
          $ket = 'Penyesuaian Stok';
        break;  
    }

    $data = $this->ProdukModel->rekap_stok($jenis, $tanggalAwal, $tanggalAkhir);
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getStyle(1)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle(2)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle(3)->getFont()->setBold(true);
    $header = array(
      'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
      ),
      'font' => array(
          'bold' => true,
          'color' => array('rgb' => 'FF0000'),
          'name' => 'Verdana'
      )
    );
    $objPHPExcel->getActiveSheet()->getStyle("A1:D2")
            ->applyFromArray($header)
            ->getFont()->setSize(16);
    $objPHPExcel->getActiveSheet()->mergeCells('A1:H2');
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Rekap '.$ket.' Tanggal: '.date('d/m/Y').' ')
        ->setCellValue('A3', 'No.')
        ->setCellValue('B3', 'Kode Histori')
        ->setCellValue('C3', 'Kode Produk')
        ->setCellValue('D3', 'Nama Produk')
        ->setCellValue('E3', 'Tanggal')
        ->setCellValue('F3', 'Jenis')
        ->setCellValue('G3', 'Jumlah')
        ->setCellValue('H3', 'Keterangan')
        ->getStyle('A3:H3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);;
        
    $ex = $objPHPExcel->setActiveSheetIndex(0);
    $no = 1;
    $counter = 4;
    foreach ($data->result() as $row) {
      $ex->setCellValue('A'.$counter, $no++);
      $ex->setCellValue('B'.$counter, $row->kode_history);
      $ex->setCellValue('C'.$counter, $row->kode_produk);
      $ex->setCellValue('D'.$counter, $row->nama_produk);
      $ex->setCellValue('E'.$counter, $row->tanggal);
      $ex->setCellValue('F'.$counter, $row->jenis);
      $ex->setCellValue('G'.$counter, $row->jumlah);
      $ex->setCellValue('H'.$counter, $row->catatan);
      $counter = $counter+1;
    }            
    $objPHPExcel->getActiveSheet()->setTitle('Rekap Stok');
    
    $objWriter  = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    header('Last-Modified:'. gmdate("D, d M Y H:i:s").'GMT');
    header('Chace-Control: no-store, no-cache, must-revalation');
    header('Chace-Control: post-check=0, pre-check=0', FALSE);
    header('Pragma: no-cache');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="rekap_'.$ket.'_'. date('d-m-Y') .'.xlsx"');
    ob_end_clean();
    $objWriter->save('php://output');
  }

  
}
?>

